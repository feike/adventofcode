DEALLOCATE problem01_2;

PREPARE problem01_2 AS
WITH input(line, lineno) AS (
	SELECT
		CAST(line AS bigint),
		lineno
	FROM
		regexp_split_to_table($1, '\s+') WITH ORDINALITY AS sub(line, lineno)
	WHERE
		line != ''
)
SELECT
	a.line * b.line * c.line
FROM
	input AS a
JOIN
	input AS b ON (a.lineno < b.lineno)
JOIN
    input AS c ON (a.lineno < c.lineno AND b.lineno < c.lineno)
WHERE
	a.line + b.line + c.line = 2020
;


EXECUTE problem01_2($$
1721
979
366
299
675
1456
$$);

\set input `cat 01.input`
EXECUTE problem01_2(:'input');

set track_io_timing to 'on';
\o 01-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_2(:'input');
\o
