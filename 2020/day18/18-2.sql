DEALLOCATE problem18_2;

PREPARE problem18_2 AS
SELECT 1
;

EXECUTE problem18_2($$
dummy line
$$);

\set input `cat 18.input`
EXECUTE problem18_2(:'input');

set track_io_timing to 'on';
\o 18-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem18_2(:'input');
\o
