DEALLOCATE problem11_2;

PREPARE problem11_2 AS
SELECT 1
;

EXECUTE problem11_2($$
dummy line
$$);

\set input `cat 11.input`
EXECUTE problem11_2(:'input');

set track_io_timing to 'on';
\o 11-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem11_2(:'input');
\o
