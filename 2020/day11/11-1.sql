DEALLOCATE problem11_1;

PREPARE problem11_1 AS
SELECT 1
;

EXECUTE problem11_1($$
dummy line
$$);

\set input `cat 11.input`
EXECUTE problem11_1(:'input');

set track_io_timing to 'on';
\o 11-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem11_1(:'input');
\o
