DEALLOCATE problem09_1;

PREPARE problem09_1 AS
SELECT 1
;

EXECUTE problem09_1($$
dummy line
$$);

\set input `cat 09.input`
EXECUTE problem09_1(:'input');

set track_io_timing to 'on';
\o 09-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_1(:'input');
\o
