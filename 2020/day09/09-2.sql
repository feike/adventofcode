DEALLOCATE problem09_2;

PREPARE problem09_2 AS
SELECT 1
;

EXECUTE problem09_2($$
dummy line
$$);

\set input `cat 09.input`
EXECUTE problem09_2(:'input');

set track_io_timing to 'on';
\o 09-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_2(:'input');
\o
