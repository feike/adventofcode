DEALLOCATE problem02_2;

PREPARE problem02_2 AS
WITH input (lineno, min, max, needle, char, charno) AS (
    SELECT
        lineno,
        match[1]::int,
        match[2]::int,
        match[3],
        char,
        charno        
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(line, lineno)
    CROSS JOIN
        regexp_matches(line, '^(\d+)-(\d+)\s+(\w):\s+(\w+)$') AS rm(match)
    CROSS JOIN
        regexp_split_to_table(match[4], '') WITH ORDINALITY as password(char, charno)
    WHERE
        line != ''
), qualified AS (
    SELECT
    FROM
        input
    WHERE
        (charno = min OR charno = max)
        AND needle = char
    GROUP BY
        lineno
    HAVING
        count(*) = 1
)
SELECT
    count(*)
FROM
    qualified
;

EXECUTE problem02_2($$
1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
$$);

\set input `cat 02.input`
EXECUTE problem02_2(:'input');

set track_io_timing to 'on';
\o 02-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_2(:'input');
\o
