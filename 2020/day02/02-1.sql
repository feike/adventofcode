DEALLOCATE problem02_1;

PREPARE problem02_1 AS
WITH input (lineno, min, max, char, password) AS (
    SELECT
        lineno,
        match[1]::int,
        match[2]::int,
        match[3],
        match[4]
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(line, lineno)
    CROSS JOIN
        regexp_matches(line, '^(\d+)-(\d+)\s+(\w):\s+(\w+)$') AS rm(match)
    WHERE
        line != ''
)
SELECT
    input.*
FROM
    input
WHERE (
    SELECT
        count(*)
    FROM
        regexp_matches(password, char, 'g')
    ) BETWEEN min AND max
;

EXECUTE problem02_1($$
1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
$$);

\set input `cat 02.input`
EXECUTE problem02_1(:'input');

set track_io_timing to 'on';
\o 02-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_1(:'input');
\o
