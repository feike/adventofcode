DEALLOCATE problem06_2;

PREPARE problem06_2 AS
WITH input AS (
    SELECT
        grp,
        person,
        answer,
        max(person) OVER (PARTITION BY grp)
    FROM
        regexp_split_to_table($1, '\n\n') WITH ORDINALITY AS rstt(a, grp)
    JOIN
        regexp_split_to_table(a, '\n') WITH ORDINALITY AS a(answers, person) ON (answers !~ '\s+')
    CROSS JOIN
        regexp_split_to_table(answers, '') AS sub(answer)
)
SELECT
    count(*) over ()
FROM
    INPUT
GROUP BY
    grp,
    max,
    answer
HAVING
    count(*) = max
LIMIT
    1
;

EXECUTE problem06_2($$abc

a
b
c

ab
ac

a
a
a
a

b$$);

\set input `cat 06.input`
EXECUTE problem06_2(:'input');

set track_io_timing to 'on';
\o 06-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_2(:'input');
\o
