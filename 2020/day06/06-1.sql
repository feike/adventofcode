DEALLOCATE problem06_1;

PREPARE problem06_1 AS
WITH input AS (
    SELECT
        grp,
        answer
    FROM
        regexp_split_to_table($1, '\n\n') WITH ORDINALITY AS rstt(answers, grp)
    CROSS JOIN
        regexp_split_to_table(answers, '') AS a(answer)
    WHERE
        answer !~ '\s+'
    GROUP BY
        grp,
        answer
)
SELECT
    sum(count(*)) OVER ()
FROM
    input
GROUP BY
    grp
LIMIT
    1
;

EXECUTE problem06_1($$abc

a
b
c

ab
ac

a
a
a
a

b$$);

\set input `cat 06.input`
EXECUTE problem06_1(:'input');

set track_io_timing to 'on';
\o 06-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_1(:'input');
\o
