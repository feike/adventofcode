DEALLOCATE problem08_1;

PREPARE problem08_1 AS
WITH RECURSIVE input AS (
    SELECT
        lineno - 1 AS idx,
        match[1] AS instruction,
        match[2]::int as delta
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(line, lineno)
    CROSS JOIN
        regexp_matches(line, '^(\w+)\s+(.\d+)$') AS rm(match)
), run AS (
    SELECT
        0 AS acc,
        '{}'::int[] AS visited,
        'jmp' AS instruction,
        0 AS delta,
        0 AS index,
        0 AS run_idx
    UNION ALL
    SELECT
        CASE run.instruction
            WHEN 'acc'
            THEN acc + run.delta
            ELSE acc
        END,
        visited||next,
        input.instruction,
        input.delta,
        next,
        run_idx + 1
    FROM
        run
    CROSS JOIN
        cast( run.index + CASE run.instruction WHEN 'jmp' THEN run.delta ELSE 1 END AS integer) AS n(next)
    JOIN
        input ON (input.idx = next)
    WHERE
        NOT visited @> ARRAY[next]::int[]
)
SELECT
    acc
FROM
    run
ORDER BY
    run_idx DESC
LIMIT
    1
;

EXECUTE problem08_1($$nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6$$);

\set input `cat 08.input`
EXECUTE problem08_1(:'input');

set track_io_timing to 'on';
\o 08-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_1(:'input');
\o
