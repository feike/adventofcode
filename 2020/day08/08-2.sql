DEALLOCATE problem08_2;

PREPARE problem08_2 AS
WITH RECURSIVE input AS (
    SELECT
        lineno - 1 AS idx,
        match[1] AS instruction,
        match[2]::int as delta
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(line, lineno)
    CROSS JOIN
        regexp_matches(line, '^(\w+)\s+(.\d+)$') AS rm(match)
), permutations AS (
    SELECT
        i as permutation,
        *
    FROM
        input
    CROSS JOIN
        generate_series(0, (select max(idx) FROM input)) AS series(i)
), run AS (
    SELECT
        0 AS acc,
        '{}'::int[] AS visited,
        'nop' AS instruction,
        1 AS delta,
        -1 AS index,
        0 AS run_idx
    FROM
        input
    UNION ALL
    SELECT
        CASE run.instruction
            WHEN 'acc'
            THEN acc + run.delta
            ELSE acc
        END,
        visited||next,
        input.instruction,
        input.delta,
        next,
        run_idx + 1
    FROM
        run
    CROSS JOIN
        cast( run.index + CASE run.instruction WHEN 'jmp' THEN run.delta ELSE 1 END AS integer) AS n(next)
    JOIN
        input ON (input.idx = next)
    WHERE
        NOT visited @> ARRAY[next]::int[]
)
SELECT
    *
FROM
    permutations
;


EXECUTE problem08_2($$nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6$$);
\set input `cat 08.input`
EXECUTE problem08_2(:'input');

set track_io_timing to 'on';
\o 08-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_2(:'input');
\o
