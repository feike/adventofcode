DEALLOCATE problem03_2;

PREPARE problem03_2 AS
WITH RECURSIVE input AS (
    SELECT
        y,
        item,
        max(length(line)) over () AS width
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(line, y)
    CROSS JOIN
        regexp_split_to_array(line, '') AS rsta(item)
    WHERE
        line != ''
), walk (dx, dy, x, y, tree) AS (
    SELECT
        dx,
        dy,
        1,
        y,
        false
    FROM
        input
    CROSS JOIN
        (VALUES (1,1), (3,1), (5,1), (7,1), (1,2)) AS slope(dx,dy)
    WHERE
        y = 1
    UNION ALL
    SELECT
        walk.dx,
        walk.dy,
        walk.x + dx,
        input.y,
        input.item[((walk.x + walk.dx - 1)%width)+1] = '#' -- PostgreSQL arrays start at index 1
    FROM
        walk
    JOIN
        input ON (walk.y + walk.dy = input.y)
), agg(trees, idx) AS (
    SELECT
        count(*) FILTER (WHERE tree),
        row_number() OVER ()
    FROM
        walk
    GROUP BY
        dx,
        dy
), multiply(trees, idx) AS (
    SELECT
        trees,
        1
    FROM
        agg
    WHERE
        idx = 1
    UNION ALL
    SELECT
        m.trees * a.trees,
        m.idx + 1
    FROM
        multiply AS m
    JOIN
        agg AS a ON (m.idx + 1 = a.idx)
)
SELECT
    *
FROM
    multiply
ORDER BY
    idx DESC
LIMIT
    1
;

EXECUTE problem03_2(
$$..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
$$);

\set input `cat 03.input`
EXECUTE problem03_2(:'input');

set track_io_timing to 'on';
\o 03-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_2(:'input');
\o
