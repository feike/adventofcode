DEALLOCATE problem03_1;

PREPARE problem03_1 AS
WITH RECURSIVE input AS (
    SELECT
        y,
        item,
        max(length(line)) over () AS width
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(line, y)
    CROSS JOIN
        regexp_split_to_array(line, '') AS rsta(item)
    WHERE
        line != ''
), walk (x, y, tree) AS (
    SELECT
        1,
        y,
        false
    FROM
        input
    WHERE
        y = 1
    UNION ALL
    SELECT
        walk.x + 3,
        walk.y + 1,
        input.item[((walk.x + 2)%width)+1] = '#' -- PostgreSQL arrays start at index 1
    FROM
        walk
    JOIN
        input ON (walk.y + 1 = input.y)
)
SELECT
    count(*)
FROM
    walk
WHERE
    tree
;

EXECUTE problem03_1(
$$..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
$$);

\set input `cat 03.input`
EXECUTE problem03_1(:'input');

set track_io_timing to 'on';
\o 03-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_1(:'input');
\o
