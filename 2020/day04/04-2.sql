DEALLOCATE problem04_2;

PREPARE problem04_2 AS
SELECT
    count(*) OVER ()
FROM
    regexp_split_to_table($1, '\n\n') WITH ORDINALITY AS rstt(line, passport)
CROSS JOIN
    regexp_split_to_array(line, '\s') AS rsta(item)
CROSS JOIN
    unnest(item) AS u(keyvalue)
CROSS JOIN
    regexp_matches(keyvalue, '(.*):(.*)') AS rm(match)
LEFT JOIN
    regexp_matches(match[2], '(\d+)(cm|in)') AS h(height) ON (match[1] = 'hgt')
WHERE
    (match[1] = 'byr' AND match[2]::int between 1920 and 2002)        
    OR (match[1] = 'iyr' AND match[2]::int between 2010 and 2020)
    OR (match[1] = 'eyr' AND match[2]::int between 2020 and 2030)
    OR (match[1] = 'hgt' AND CASE WHEN h[2] = 'cm' THEN h[1]::int between 150 and 193 WHEN h[2] = 'in' THEN h[1]::int between 59 AND 76 END)
    OR (match[1] = 'ecl' AND match[2] IN ('amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'))
    OR (match[1] = 'hcl' AND match[2] ~ '^\#[0-9a-f]{6}$')
    OR (match[1] = 'ecl' AND match[2] IN ('byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'))
    OR (match[1] = 'pid' AND match[2] ~ '^\d{9}$')
GROUP BY
    passport
HAVING
    count(DISTINCT match[1]) = 7
LIMIT
    1
;

EXECUTE problem04_2(
$$eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007$$);

EXECUTE problem04_2(
$$pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719$$);

\set input `cat 04.input`
EXECUTE problem04_2(:'input');

set track_io_timing to 'on';
\o 04-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem04_2(:'input');
\o
