DEALLOCATE problem04_1;

PREPARE problem04_1 AS
SELECT
    count(*) OVER ()
FROM
    regexp_split_to_table($1, '\n\n') WITH ORDINALITY AS rstt(line, lineno)
CROSS JOIN
    regexp_split_to_array(line, '\s') AS rsta(item)
CROSS JOIN
    unnest(item) AS u(keyvalue)
CROSS JOIN
    regexp_matches(keyvalue, '(.*):(.*)') AS rm(match)
WHERE
    match[1] IN ('byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid')
GROUP BY
    lineno
HAVING
    count(DISTINCT match[1]) = 7
LIMIT
    1
;

EXECUTE problem04_1(
$$ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in$$);

\set input `cat 04.input`
EXECUTE problem04_1(:'input');

set track_io_timing to 'on';
\o 04-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem04_1(:'input');
\o
