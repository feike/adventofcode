DEALLOCATE problem25_1;

PREPARE problem25_1 AS
SELECT 1
;

EXECUTE problem25_1($$
dummy line
$$);

\set input `cat 25.input`
EXECUTE problem25_1(:'input');

set track_io_timing to 'on';
\o 25-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem25_1(:'input');
\o
