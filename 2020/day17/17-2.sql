DEALLOCATE problem17_2;

PREPARE problem17_2 AS
SELECT 1
;

EXECUTE problem17_2($$
dummy line
$$);

\set input `cat 17.input`
EXECUTE problem17_2(:'input');

set track_io_timing to 'on';
\o 17-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem17_2(:'input');
\o
