DEALLOCATE problem13_2;

PREPARE problem13_2 AS
SELECT 1
;

EXECUTE problem13_2($$
dummy line
$$);

\set input `cat 13.input`
EXECUTE problem13_2(:'input');

set track_io_timing to 'on';
\o 13-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem13_2(:'input');
\o
