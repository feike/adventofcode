DEALLOCATE problem10_1;

PREPARE problem10_1 AS
SELECT 1
;

EXECUTE problem10_1($$
dummy line
$$);

\set input `cat 10.input`
EXECUTE problem10_1(:'input');

set track_io_timing to 'on';
\o 10-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem10_1(:'input');
\o
