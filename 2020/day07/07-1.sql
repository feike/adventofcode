DEALLOCATE problem07_1;

PREPARE problem07_1 AS
WITH RECURSIVE input AS (
    SELECT
        rule,
        ruleno,
        contains,
        contains[1] AS container,
        bag[1]::int AS amount,
        bag[2] AS contents
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(rule, ruleno)
    CROSS JOIN
        regexp_matches(rule, '^(.*)\s+bags contain\s+(.*)\s?') AS rm1(contains)
    CROSS JOIN
        regexp_split_to_table(contains[2], ',') AS rstt2(bags)
    CROSS JOIN
        regexp_matches(bags, '\s*(\d+)\s+(.*)\s+bag.?') AS rm2(bag)
), walk AS (
    SELECT
        rule,
        container
    FROM
        input
    WHERE
        contents = 'shiny gold'
    UNION ALL
    SELECT
        c.rule,
        c.container
    FROM
        walk
    JOIN
        input AS c ON (walk.container = c.contents)
)
SELECT
    count(distinct container)
FROM
    walk
;

EXECUTE problem07_1($$light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.$$);

\set input `cat 07.input`
EXECUTE problem07_1(:'input');

set track_io_timing to 'on';
\o 07-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_1(:'input');
\o
