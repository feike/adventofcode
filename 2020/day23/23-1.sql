DEALLOCATE problem23_1;

PREPARE problem23_1 AS
SELECT 1
;

EXECUTE problem23_1($$
dummy line
$$);

\set input `cat 23.input`
EXECUTE problem23_1(:'input');

set track_io_timing to 'on';
\o 23-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem23_1(:'input');
\o
