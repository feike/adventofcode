DEALLOCATE problem23_2;

PREPARE problem23_2 AS
SELECT 1
;

EXECUTE problem23_2($$
dummy line
$$);

\set input `cat 23.input`
EXECUTE problem23_2(:'input');

set track_io_timing to 'on';
\o 23-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem23_2(:'input');
\o
