DEALLOCATE problem05_1;

PREPARE problem05_1 AS
WITH input AS (
    SELECT
        array_to_string(x, '')::bit(3)::bigint as x,
        array_to_string(y, '')::bit(7)::bigint as y
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(line, lineno)
    CROSS JOIN
        regexp_split_to_array(replace(replace(substring(line for 7), 'F', '0'), 'B', '1'), '') AS y(y)
    CROSS JOIN
        regexp_split_to_array(replace(replace(substring(line from 8 for 3), 'L', '0'), 'R', '1'), '') AS x(x)
    WHERE
        line != ''
)
SELECT
    max(y*8+x)
FROM
    input
;

EXECUTE problem05_1(
$$BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL$$);

\set input `cat 05.input`
EXECUTE problem05_1(:'input');

set track_io_timing to 'on';
\o 05-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem05_1(:'input');
\o
