DEALLOCATE problem17_1;

PREPARE problem17_1 AS
SELECT 1
;

EXECUTE problem17_1($$
dummy line
$$);

\set input `cat 17.input`
EXECUTE problem17_1(:'input');

set track_io_timing to 'on';
\o 17_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem17_1(:'input');
\o
