DEALLOCATE problem21_1;

PREPARE problem21_1 AS
SELECT 1
;

EXECUTE problem21_1($$
dummy line
$$);

\set input `cat 21.input`
EXECUTE problem21_1(:'input');

set track_io_timing to 'on';
\o 21_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem21_1(:'input');
\o
