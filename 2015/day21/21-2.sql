DEALLOCATE problem21_2;

PREPARE problem21_2 AS
SELECT 1
;

EXECUTE problem21_2($$
dummy line
$$);

\set input `cat 21.input`
EXECUTE problem21_2(:'input');

set track_io_timing to 'on';
\o 21_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem21_2(:'input');
\o
