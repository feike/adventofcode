DEALLOCATE problem08_2;

PREPARE problem08_2 AS
SELECT
    sum(
        length(esc) - length(line)
    )
FROM
    regexp_split_to_table($1, '\n') AS sub(line)
CROSS JOIN LATERAL
    concat('"', regexp_replace(line, '("|\\)', '\\\1', 'g'), '"') AS sub2(esc)
WHERE
    line LIKE '"%"'
;

EXECUTE problem08_2(
$$""
"abc"
"aaa\"aaa"
"\x27"
$$);

\set input `cat 08.input`
EXECUTE problem08_2(:'input');

set track_io_timing to 'on';
\o 08_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_2(:'input');
\o
