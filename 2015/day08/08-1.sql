DEALLOCATE problem08_1;

PREPARE problem08_1 AS
SELECT
    sum(
        length(line) - (length(memory) - 2)
    )
FROM
    regexp_split_to_table($1, '\n') AS sub(line)
CROSS JOIN LATERAL
    regexp_replace(line, '(\\\\|\\x..|\\")', '.', 'g') AS sub2(memory)
WHERE
    line LIKE '"%"'
;

EXECUTE problem08_1(
$$""
"abc"
"aaa\"aaa"
"\x27"
$$);

\set input `cat 08.input`
EXECUTE problem08_1(:'input');

set track_io_timing to 'on';
\o 08_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_1(:'input');
\o
