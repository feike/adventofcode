DEALLOCATE problem25_2;

PREPARE problem25_2 AS
SELECT 1
;

EXECUTE problem25_2($$
dummy line
$$);

\set input `cat 25.input`
EXECUTE problem25_2(:'input');

set track_io_timing to 'on';
\o 25_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem25_2(:'input');
\o
