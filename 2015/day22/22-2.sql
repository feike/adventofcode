DEALLOCATE problem22_2;

PREPARE problem22_2 AS
SELECT 1
;

EXECUTE problem22_2($$
dummy line
$$);

\set input `cat 22.input`
EXECUTE problem22_2(:'input');

set track_io_timing to 'on';
\o 22_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem22_2(:'input');
\o
