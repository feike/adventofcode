DEALLOCATE problem12_2;

PREPARE problem12_2 AS
SELECT 1
;

EXECUTE problem12_2($$
dummy line
$$);

\set input `cat 12.input`
EXECUTE problem12_2(:'input');

set track_io_timing to 'on';
\o 12_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem12_2(:'input');
\o
