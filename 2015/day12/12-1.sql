DEALLOCATE problem12_1;

PREPARE problem12_1 AS
SELECT 1
;

EXECUTE problem12_1($$
dummy line
$$);

\set input `cat 12.input`
EXECUTE problem12_1(:'input');

set track_io_timing to 'on';
\o 12_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem12_1(:'input');
\o
