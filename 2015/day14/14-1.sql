DEALLOCATE problem14_1;

PREPARE problem14_1 AS
SELECT 1
;

EXECUTE problem14_1($$
dummy line
$$);

\set input `cat 14.input`
EXECUTE problem14_1(:'input');

set track_io_timing to 'on';
\o 14_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem14_1(:'input');
\o
