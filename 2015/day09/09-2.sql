DEALLOCATE problem09_2;

PREPARE problem09_2 AS
SELECT 1
;

EXECUTE problem09_2(
$$London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141$$);

\set input `cat 09.input`
EXECUTE problem09_2(:'input');

set track_io_timing to 'on';
\o 09_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_2(:'input');
\o
