DEALLOCATE problem09_1;

PREPARE problem09_1 AS
SELECT 1
;

EXECUTE problem09_1(
$$London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141$$);

\set input `cat 09.input`
EXECUTE problem09_1(:'input');

set track_io_timing to 'on';
\o 09_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_1(:'input');
\o
