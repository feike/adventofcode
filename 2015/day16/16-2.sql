DEALLOCATE problem16_2;

PREPARE problem16_2 AS
SELECT 1
;

EXECUTE problem16_2($$
dummy line
$$);

\set input `cat 16.input`
EXECUTE problem16_2(:'input');

set track_io_timing to 'on';
\o 16_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem16_2(:'input');
\o
