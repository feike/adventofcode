DEALLOCATE problem10_2;

PREPARE problem10_2 AS
SELECT 1
;

EXECUTE problem10_2($$
dummy line
$$);

\set input `cat 10.input`
EXECUTE problem10_2(:'input');

set track_io_timing to 'on';
\o 10_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem10_2(:'input');
\o
