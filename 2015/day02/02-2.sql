DEALLOCATE problem02_2;

PREPARE problem02_2 AS
WITH dimensions(l, w, h) AS (
	SELECT
		f[1]::bigint,
		f[2]::bigint,
		f[3]::bigint
	FROM
		regexp_split_to_table($1, '\n') AS rstt(line)
	CROSS JOIN LATERAL
		regexp_split_to_array(line, 'x') AS rsta(f)
)
SELECT
	sum(
		2*least(l+w, w+h, h+l) +
		l*w*h
	)
FROM
	dimensions;

EXECUTE problem02_2($$2x3x4$$);
EXECUTE problem02_2($$1x1x10$$);

\set input `cat 02.input`
EXECUTE problem02_2(:'input');

set track_io_timing to 'on';
\o 02_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_2(:'input');
\o
