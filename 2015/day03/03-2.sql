DEALLOCATE problem03_2;

PREPARE problem03_2 AS
WITH agents(agent, rank, count) AS (
    SELECT
        *,
        count(*) OVER ()
    FROM
        unnest('{Santa, Robo-Santa}'::text[]) WITH ordinality
) , instructions (y, x, position, agent, instruction) AS (
    SELECT
        0,
        0,
        -rank,
        agent,
        null::text
    FROM
        agents
    UNION ALL
    SELECT
        x,
        y,
        position,
        agent,
        instruction
    FROM
        regexp_split_to_table($1, '') WITH ORDINALITY AS rstt(instruction, position)
    JOIN
        ( VALUES
            ('^',  1,  0),
            ('v', -1,  0),
            ('>',  0,  1),
            ('<',  0, -1)
        ) AS v(instruction, x, y) USING (instruction)
    JOIN
        agents ON (rank%count = position%count)
) , visits AS (
    SELECT
        instruction,
        agent,
        x,
        y,
        (sum(x) OVER (w), sum(y) OVER (w)) AS location
    FROM
        instructions
    WINDOW
        w AS (PARTITION BY agent ORDER BY position)
)
SELECT
    count(distinct location)
FROM
    visits
;

EXECUTE problem03_2('^v');
EXECUTE problem03_2('^>v<');
EXECUTE problem03_2('^v^v^v^v^v');

\set input `cat 03.input`
EXECUTE problem03_2(:'input');

set track_io_timing to 'on';
\o 03_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_2(:'input');
\o
