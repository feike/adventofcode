DEALLOCATE problem03_1;

PREPARE problem03_1 AS
WITH instructions (y, x, position, instruction) AS (
    SELECT
        0,
        0,
        0,
        null::text
    UNION ALL
    SELECT
        x,
        y,
        position,
        instruction
    FROM
        regexp_split_to_table($1, '') WITH ORDINALITY AS rstt(instruction, position)
    JOIN
        ( VALUES
            ('^',  1,  0),
            ('v', -1,  0),
            ('>',  0,  1),
            ('<',  0, -1)
        ) AS v(instruction, x, y) USING (instruction)
) , visits AS (
    SELECT
        instruction,
        x,
        y,
        (sum(x) OVER (ORDER BY position), sum(y) OVER (ORDER BY position)) AS location
    FROM
        instructions
)
SELECT
    count(distinct location)
FROM
    visits
;

EXECUTE problem03_1('>');
EXECUTE problem03_1('^>v<');
EXECUTE problem03_1('^v^v^v^v^v');

\set input `cat 03.input`
EXECUTE problem03_1(:'input');

set track_io_timing to 'on';
\o 03_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_1(:'input');
\o
