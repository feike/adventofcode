DEALLOCATE problem18_1;

PREPARE problem18_1 AS
SELECT 1
;

EXECUTE problem18_1($$
dummy line
$$);

\set input `cat 18.input`
EXECUTE problem18_1(:'input');

set track_io_timing to 'on';
\o 18_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem18_1(:'input');
\o
