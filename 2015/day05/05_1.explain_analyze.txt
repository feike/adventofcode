                                                                          QUERY PLAN                                                                          
--------------------------------------------------------------------------------------------------------------------------------------------------------------
 Aggregate  (cost=22.67..22.68 rows=1 width=8) (actual time=69.070..69.070 rows=1 loops=1)
   ->  Function Scan on regexp_split_to_table rstt  (cost=0.00..22.50 rows=67 width=0) (actual time=1.001..69.029 rows=255 loops=1)
         Filter: ((word !~* 'ab|cd|pq|xy'::text) AND (word ~* '(.)\1+'::text) AND (length(regexp_replace(word, '[^aeiou]'::text, ''::text, 'g'::text)) >= 3))
         Rows Removed by Filter: 745
 Planning Time: 0.007 ms
 Execution Time: 69.093 ms
(6 rows)

