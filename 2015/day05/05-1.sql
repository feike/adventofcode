DEALLOCATE problem05_1;

PREPARE problem05_1 AS
SELECT
    count(*)
FROM
    regexp_split_to_table($1, '\n') AS rstt(word)
WHERE
    word !~* 'ab|cd|pq|xy'
    AND word ~* '(.)\1+'
    AND length(regexp_replace(word, '[^aeiou]', '', 'g')) >= 3
;

EXECUTE problem05_1('ugknbfddgicrmopn');
EXECUTE problem05_1('aaa');
EXECUTE problem05_1('jchzalrnumimnmhp');
EXECUTE problem05_1('haegwjzuvuyypxyu');
EXECUTE problem05_1('dvszwmarrgswjxmb');

\set input `cat 05.input`
EXECUTE problem05_1(:'input');

set track_io_timing to 'on';
\o 05_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem05_1(:'input');
\o
