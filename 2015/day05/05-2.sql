DEALLOCATE problem05_2;

PREPARE problem05_2 AS
WITH RECURSIVE input AS (
    SELECT
        *
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(word, line)
    WHERE
        length(word) >= 3
), mirror1(word, line, match, position) AS (
    SELECT
        word,
        line,
        substr(word, 1, 1) = substr(word, 3, 1),
        1
    FROM
        input
    UNION ALL
    SELECT
        m.word,
        line,
        substr(m.word, position+1, 1) = substr(m.word, position+3, 1),
        position+1
    FROM
        mirror1 m
    JOIN
        input USING (line)
    WHERE
        length(m.word) > position + 2
        AND NOT match
), mirror2 AS (
    SELECT
        word,
        line,
        substr(word, position, 3) AS mirror
    FROM
        mirror1
    WHERE
        match
), repeat(word, line, mirror, match, position) AS (
    SELECT
        word,
        line,
        mirror,
        substr(word, 1, 2),
        1
    FROM
        mirror2
    UNION ALL
    SELECT
        r.word,
        line,
        r.mirror,
        substr(r.word, position+1, 2),
        r.position + 1
    FROM
        repeat r
    JOIN
        mirror2 USING (line)
    WHERE
        length(r.word) > position + 1
)
SELECT
    count(DISTINCT word)
FROM
    repeat
WHERE
    array_length(regexp_split_to_array(word, match), 1) >= 3;

EXECUTE problem05_2('qjhvhtzxzqqjkmpb');
EXECUTE problem05_2('xxyxx');
EXECUTE problem05_2('uurcxstgmygtbstg');
EXECUTE problem05_2('ieodomkazucvgmuy');
EXECUTE problem05_2('aaa');

\set input `cat 05.input`
EXECUTE problem05_2(:'input');

set track_io_timing to 'on';
\o 05_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem05_2(:'input');
\o
