DEALLOCATE problem24_1;

PREPARE problem24_1 AS
SELECT 1
;

EXECUTE problem24_1($$
dummy line
$$);

\set input `cat 24.input`
EXECUTE problem24_1(:'input');

set track_io_timing to 'on';
\o 24_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem24_1(:'input');
\o
