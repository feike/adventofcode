DEALLOCATE problem19_1;

PREPARE problem19_1 AS
SELECT 1
;

EXECUTE problem19_1($$
dummy line
$$);

\set input `cat 19.input`
EXECUTE problem19_1(:'input');

set track_io_timing to 'on';
\o 19_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem19_1(:'input');
\o
