DEALLOCATE problem19_2;

PREPARE problem19_2 AS
SELECT 1
;

EXECUTE problem19_2($$
dummy line
$$);

\set input `cat 19.input`
EXECUTE problem19_2(:'input');

set track_io_timing to 'on';
\o 19_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem19_2(:'input');
\o
