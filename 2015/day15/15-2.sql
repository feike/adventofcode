DEALLOCATE problem15_2;

PREPARE problem15_2 AS
WITH RECURSIVE input(x, y, position, risk) AS MATERIALIZED (
    SELECT
        (x-1)::smallint,
        (y-1)::smallint,
        1 + ((y-1) * max(x) OVER () + (x-1))::smallint,
        risk::smallint
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS sub(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS sub2(risk, x)
    WHERE
        line != ''
), bounds AS MATERIALIZED (
    SELECT
        max(x)::smallint AS max_x,
        max(y)::smallint AS max_y,
        max(position)::smallint AS max_position
    FROM
        input
), walk AS (
    SELECT
        array_agg(((x + y) * 9)::smallint) AS nodes,
        1::smallint AS position,
        null::smallint AS risk,
        0::smallint AS x,
        0::smallint AS y,
        0::smallint AS level
    FROM
        input
    CROSS JOIN
        bounds

    UNION ALL

    SELECT
        new_nodes,
        new_position,
        new_nodes[new_position],
        new_x,
        new_y,
        (level + 1)::smallint
    FROM
        walk
    CROSS JOIN LATERAL (
        WITH new_nodes AS (
            SELECT
                position::smallint,
                x,
                y,
                CASE
                    WHEN old_node IS NULL
                    THEN null
                    WHEN input.position = walk.position
                    THEN null
                    WHEN abs(input.y - walk.y) + abs(input.x - walk.x) = 1
                    THEN least(old_node, input.risk + nodes[walk.position])
                    ELSE old_node
                END::smallint AS risk
            FROM
                input
            JOIN
                unnest(nodes) WITH ORDINALITY AS _(old_node, position) USING (position)
        ), next_position(new_position, new_x, new_y) AS (
            SELECT
                position,
                x,
                y
            FROM
                new_nodes
            WHERE
                risk IS NOT NULL
            ORDER BY
                risk ASC,
                position DESC
            LIMIT
                1
        )
        SELECT
            (SELECT array_agg(risk ORDER BY position) FROM new_nodes),
            *
        FROM
            next_position           
    ) _(new_nodes, new_position)
    WHERE
        position != (SELECT max_position FROM bounds)
)
/*
, visualize AS (
    SELECT
        level,
        string_agg(line|| '   ', E'\n' ORDER BY y)||E'\n'
    FROM (
        SELECT
            level,
            (idx-1)/(max_y+1) AS y,
            string_agg(
                CASE
                    WHEN node IS NULL
                    THEN '  *'
                    ELSE lpad(node::text, 3)
                END, ' ' ORDER BY idx
            ) AS line
        FROM
            walk
        CROSS JOIN
            bounds
        CROSS JOIN
            unnest(nodes) WITH ORDINALITY AS _(node, idx)
        GROUP BY
            level,
            (idx-1)/(max_y+1)
        ) _
    GROUP BY
        level
    ORDER BY
        level
)
*/
SELECT
    nodes[max_position]
FROM
    walk
CROSS JOIN
    bounds
ORDER BY
    level DESC
LIMIT
    1
;

EXECUTE problem15_2(
$$1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581$$);

/*
\set input `cat 15.input`
EXECUTE problem15_2(:'input');

set track_io_timing to 'on';
\o 15-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem15_2(:'input');
\o
*/