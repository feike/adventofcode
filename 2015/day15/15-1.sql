DEALLOCATE problem15_1;

PREPARE problem15_1 AS
SELECT 1
;

EXECUTE problem15_1($$
dummy line
$$);

\set input `cat 15.input`
EXECUTE problem15_1(:'input');

set track_io_timing to 'on';
\o 15_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem15_1(:'input');
\o
