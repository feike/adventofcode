DEALLOCATE problem20_2;

PREPARE problem20_2 AS
SELECT 1
;

EXECUTE problem20_2($$
dummy line
$$);

\set input `cat 20.input`
EXECUTE problem20_2(:'input');

set track_io_timing to 'on';
\o 20_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem20_2(:'input');
\o
