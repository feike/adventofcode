DEALLOCATE problem20_1;

PREPARE problem20_1 AS
SELECT 1
;

EXECUTE problem20_1($$
dummy line
$$);

\set input `cat 20.input`
EXECUTE problem20_1(:'input');

set track_io_timing to 'on';
\o 20_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem20_1(:'input');
\o
