DEALLOCATE problem04_2;

PREPARE problem04_2 AS
WITH recursive zero_hash(id) AS (
    SELECT
        0
    UNION ALL
    SELECT
        id + 1
    FROM
        zero_hash
    WHERE
        md5($1||id) NOT LIKE '000000%'
)
SELECT
    max(id)
FROM
    zero_hash
;

\set input `cat 04.input`
EXECUTE problem04_2(:'input');

set track_io_timing to 'on';
\o 04_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem04_2(:'input');
\o
