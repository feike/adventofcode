DEALLOCATE problem04_1;

PREPARE problem04_1 AS
WITH recursive zero_hash(id) AS (
    SELECT
        0
    UNION ALL
    SELECT
        id + 1
    FROM
        zero_hash
    WHERE
        md5($1||id) NOT LIKE '00000%'
)
SELECT
    max(id)
FROM
    zero_hash
;

EXECUTE problem04_1('abcdef');
EXECUTE problem04_1('pqrstuv');

\set input `cat 04.input`
EXECUTE problem04_1(:'input');

set track_io_timing to 'on';
\o 04_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem04_1(:'input');
\o
