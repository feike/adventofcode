DEALLOCATE problem07_1;

PREPARE problem07_1 AS
SELECT 1
;

EXECUTE problem07_1($$
dummy line
$$);

\set input `cat 07.input`
EXECUTE problem07_1(:'input');

set track_io_timing to 'on';
\o 07_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_1(:'input');
\o
