DEALLOCATE problem07_2;

PREPARE problem07_2 AS
SELECT 1
;

EXECUTE problem07_2($$
dummy line
$$);

\set input `cat 07.input`
EXECUTE problem07_2(:'input');

set track_io_timing to 'on';
\o 07_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_2(:'input');
\o
