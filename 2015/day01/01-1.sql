DEALLOCATE problem01_1;

PREPARE problem01_1 AS
	SELECT
		sum(direction)
	FROM regexp_split_to_table($1, '') AS rstt(instruction)
	JOIN (VALUES ('(', 1), (')', -1)) AS v(instruction, direction) USING (instruction)
;

EXECUTE problem01_1($$(())$$);
EXECUTE problem01_1($$()()$$);
EXECUTE problem01_1($$((($$);
EXECUTE problem01_1($$(()(()($$);
EXECUTE problem01_1($$))((((($$);
EXECUTE problem01_1($$())$$);
EXECUTE problem01_1($$))($$);
EXECUTE problem01_1($$)))$$);
EXECUTE problem01_1($$)())())$$);

\set input `cat 01.input`
EXECUTE problem01_1(:'input');

set track_io_timing to 'on';
\o 01_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_1(:'input');
\o
