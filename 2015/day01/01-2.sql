DEALLOCATE problem01_2;

PREPARE problem01_2 AS
WITH RECURSIVE instructions AS (
	SELECT
		direction,
		ordinality AS position
	FROM regexp_split_to_table($1, '') WITH ORDINALITY AS rstt(instruction)
	JOIN (VALUES ('(', 1), (')', -1)) AS v(instruction, direction) USING (instruction)
), walk AS (
	SELECT
		0::bigint AS position,
		0::bigint AS floor
	UNION ALL
	SELECT
		c.position,
		p.floor + c.direction
	FROM
		walk p
	JOIN
		instructions c ON (c.position = p.position+1)
	WHERE
		p.floor != -1
)
SELECT
	max(position)
FROM
	walk;


EXECUTE problem01_2($$)$$);
EXECUTE problem01_2($$()())$$);

\set input `cat 01.input`
EXECUTE problem01_2(:'input');

set track_io_timing to 'on';
\o 01_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_2(:'input');
\o
