DEALLOCATE problem06_2;

\set gridsize 1000

PREPARE problem06_2 AS
WITH RECURSIVE input AS (
    SELECT
        lineno::int,
        match[1] AS instruction,
        match[2]::int AS x_start,
        match[3]::int AS y_start,
        match[4]::int AS x_stop,
        match[5]::int AS y_stop
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(instruction, lineno)
    CROSS JOIN
        regexp_matches(instruction, '^\s*([^\d]+)\s+(\d+),(\d+)\s+through\s+(\d+),(\d+)') AS rm(match)
), instructions(lineno, y, deltas) AS (
    SELECT
        lineno,
        y::smallint,
        array_agg(
            CASE
                WHEN x < x_start OR x > x_stop
                THEN 0::smallint
                WHEN instruction = 'turn off'
                THEN -1::smallint
                WHEN instruction = 'turn on'
                THEN +1::smallint
                WHEN instruction = 'toggle'
                THEN +2::smallint
            END
        ORDER BY x)
    FROM
        input
    CROSS JOIN
        generate_series(0, :'gridsize' - 1) AS subx(x)
    CROSS JOIN
        generate_series(y_start, y_stop) AS suby(y)
    GROUP BY
        lineno,
        y
), grid(y, values) AS (
    SELECT
        y::smallint,
        (SELECT array_agg(0) FROM generate_series(0, :'gridsize' - 1))
    FROM
        generate_series(0, :'gridsize' - 1) AS sub(y)
), walk(y, values, lineno) AS (
    SELECT
        y,
        values,
        0
    FROM
        grid
    UNION ALL
    SELECT
        w.y,
        CASE
            WHEN deltas IS NULL
            THEN values
            ELSE (
                SELECT
                    array_agg(greatest(0, v+d) ORDER BY x)
                FROM
                    unnest(values) WITH ORDINALITY AS s1(v, x)
                JOIN
                    unnest(deltas) WITH ORDINALITY AS s2(d, x) USING (x)
                )
        END,
        w.lineno+1
    FROM
        walk w
    LEFT JOIN
        instructions i ON (w.lineno + 1 = i.lineno AND w.y = i.y)
    WHERE
        w.lineno < (SELECT max(lineno) FROM input)
)
SELECT
    sum(value)
FROM
    walk
CROSS JOIN
    unnest(values) u(value)
WHERE
    lineno = (SELECT max(lineno) FROM input)
;

\set input `cat 06.input`
EXECUTE problem06_2(:'input');

set track_io_timing to 'on';
\o 06_2.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_2(:'input');
\o
