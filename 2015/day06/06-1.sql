DEALLOCATE problem06_1;

PREPARE problem06_1 AS
WITH RECURSIVE input AS (
    SELECT
        lineno,
        match[1] AS instruction,
        match[2]::int AS x_start,
        match[3]::int AS y_start,
        match[4]::int AS x_stop,
        match[5]::int AS y_stop
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(instruction, lineno)
    CROSS JOIN
        regexp_matches(instruction, '^\s*([^\d]+)\s+(\d+),(\d+)\s+through\s+(\d+),(\d+)') AS rm(match)
), instructions(lineno, y, instruction, mask) AS (
    SELECT
        lineno,
        y,
        instruction,
        CASE
            WHEN instruction = 'turn off'
            THEN repeat('1', x_start)||repeat('0', x_stop - x_start + 1)||repeat('1', 1000 - x_stop - 1)
            ELSE repeat('0', x_start)||repeat('1', x_stop - x_start + 1)||repeat('0', 1000 - x_stop - 1)
        END::bit(1000)
    FROM
        input
    CROSS JOIN
        generate_series(y_start, y_stop) AS sub(y)
), grid(y, gridline) AS (
    SELECT
        i-1,
        repeat('0', 1000)::bit(1000)
    FROM
        generate_series(1,1000) as sub(i)
), walk(y, gridline, lineno, level) AS (
    SELECT
        y,
        gridline,
        0::bigint,
        0::int
    FROM
        grid
    UNION ALL
    SELECT
        w.y,
        CASE instruction
            WHEN 'turn off'
            THEN w.gridline & i.mask
            WHEN 'turn on'
            THEN w.gridline | i.mask
            WHEN 'toggle'
            THEN w.gridline # i.mask
            ELSE w.gridline
        END::bit(1000),
        w.lineno + 1,
        level + 1
    FROM
        walk w
    LEFT JOIN
        instructions i ON (w.lineno + 1 = i.lineno AND w.y = i.y)
    WHERE
        level < (SELECT max(lineno) FROM input)
)
SELECT
    sum(
        length(regexp_replace(gridline::text, '0', '', 'g'))
    )
FROM
    walk
WHERE
    level = (SELECT max(lineno) FROM input)
;

EXECUTE problem06_1(
$$turn on 0,0 through 999,999
toggle 0,0 through 999,0
turn off 499,499 through 500,500
$$);

\set input `cat 06.input`
EXECUTE problem06_1(:'input');

set track_io_timing to 'on';
\o 06_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_1(:'input');
\o
