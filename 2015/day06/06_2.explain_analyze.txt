                                                                           QUERY PLAN                                                                            
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
 Aggregate  (cost=218889866.80..218889866.81 rows=1 width=8) (actual time=116275.971..116275.971 rows=1 loops=1)
   CTE input
     ->  Nested Loop  (cost=0.01..52.51 rows=1000 width=52) (actual time=0.260..9.211 rows=300 loops=1)
           ->  Function Scan on regexp_split_to_table rstt  (cost=0.00..10.00 rows=1000 width=40) (actual time=0.199..0.223 rows=300 loops=1)
           ->  Function Scan on regexp_matches rm  (cost=0.00..0.01 rows=1 width=32) (actual time=0.029..0.029 rows=1 loops=300)
   CTE instructions
     ->  GroupAggregate  (cost=196188981.77..218689581.77 rows=40000 width=42) (actual time=35345.195..60394.437 rows=77396 loops=1)
           Group Key: input.lineno, suby.y
           ->  Sort  (cost=196188981.77..198688981.77 rows=1000000000 width=52) (actual time=35344.787..38911.193 rows=77396000 loops=1)
                 Sort Key: input.lineno, suby.y
                 Sort Method: quicksort  Memory: 7982396kB
                 ->  Nested Loop  (cost=0.01..12522530.01 rows=1000000000 width=52) (actual time=0.114..9727.788 rows=77396000 loops=1)
                       ->  Function Scan on generate_series subx  (cost=0.00..10.00 rows=1000 width=4) (actual time=0.083..0.883 rows=1000 loops=1)
                       ->  Materialize  (cost=0.00..25020.00 rows=1000000 width=48) (actual time=0.000..2.654 rows=77396 loops=1000)
                             ->  Nested Loop  (cost=0.00..20020.00 rows=1000000 width=48) (actual time=0.024..22.676 rows=77396 loops=1)
                                   ->  CTE Scan on input  (cost=0.00..20.00 rows=1000 width=52) (actual time=0.001..0.142 rows=300 loops=1)
                                   ->  Function Scan on generate_series suby  (cost=0.00..10.00 rows=1000 width=4) (actual time=0.018..0.040 rows=258 loops=300)
   CTE grid
     ->  Function Scan on generate_series sub  (cost=12.52..25.02 rows=1000 width=34) (actual time=0.178..0.269 rows=1000 loops=1)
           InitPlan 3 (returns $5)
             ->  Aggregate  (cost=12.50..12.51 rows=1 width=32) (actual time=0.136..0.137 rows=1 loops=1)
                   ->  Function Scan on generate_series  (cost=0.00..10.00 rows=1000 width=0) (actual time=0.038..0.074 rows=1000 loops=1)
   CTE walk
     ->  Recursive Union  (cost=0.00..199025.56 rows=34330 width=38) (actual time=0.180..115490.174 rows=301000 loops=1)
           ->  CTE Scan on grid  (cost=0.00..20.00 rows=1000 width=38) (actual time=0.179..2.015 rows=1000 loops=1)
           ->  Merge Left Join  (cost=4300.08..19831.90 rows=3333 width=38) (actual time=208.627..383.177 rows=997 loops=301)
                 Merge Cond: ((((w.lineno + 1)) = i.lineno) AND (w.y = i.y))
                 InitPlan 6 (returns $10)
                   ->  Aggregate  (cost=22.50..22.51 rows=1 width=4) (actual time=0.035..0.035 rows=1 loops=1)
                         ->  CTE Scan on input input_1  (cost=0.00..20.00 rows=1000 width=4) (actual time=0.001..0.016 rows=300 loops=1)
                 ->  Sort  (cost=420.02..428.36 rows=3333 width=38) (actual time=0.775..0.830 rows=997 loops=301)
                       Sort Key: ((w.lineno + 1)), w.y
                       Sort Method: quicksort  Memory: 25kB
                       ->  WorkTable Scan on walk w  (cost=0.00..225.00 rows=3333 width=38) (actual time=0.001..0.196 rows=997 loops=301)
                             Filter: (lineno < $10)
                             Rows Removed by Filter: 3
                 ->  Sort  (cost=3857.54..3957.54 rows=40000 width=38) (actual time=202.079..203.373 rows=38822 loops=300)
                       Sort Key: i.lineno, i.y
                       Sort Method: quicksort  Memory: 159074kB
                       ->  CTE Scan on instructions i  (cost=0.00..800.00 rows=40000 width=38) (actual time=35345.199..60557.348 rows=77396 loops=1)
                 SubPlan 5
                   ->  Aggregate  (cost=4.51..4.52 rows=1 width=32) (actual time=0.677..0.677 rows=1 loops=77396)
                         ->  Merge Join  (cost=0.01..3.75 rows=100 width=14) (actual time=0.090..0.512 rows=1000 loops=77396)
                               Merge Cond: (s1.x = s2.x)
                               ->  Function Scan on unnest s1  (cost=0.00..1.00 rows=100 width=12) (actual time=0.041..0.095 rows=1000 loops=77396)
                               ->  Materialize  (cost=0.00..1.25 rows=100 width=10) (actual time=0.045..0.190 rows=1000 loops=77396)
                                     ->  Function Scan on unnest s2  (cost=0.00..1.00 rows=100 width=10) (actual time=0.041..0.096 rows=1000 loops=77396)
   InitPlan 8 (returns $12)
     ->  Aggregate  (cost=22.50..22.51 rows=1 width=4) (actual time=9.316..9.316 rows=1 loops=1)
           ->  CTE Scan on input input_2  (cost=0.00..20.00 rows=1000 width=4) (actual time=0.260..9.287 rows=300 loops=1)
   ->  Nested Loop  (cost=0.00..1116.43 rows=17200 width=4) (actual time=116018.950..116231.669 rows=1000000 loops=1)
         ->  CTE Scan on walk  (cost=0.00..772.42 rows=172 width=32) (actual time=116018.878..116082.211 rows=1000 loops=1)
               Filter: (lineno = $12)
               Rows Removed by Filter: 300000
         ->  Function Scan on unnest u  (cost=0.00..1.00 rows=100 width=4) (actual time=0.042..0.078 rows=1000 loops=1000)
 Planning Time: 0.354 ms
 Execution Time: 116559.273 ms
(57 rows)

