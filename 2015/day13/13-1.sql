DEALLOCATE problem13_1;

PREPARE problem13_1 AS
SELECT 1
;

EXECUTE problem13_1($$
dummy line
$$);

\set input `cat 13.input`
EXECUTE problem13_1(:'input');

set track_io_timing to 'on';
\o 13_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem13_1(:'input');
\o
