
WITH input AS (
    SELECT
        regexp_replace(match, '.*\((\d+),.*', '\1')::int AS x,
        regexp_replace(match, '.*,(\d+).*', '\1')::int AS y
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, line_no)
    CROSS JOIN
        regexp_matches(line, '(mul\(\d+,\d+\))', 'g') AS re(matches)
    CROSS JOIN
        unnest(matches) AS u(match)
)
SELECT
    sum(x * y)
FROM
    input;
