

WITH input AS (
    SELECT
        regexp_replace(match, '.*\((\d+),.*', '\1')::int AS x,
        regexp_replace(match, '.*,(\d+).*', '\1')::int AS y
    FROM
        regexp_split_to_table($1, 'do\(\)') AS _(line)
    CROSS JOIN
        split_part(line, $$don't()$$, 1) AS p(p)
    CROSS JOIN
        regexp_matches(p, '(mul\(\d+,\d+\))', 'g') AS re(matches)
    CROSS JOIN
        unnest(matches) AS u(match)
)
SELECT
    sum(x * y)
FROM
    input;
