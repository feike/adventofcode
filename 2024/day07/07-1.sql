WITH RECURSIVE input AS (
    SELECT
        y,
        p[1]::bigint AS result,
        v::int[] AS values
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_array(line, '\s*:\s*') AS r(p)
    CROSS JOIN
        regexp_split_to_array(p[2], '\s+') AS rt(v)
    WHERE
        line ~ '\d+'
), walk AS (
    SELECT
        y,
        result,
        values[1]::numeric AS value,
        values,
        1::int AS idx
    FROM
        input

    UNION ALL

    SELECT
        y,
        result,
        CASE
            WHEN mul
            THEN w.value * values[idx+1]
            ELSE w.value + values[idx+1]
        END,
        values,
        idx + 1
    FROM
        walk AS w
    CROSS JOIN
        (VALUES (true), (false)) AS _(mul)
    WHERE
        value <= result
        AND values[idx+1] IS NOT NULL
)
SELECT
    sum(any_value(result)) OVER ()
FROM
    walk
WHERE
    array_length(values, 1) = idx
    AND value = result
GROUP BY
    y
LIMIT
    1;


