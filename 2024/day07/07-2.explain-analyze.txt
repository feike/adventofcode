                                                                      QUERY PLAN                                                                       
-------------------------------------------------------------------------------------------------------------------------------------------------------
 Limit  (cost=12165.22..12165.23 rows=1 width=40) (actual time=11534.920..11534.923 rows=1 loops=1)
   Buffers: temp written=168283
   I/O Timings: temp write=908.859
   CTE walk
     ->  Recursive Union  (cost=0.01..9149.83 rows=100510 width=84) (actual time=0.515..6541.061 rows=13061500 loops=1)
           ->  Nested Loop  (cost=0.01..170.01 rows=1000 width=84) (actual time=0.515..5.560 rows=850 loops=1)
                 ->  Nested Loop  (cost=0.01..32.51 rows=1000 width=40) (actual time=0.505..1.966 rows=850 loops=1)
                       ->  Function Scan on regexp_split_to_table _  (cost=0.00..12.50 rows=1000 width=40) (actual time=0.499..0.793 rows=850 loops=1)
                             Filter: (line ~ '\d+'::text)
                       ->  Function Scan on regexp_split_to_array r  (cost=0.00..0.01 rows=1 width=32) (actual time=0.001..0.001 rows=1 loops=850)
                 ->  Function Scan on regexp_split_to_array rt  (cost=0.00..0.01 rows=1 width=32) (actual time=0.003..0.003 rows=1 loops=850)
           ->  Nested Loop  (cost=0.00..797.47 rows=9951 width=84) (actual time=39.619..413.254 rows=1088388 loops=12)
                 ->  WorkTable Scan on walk w  (cost=0.00..275.00 rows=3317 width=84) (actual time=39.616..128.089 rows=362796 loops=12)
                       Filter: (("values"[(idx + 1)] IS NOT NULL) AND (value <= (result)::numeric))
                       Rows Removed by Filter: 725662
                 ->  Materialize  (cost=0.00..0.05 rows=3 width=1) (actual time=0.000..0.000 rows=3 loops=4353550)
                       ->  Values Scan on "*VALUES*"  (cost=0.00..0.04 rows=3 width=1) (actual time=0.001..0.001 rows=3 loops=1)
   ->  WindowAgg  (cost=3015.38..3015.41 rows=3 width=40) (actual time=11534.918..11534.919 rows=1 loops=1)
         Buffers: temp written=168283
         I/O Timings: temp write=908.859
         ->  GroupAggregate  (cost=3015.32..3015.38 rows=3 width=16) (actual time=11534.698..11534.807 rows=651 loops=1)
               Group Key: walk.y
               Buffers: temp written=168283
               I/O Timings: temp write=908.859
               ->  Sort  (cost=3015.32..3015.33 rows=3 width=16) (actual time=11534.687..11534.706 rows=677 loops=1)
                     Sort Key: walk.y
                     Sort Method: quicksort  Memory: 46kB
                     Buffers: temp written=168283
                     I/O Timings: temp write=908.859
                     ->  CTE Scan on walk  (cost=0.00..3015.30 rows=3 width=16) (actual time=7.788..11534.374 rows=677 loops=1)
                           Filter: ((array_length("values", 1) = idx) AND (value = (result)::numeric))
                           Rows Removed by Filter: 13060823
                           Buffers: temp written=168283
                           I/O Timings: temp write=908.859
 Planning:
   Memory: used=262kB  allocated=345kB
 Planning Time: 0.325 ms
 Execution Time: 11751.218 ms
(38 rows)

