#!/bin/bash

if [ -z "$2" ]; then
    echo "Usage: $0 DAY PART [dev]"
    exit 1
fi

DAY=$(printf "%02d" "${1}")
PART=$2

PSQL_HEADER="
set track_io_timing to 'on';
set parallel_setup_cost to 10000000000;
set work_mem to '1GB';
set jit to off;
";

SCRIPTDIR="$(dirname $0)"
DAYDIR="${SCRIPTDIR}/day${DAY}"
SQL_FILE="${DAY}-${PART}.sql"
INPUT_FILE="${DAY}.input"

cd "${DAYDIR}"

for file in *ample*
do
    psql -Xq --file - <<__SQL__
${PSQL_HEADER}

\set input \`cat ${file}\`

PREPARE problem_${DAY}_${PART} AS
$(cat "${SQL_FILE}");

select 'Result for day ${DAY}, part ${PART}, ${file}' AS title;
EXECUTE problem_${DAY}_${PART} (:'input');
__SQL__
done

if [ "$3" = "dev" ]; then
    exit 0
fi

psql -Xq --file - <<__SQL__
${PSQL_HEADER}

\set input \`cat ${INPUT_FILE}\`

PREPARE problem_${DAY}_${PART} AS
$(cat "${SQL_FILE}");

select 'Result for year ${YEAR}, day ${DAY}, part ${PART}, ${INPUT_FILE}' AS title;
EXECUTE problem_${DAY}_${PART} (:'input');

\o ${DAY}-${PART}.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON, MEMORY ON) EXECUTE problem_${DAY}_${PART} (:'input');
\o
__SQL__

cat "${DAY}-${PART}.explain-analyze.txt"
