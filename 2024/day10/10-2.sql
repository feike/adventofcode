WITH RECURSIVE input AS (
    SELECT
        x::smallint,
        y::smallint,
        CASE WHEN h = '.' THEN -1 ELSE h::smallint END AS h
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS r(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rs(h, x)
    WHERE
        h ~ '[\d.]'
), walk AS (
    SELECT
        x AS s_x,
        y AS s_y,
        x,
        y,
        h
    FROM
        input
    WHERE
        h = 0

    UNION ALL

    SELECT
        s_x,
        s_y,
        n.x,
        n.y,
        n.h
    FROM
        walk AS w
    CROSS JOIN (
        VALUES
            (-1,  0),
            ( 1,  0),
            ( 0, -1),
            ( 0,  1)
        ) AS _(dx, dy)
    JOIN
        input AS n ON (
            n.h = w.h + 1
            AND n.x = w.x + dx
            AND n.y = w.y + dy
        )
)
SELECT
    count(*)
FROM
    walk
WHERE
    h = 9;