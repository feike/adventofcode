WITH RECURSIVE input AS (
    SELECT
        col::int,
        line_no,
        col_no,
        max(col_no) OVER (PARTITION BY line_no) AS nr_cols
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, line_no)
    CROSS JOIN
        regexp_split_to_table(line, '\s+') WITH ORDINALITY AS c(col, col_no)
    WHERE
        col ~ '\d+'
), permutations AS (
    SELECT
        line_no,
        skip,
        col_no,
        col
    FROM (
        SELECT
            line_no,
            skip,
            array_agg(col ORDER BY col_no) FILTER (WHERE col_no != skip) AS cols
        FROM
            input
        CROSS JOIN
            generate_series(0, nr_cols) AS _(skip)
        GROUP BY
            line_no,
            skip
        ) _
    CROSS JOIN
        unnest(cols) WITH ORDINALITY AS u(col, col_no)
), all_matches AS (
    SELECT
        a.line_no,
        a.skip
    FROM
        permutations a
    JOIN
        permutations b ON (a.line_no = b.line_no AND b.col_no = a.col_no + 1 AND a.skip = b.skip)
    GROUP BY
        a.skip,
        a.line_no
    HAVING
        (bool_and(a.col > b.col)
            OR bool_and(a.col < b.col)
        )
        AND bool_and(abs(a.col - b.col) BETWEEN 1 AND 3)
)
SELECT
    count(distinct line_no)
FROM
    all_matches;
