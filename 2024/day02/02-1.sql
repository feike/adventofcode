WITH RECURSIVE input AS (
    SELECT
        col::int,
        line_no,
        col_no
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, line_no)
    CROSS JOIN
        regexp_split_to_table(line, '\s+') WITH ORDINALITY AS c(col, col_no)
    WHERE
        col ~ '\d+'
)
SELECT
    count(a.line_no) OVER ()
FROM
    input a
JOIN
    input b ON (a.line_no = b.line_no AND b.col_no = a.col_no + 1)
GROUP BY
    a.line_no
HAVING
    (bool_and(a.col > b.col)
        OR bool_and(a.col < b.col)
    )
    AND bool_and(abs(a.col - b.col) BETWEEN 1 AND 3)
LIMIT
    1
;
