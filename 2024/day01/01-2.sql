WITH input AS (
    SELECT
        array_agg(fields[1]::int ORDER BY fields[1]::int, idx) AS a,
        array_agg(fields[2]::int ORDER BY fields[2]::int, idx) AS b
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, idx)
    CROSS JOIN
        regexp_split_to_array(line, '\s+') AS _s(fields)
    WHERE
        line ~ '\d+'
)
SELECT
    sum(x * count(*)) OVER ()
FROM
    input
CROSS JOIN
    unnest(a) AS _(x)
JOIN
    unnest(b) AS s(y) ON (x=y)
GROUP BY
    x
LIMIT
    1
;
