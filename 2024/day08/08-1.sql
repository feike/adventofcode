WITH RECURSIVE input AS (
    SELECT
        x,
        y,
        CASE
            WHEN v IN ('.', '#') THEN null
            ELSE v
        END AS freq
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS r(v, x)
    WHERE
        line ~ '.+'
), identical AS (
    SELECT
        a.x AS a_x,
        a.y AS a_y,
        b.x AS b_x,
        b.y AS b_y,
        a.x - (b.x - a.x) AS c_x,
        a.y - (b.y - a.y) AS c_y,
        b.x + (b.x - a.x) AS d_x,
        b.y + (b.y - a.y) AS d_y,
        a.freq
    FROM
        input a
    JOIN
        input b ON (
            a.freq = b.freq
            AND
                CASE
                    WHEN a.y = b.y THEN a.x < b.x
                    ELSE a.y < b.y
                END
        )
), antinodes AS (
    SELECT
        c_x AS x,
        c_y AS y,
        freq
    FROM
        identical
    
    UNION ALL

    SELECT
        d_x,
        d_y,
        freq
    FROM
        identical
), n_antinodes AS (
    SELECT
        y,
        x,
        count(*),
        array_agg(a.freq) AS freqs
    FROM
        antinodes AS a
    JOIN
        input USING (x, y)
    GROUP BY
        y,
        x
)
SELECT
    count(distinct (x, y))
FROM
    n_antinodes;
