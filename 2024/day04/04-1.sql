WITH RECURSIVE input AS MATERIALIZED (
    SELECT
        y,
        x,
        col
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS r(col, x)
), walk AS (
    SELECT
        x,
        y,
        col,
        dx,
        dy,
        'MAS' AS remaining,
        ARRAY[x] AS all_x,
        ARRAY[y] AS all_y
    FROM
        input
    CROSS JOIN
        ( VALUES
            ( 1,  0),
            (-1,  0),
            ( 0,  1),
            ( 0, -1),
            (-1, -1),
            ( 1,  1),
            ( 1, -1),
            (-1,  1)
        ) AS _(dx, dy)
    WHERE
        col = 'X'

    UNION ALL

    SELECT
        n.x,
        n.y,
        n.col,
        dx,
        dy,
        substring(remaining, 2, 4),
        all_x || w.x,
        all_y || w.y
    FROM
        walk AS w
    JOIN
        input AS n ON (
            w.x + dx = n.x
            AND w.y + dy = n.y
            AND substring(remaining, 1, 1) = n.col
        )
    WHERE
        length(w.remaining) > 0
)
SELECT
    count(*)
FROM
    walk
WHERE
    col = 'S';
