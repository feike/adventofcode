
WITH RECURSIVE input AS (
    SELECT
        y,
        x,
        CASE p
            WHEN '#' THEN true
            ELSE false
        END AS obstacle,
        p = '^' AS guard
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS r(p, x)
    WHERE
        line != ''
), walk AS (
    SELECT
        y,
        x,
        -1 AS d_y,
        0 AS d_x,
        0::int AS level
    FROM
        input
    WHERE
        guard

    UNION ALL

    SELECT
        CASE
            WHEN obstacle
            THEN w.y
            ELSE n.y
        END,
        CASE
            WHEN obstacle
            THEN w.x
            ELSE n.x
        END,
        CASE
            WHEN obstacle AND d_y != 0
            THEN 0
            WHEN obstacle AND d_x = 1
            THEN 1
            WHEN obstacle AND d_x = -1
            THEN -1
            ELSE w.d_y
        END,
        CASE
            WHEN obstacle AND d_x != 0
            THEN 0
            WHEN obstacle AND d_y = 1
            THEN -1
            WHEN obstacle AND d_y = -1
            THEN 1
            ELSE w.d_x
        END,
        level + 1
    FROM
        walk AS w
    JOIN
        input AS n ON (w.x+d_x = n.x AND w.y+d_y = n.y)
)
SELECT
    count(distinct (x, y))
FROM
    walk;
