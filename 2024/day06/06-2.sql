

WITH RECURSIVE input AS (
    SELECT
        y,
        x,
        CASE p
            WHEN '#' THEN true
            ELSE false
        END AS obstacle,
        p = '^' AS guard
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS r(p, x)
    WHERE
        line != ''
), out_of_bounds AS MATERIALIZED (
    SELECT
        max(x) + 1 as x_bound,
        max(y) + 1 as y_bound
    FROM
        input
), regular_walk AS (
    SELECT
        y,
        x,
        y AS prev_y,
        x AS prev_x,
        -1 AS d_y,
        0 AS d_x,
        0::int AS level,
        false::bool AS exit
    FROM
        input
    WHERE
        guard

    UNION ALL

    SELECT
        CASE
            WHEN n.y IS NULL
            THEN
                CASE
                    d_y
                    WHEN -1 THEN 0
                    WHEN  1 THEN (SELECT y_bound FROM out_of_bounds)
                    ELSE w.y
                END
            ELSE n.y - w.d_y
        END,
        CASE
            WHEN n.x IS NULL
            THEN
                CASE
                    d_x
                    WHEN -1 THEN 0
                    WHEN 1 THEN (SELECT x_bound FROM out_of_bounds)
                    ELSE w.x
                END
            ELSE n.x - w.d_x
        END,
        w.y,
        w.x,
        CASE
            WHEN d_y != 0
            THEN 0
            WHEN d_x = 1
            THEN  1
            ELSE -1
        END,
        CASE
            WHEN d_x != 0
            THEN 0
            WHEN d_y = 1
            THEN -1
            ELSE  1
        END,
        level + 1,
        n.x IS NULL
    FROM
        regular_walk AS w
    LEFT JOIN LATERAL
        (
            SELECT
                CASE
                    WHEN d_x = -1
                    THEN max(x)
                    ELSE min(x)
                END,
                CASE
                    WHEN d_y = -1
                    THEN max(y)
                    ELSE min(y)
                END
            FROM
                input AS i
            WHERE
                obstacle
                AND
                    CASE  w.d_x
                        WHEN  0 THEN i.x=w.x
                        WHEN -1 THEN i.x<w.x
                        ELSE i.x>w.x
                    END
                AND
                    CASE  w.d_y
                        WHEN  0 THEN i.y=w.y
                        WHEN -1 THEN i.y<w.y
                        ELSE i.y>w.y
                    END
        ) AS n(x, y) ON (n.x IS NOT NULL)
) CYCLE x, y, d_x, d_y SET is_cycle USING path
, travelled AS (
    SELECT
        i.y,
        i.x,
        CASE
            WHEN obstacle AND (bool_or(d_y != 0) OR bool_or(d_x != 0)) THEN 'x'
            WHEN obstacle THEN '#'
            WHEN guard THEN '@'
            WHEN bool_or(d_x != 0)
            THEN CASE
                WHEN bool_and(d_y = 0) AND bool_and(d_x = 1)
                THEN '^'
                WHEN bool_and(d_y = 0) AND bool_and(d_x = -1)
                THEN 'v'
                WHEN bool_or(d_y != 0)
                THEN '+'
                ELSE '?'
            END
            WHEN
                bool_or(d_y != 0)
                THEN CASE
                    WHEN bool_and(d_x = 0) AND bool_and(d_y = 1)
                    THEN '>'
                    WHEN bool_and(d_x = 0) AND bool_and(d_y = -1)
                    THEN '<'
                    ELSE '?'
                END
            ELSE '.'
        END AS travel
    FROM
        regular_walk AS rw
    LEFT JOIN
        generate_series(least(prev_x, x), greatest(prev_x, x), 1) AS a(x) ON (true)
    LEFT JOIN
        generate_series(least(prev_y, y), greatest(prev_y, y), 1) AS b(y) ON (true)
    RIGHT JOIN
        input AS i ON (i.y=b.y AND i.x=a.x)
    GROUP BY
        i.x,
        i.y,
        i.obstacle,
        i.guard
), map AS (
    SELECT
        y,
        string_agg(
            travel, ''
            ORDER BY x
        )
    FROM
        travelled
    GROUP BY
        y
    ORDER BY
        y
)
SELECT
    *
FROM
    map;
