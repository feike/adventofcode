
WITH RECURSIVE input AS (
    SELECT
        y,
        cols::int[],
        line ~ '\|' AS order_rule
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_array(line, '[|,]') AS r(cols)
    WHERE
        line ~ '\d'
), order_rules AS MATERIALIZED (
    SELECT
        y,
        cols[1] AS before,
        cols[2] AS after
    FROM
        input
    WHERE
        order_rule
), updates AS MATERIALIZED (
    SELECT
        y,
        cols
    FROM
        input
    WHERE
        NOT order_rule
), incorrect AS MATERIALIZED (
    SELECT
        rank() OVER (ORDER BY u.y) AS update,
        cols
    FROM
        updates AS u
    JOIN
        order_rules AS o ON (before = ANY(cols) AND after = ANY(cols))
    GROUP BY
        u.y,
        u.cols
    HAVING
        bool_or(
            array_position(cols, before) >= array_position(cols, after)
        )
), sequences AS (
    SELECT
        update,
        before AS current,
        ARRAY[before] AS visited,
        0::int AS level,
        false AS done
    FROM
        order_rules
    JOIN
        incorrect ON (before = ANY(cols))

    UNION ALL

    SELECT DISTINCT
        update,
        r.after,
        visited||r.after,
        level + 1,
        array_length(visited, 1) + 1 = array_length(cols, 1)
    FROM
        sequences AS s
    JOIN
        incorrect AS u USING (update)
    JOIN
        order_rules AS r ON (
            s.current=r.before
            AND NOT r.after = ANY(visited)
            AND r.after = ANY(cols)
        )
    WHERE
        NOT done
)
SELECT
    sum(visited[array_length(visited, 1)/2 + 1])
FROM
    sequences
WHERE
    done;