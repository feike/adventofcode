WITH input AS (
    SELECT
        y,
        cols::int[],
        line ~ '\|' AS order_rule
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_array(line, '[|,]') AS r(cols)
    WHERE
        line ~ '\d'
), order_rules AS MATERIALIZED (
    SELECT
        cols[1] AS before,
        cols[2] AS after
    FROM
        input
    WHERE
        order_rule
), updates AS MATERIALIZED (
    SELECT
        y,
        cols
    FROM
        input
    WHERE
        NOT order_rule
), compared AS (
    SELECT
        y,
        u.cols,
        array_position(cols, before) < array_position(cols, after) AS correct
    FROM
        updates AS u
    LEFT JOIN
        order_rules ON (before = ANY(cols) AND after = ANY(cols))
)
SELECT
    sum(cols[array_length(cols, 1)/2 + 1]) OVER ()
FROM
    compared
GROUP BY
    y,
    cols
HAVING
    bool_and(correct)
LIMIT
    1
;