use std::env;
use std::fs;
use std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::Path,
};

use std::collections::HashMap;

fn part1(input: &Vec<String>) {}

fn part2(input: &Vec<String>) {}

fn main() {
    let args: Vec<String> = env::args().collect();

    for i in args.iter().skip(1) {
        let file = File::open(i).expect("no such file");
        let buf = BufReader::new(file);
        let lines: Vec<String> = buf
            .lines()
            .map(|l| l.expect("Could not parse line"))
            .collect();
        part1(&lines);
        part2(&lines);
    }
}
