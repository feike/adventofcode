#![allow(dead_code)]

use std::cmp::Ordering;
use std::collections::HashMap;
use std::env;

use std::{
    fs::File,
    io::{prelude::*, BufReader},
};

#[derive(Debug)]
struct Room {
    encrypted_name: String,
    sector_id: usize,
    checksum: String,
}

impl Room {
    fn new(info: &str) -> Option<Room> {
        let mut f: Vec<&str> = info.split('-').collect();
        println!("{:?}", f);
        let checksum = f.pop().unwrap().to_string();
        let sector_id = f.pop().unwrap().parse::<usize>().unwrap();
        let encrypted_name = f.join("-");

        let room = Room {
            encrypted_name,
            sector_id,
            checksum,
        };

        if room.checksum() == room.checksum {
            Some(room)
        } else {
            None
        }
    }

    fn checksum(&self) -> String {
        let mut chars: HashMap<char, usize> = HashMap::with_capacity(10);

        for c in self.encrypted_name.chars() {
            *chars.entry(c).or_insert(1) += 2;
        }
        let mut count_vec: Vec<_> = chars.iter().collect();
        count_vec.sort_by(|a, b| match b.1.cmp(a.1) {
            Ordering::Less => Ordering::Less,
            Ordering::Greater => Ordering::Greater,
            Ordering::Equal => b.0.cmp(a.0),
        });

        count_vec.iter().take(5).map(|i| i.0).collect()
    }
}

fn part1(input: &[String]) {
    let rooms: Vec<Room> = input.iter().filter_map(|l| Room::new(l)).collect();
    println!("{0:?}", rooms);
}

fn part2(_input: &[String]) {}

fn main() {
    let args: Vec<String> = env::args().collect();

    for i in args.iter().skip(1) {
        let file = File::open(i).expect("no such file");
        let buf = BufReader::new(file);
        let lines: Vec<String> = buf
            .lines()
            .map(|l| l.expect("Could not parse line"))
            .collect();
        part1(&lines);
        part2(&lines);
    }
}
