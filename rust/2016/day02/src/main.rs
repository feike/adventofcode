use std::char;
use std::env;
use std::{
    fs::File,
    io::{prelude::*, BufReader},
};

struct Keypad {
    x: u32,
    y: u32,
    code: String,
}

impl Keypad {
    fn new() -> Keypad {
        Keypad {
            x: 1,
            y: 1,
            code: String::new(),
        }
    }

    fn follow_instruction(&mut self, instruction: &str) {
        for c in instruction.chars() {
            match c {
                'U' if self.y > 0 => {
                    self.y -= 1;
                }
                'L' if self.x > 0 => {
                    self.x -= 1;
                }
                'R' if self.x < 2 => {
                    self.x += 1;
                }
                'D' if self.y < 2 => {
                    self.y += 1;
                }
                _ => (),
            }
        }
        self.code
            .push(char::from_digit(1 + self.y * 3 + self.x, 10).unwrap());
    }

    fn code(&self) -> &str {
        self.code.as_str()
    }
}

static KEYPAD_NUMBERS: &[Option<char>] = &[
    None,
    None,
    Some('1'),
    None,
    None,
    None,
    Some('2'),
    Some('3'),
    Some('4'),
    None,
    Some('5'),
    Some('6'),
    Some('7'),
    Some('8'),
    Some('9'),
    None,
    Some('A'),
    Some('B'),
    Some('C'),
    None,
    None,
    None,
    Some('D'),
    None,
    None,
    None,
    None,
    None,
    None,
];

struct KeypadMeeting {
    x: usize,
    y: usize,
    code: String,
}

impl KeypadMeeting {
    fn follow_instruction(&mut self, instruction: &str) {
        for c in instruction.chars() {
            match c {
                'U' if self.y > 0 && KEYPAD_NUMBERS[(self.y - 1) * 5 + self.x].is_some() => {
                    self.y -= 1;
                }
                'L' if KEYPAD_NUMBERS[self.y * 5 + self.x - 1].is_some() => {
                    self.x -= 1;
                }
                'R' if KEYPAD_NUMBERS[self.y * 5 + self.x + 1].is_some() => {
                    self.x += 1;
                }
                'D' if KEYPAD_NUMBERS[(self.y + 1) * 5 + self.x].is_some() => {
                    self.y += 1;
                }
                _ => (),
            }
        }
        self.code.push(KEYPAD_NUMBERS[self.y * 5 + self.x].unwrap())
    }

    fn code(&self) -> &str {
        self.code.as_str()
    }
}

fn part1(input: &Vec<String>) {
    let mut keypad = Keypad::new();
    for line in input {
        keypad.follow_instruction(line.as_str());
    }
    println!("The code is {0}", keypad.code());
}

fn part2(input: &Vec<String>) {
    let mut keypad = KeypadMeeting {
        x: 0,
        y: 2,
        code: String::with_capacity(input.len()),
    };
    for line in input {
        keypad.follow_instruction(line.as_str());
    }
    println!("The code is {0}", keypad.code());
}

fn main() {
    let args: Vec<String> = env::args().collect();

    for i in args.iter().skip(1) {
        let file = File::open(i).expect("no such file");
        let buf = BufReader::new(file);
        let lines: Vec<String> = buf
            .lines()
            .map(|l| l.expect("Could not parse line"))
            .collect();
        part1(&lines);
        part2(&lines);
    }
}
