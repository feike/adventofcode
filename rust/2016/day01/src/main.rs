#![allow(dead_code)]
use std::collections::HashSet;
use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();

    for i in args.iter().skip(1) {
        println!("Reading file {}", &i);
        let contents = fs::read_to_string(i)
            .expect("Something went wrong reading the file")
            .trim_end()
            .to_string();

        part1(&contents);
        part2(&contents);
    }
}

enum Direction {
    North,
    East,
    South,
    West,
}

struct Position {
    x: isize,
    y: isize,
    dir: Direction,
}

impl Position {
    fn take_step(&mut self, steps: usize) -> Vec<(isize, isize)> {
        let (mut dx, mut dy) = (0, 0);
        match self.dir {
            Direction::North => dy = 1,
            Direction::East => dx = 1,
            Direction::South => dy = -1,
            Direction::West => dx = -1,
        };

        let mut coords: Vec<(isize, isize)> = Vec::with_capacity(steps);
        for _i in 0..steps {
            self.x += dx;
            self.y += dy;
            coords.push((self.x, self.y));
        }

        coords
    }

    fn take_turn(&mut self, rotation: char) {
        match rotation {
            'R' => {
                self.dir = match self.dir {
                    Direction::North => Direction::East,
                    Direction::East => Direction::South,
                    Direction::South => Direction::West,
                    Direction::West => Direction::North,
                }
            }
            'L' => {
                self.dir = match self.dir {
                    Direction::North => Direction::West,
                    Direction::East => Direction::North,
                    Direction::South => Direction::East,
                    Direction::West => Direction::South,
                }
            }
            _ => panic!("Invalid rotation"),
        }
    }

    fn distance(&self) -> isize {
        self.x.abs() + self.y.abs()
    }
}

fn part1(input: &str) {
    let (_x, _y, _dir, mut pos) = (
        0,
        0,
        Direction::North,
        Position {
            x: 0,
            y: 0,
            dir: Direction::North,
        },
    );

    let mut steps: usize = 0;

    for c in input.chars() {
        match c {
            'R' | 'L' => pos.take_turn(c),
            ',' => {
                pos.take_step(steps);
                steps = 0;
            }
            '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                steps = steps * 10 + c.to_digit(10).unwrap() as usize
            }
            _ => (),
        }
    }
    pos.take_step(steps);

    println!(
        "You are at location {0}, {1}, you therefore moved {2} blocks",
        pos.x,
        pos.y,
        pos.x.abs() + pos.y.abs()
    );
}

fn part2(input: &str) {
    let (_x, _y, _dir, mut pos) = (
        0,
        0,
        Direction::North,
        Position {
            x: 0,
            y: 0,
            dir: Direction::North,
        },
    );
    let mut visited: HashSet<(isize, isize)> = HashSet::new();
    visited.insert((pos.x, pos.y));
    let mut steps: usize = 0;

    for c in input.chars() {
        match c {
            'R' | 'L' => pos.take_turn(c),
            ',' => {
                for c in pos.take_step(steps) {
                    if !visited.insert(c) {
                        println!(
                            "Second time at location {0:?}, distance {1}",
                            c,
                            c.0.abs() + c.1.abs()
                        );
                        return;
                    }
                }
                steps = 0;
            }
            '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                steps = steps * 10 + c.to_digit(10).unwrap() as usize
            }
            _ => (),
        }
    }
    for c in pos.take_step(steps) {
        if !visited.insert(c) {
            println!(
                "Second time at location {0:?}, distance {1}",
                c,
                c.0.abs() + c.1.abs()
            );
            return;
        }
    }
}
