#![allow(clippy::if_same_then_else)]

use std::env;

use std::{
    fs::File,
    io::{prelude::*, BufReader},
};

fn is_triangle(line: &str) -> bool {
    let f: Vec<i32> = line
        .split_whitespace()
        .map(|s| s.parse::<i32>().unwrap())
        .collect();

    #[allow(clippy::needless_bool)]
    if f[0] + f[1] <= f[2] {
        false
    } else if f[1] + f[2] <= f[0] {
        false
    } else if f[2] + f[0] <= f[1] {
        false
    } else {
        true
    }
}

fn is_triangle_int(f: &[i32]) -> bool {
    #[allow(clippy::needless_bool)]
    if f[0] + f[1] <= f[2] {
        false
    } else if f[1] + f[2] <= f[0] {
        false
    } else if f[2] + f[0] <= f[1] {
        false
    } else {
        true
    }
}

fn part1(input: &[String]) {
    let valid: i32 = input
        .iter()
        .map(|l| if is_triangle(l) { 1 } else { 0 })
        .sum();
    println!("There are {0} valid triangles", valid);
}

fn part2(input: &[String]) {
    let mut a: [i32; 3] = [0, 0, 0];
    let mut b: [i32; 3] = [0, 0, 0];
    let mut c: [i32; 3] = [0, 0, 0];

    let mut valid: usize = 0;

    for (idx, line) in input.iter().enumerate() {
        let f: Vec<i32> = line
            .split_whitespace()
            .map(|s| s.parse::<i32>().unwrap())
            .collect();
        a[idx % 3] = f[0];
        b[idx % 3] = f[1];
        c[idx % 3] = f[2];
        if idx % 3 == 2 {
            if is_triangle_int(&a) {
                valid += 1;
            }
            if is_triangle_int(&b) {
                valid += 1;
            }
            if is_triangle_int(&c) {
                valid += 1;
            }
        }
    }
    println!("There are {0} valid triangles", valid);
}

fn main() {
    let args: Vec<String> = env::args().collect();

    for i in args.iter().skip(1) {
        let file = File::open(i).expect("no such file");
        let buf = BufReader::new(file);
        let lines: Vec<String> = buf
            .lines()
            .map(|l| l.expect("Could not parse line"))
            .collect();
        part1(&lines);
        part2(&lines);
    }
}
