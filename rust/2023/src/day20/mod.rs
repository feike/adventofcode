#![allow(unused_variables)]

use crate::AdventOfCode;

#[derive(Debug)]
pub struct Day20;

impl AdventOfCode for Day20 {
    fn solve_part1(&self, input: &str) -> String {
        todo!()
    }

    fn solve_part2(&self, input: &str) -> String {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use crate::{day20::Day20, AdventOfCode};

    struct TestCase {
        input: &'static str,
        output: &'static str,
    }

    #[test]
    fn day20_verify_part1() {
        let cases: Vec<_> = vec![
            TestCase {
                input: "",
                output: "todo",
            },
            TestCase {
                input: "",
                output: "todo",
            },
        ];

        let solver = Day20;
        for c in cases {
            assert_eq!(c.output, solver.solve_part1(c.input));
        }
    }

    #[test]
    fn day20_verify_part2() {
        let cases: Vec<_> = vec![
            TestCase {
                input: "",
                output: "todo",
            },
            TestCase {
                input: "",
                output: "todo",
            },
        ];

        let solver = Day20;
        for c in cases {
            assert_eq!(c.output, solver.solve_part2(c.input));
        }
    }
}
