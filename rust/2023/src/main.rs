use day1::Day1;
use day10::Day10;
use day11::Day11;
use day12::Day12;
use day13::Day13;
use day14::Day14;
use day15::Day15;
use day16::Day16;
use day17::Day17;
use day18::Day18;
use day19::Day19;
use day2::Day2;
use day20::Day20;
use day21::Day21;
use day22::Day22;
use day23::Day23;
use day24::Day24;
use day25::Day25;
use day3::Day3;
use day4::Day4;
use day5::Day5;
use day6::Day6;
use day7::Day7;
use day8::Day8;
use day9::Day9;

mod day1;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day2;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

trait AdventOfCode {
    fn solve_part1(&self, input: &str) -> String;
    fn solve_part2(&self, input: &str) -> String;
}

fn main() {
    let puzzles: Vec<Box<dyn AdventOfCode>> = vec![
        Box::new(Day1) as Box<dyn AdventOfCode>,
        Box::new(Day2) as Box<dyn AdventOfCode>,
        Box::new(Day3) as Box<dyn AdventOfCode>,
        Box::new(Day4) as Box<dyn AdventOfCode>,
        Box::new(Day5) as Box<dyn AdventOfCode>,
        Box::new(Day6) as Box<dyn AdventOfCode>,
        Box::new(Day7) as Box<dyn AdventOfCode>,
        Box::new(Day8) as Box<dyn AdventOfCode>,
        Box::new(Day9) as Box<dyn AdventOfCode>,
        Box::new(Day10) as Box<dyn AdventOfCode>,
        Box::new(Day11) as Box<dyn AdventOfCode>,
        Box::new(Day12) as Box<dyn AdventOfCode>,
        Box::new(Day13) as Box<dyn AdventOfCode>,
        Box::new(Day14) as Box<dyn AdventOfCode>,
        Box::new(Day15) as Box<dyn AdventOfCode>,
        Box::new(Day16) as Box<dyn AdventOfCode>,
        Box::new(Day17) as Box<dyn AdventOfCode>,
        Box::new(Day18) as Box<dyn AdventOfCode>,
        Box::new(Day19) as Box<dyn AdventOfCode>,
        Box::new(Day20) as Box<dyn AdventOfCode>,
        Box::new(Day21) as Box<dyn AdventOfCode>,
        Box::new(Day22) as Box<dyn AdventOfCode>,
        Box::new(Day23) as Box<dyn AdventOfCode>,
        Box::new(Day24) as Box<dyn AdventOfCode>,
        Box::new(Day25) as Box<dyn AdventOfCode>,
    ];

    let args: Vec<String> = std::env::args().collect();

    let day = args.get(1).expect("missing day argument");
    let day = day.parse::<usize>().expect("not a valid integer for day");

    let puzzle = puzzles.get(day + 1).expect("day out of range");
    let path = format!("src/day{day}/input.txt");
    let input = std::fs::read_to_string(path)
        .expect("could not read input.txt for given day");

    let mut parts: Vec<usize> = vec![];
    match args.get(2).map(|a| a.parse::<u128>().unwrap()) {
        Some(1) => parts.push(1),
        Some(2) => parts.push(2),
        Some(_) => panic!("Invalid part: either 1 or 2"),
        None => {
            parts.push(1);
            parts.push(2);
        }
    }

    for part in parts {
        if part == 1 {
            println!("Running puzzle for Day {day}, part {part}");
            puzzle.solve_part1(&input);
        }
        if part == 2 {
            println!("Running puzzle for Day {day}, part {part}");
            puzzle.solve_part2(&input);
        }
    }
}
