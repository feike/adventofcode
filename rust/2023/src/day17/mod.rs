#![allow(unused_variables)]

use crate::AdventOfCode;

#[derive(Debug)]
pub struct Day17;

impl AdventOfCode for Day17 {
    fn solve_part1(&self, input: &str) -> String {
        todo!()
    }

    fn solve_part2(&self, input: &str) -> String {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use crate::{day17::Day17, AdventOfCode};

    struct TestCase {
        input: &'static str,
        output: &'static str,
    }

    #[test]
    fn day17_verify_part1() {
        let cases: Vec<_> = vec![
            TestCase {
                input: "",
                output: "todo",
            },
            TestCase {
                input: "",
                output: "todo",
            },
        ];

        let solver = Day17;
        for c in cases {
            assert_eq!(c.output, solver.solve_part1(c.input));
        }
    }

    #[test]
    fn day17_verify_part2() {
        let cases: Vec<_> = vec![
            TestCase {
                input: "",
                output: "todo",
            },
            TestCase {
                input: "",
                output: "todo",
            },
        ];

        let solver = Day17;
        for c in cases {
            assert_eq!(c.output, solver.solve_part2(c.input));
        }
    }
}
