#![allow(unused_variables)]

use crate::AdventOfCode;

#[derive(Debug)]
pub struct Day5;

impl AdventOfCode for Day5 {
    fn solve_part1(&self, input: &str) -> String {
        todo!()
    }

    fn solve_part2(&self, input: &str) -> String {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use crate::{day5::Day5, AdventOfCode};

    struct TestCase {
        input: &'static str,
        output: &'static str,
    }

    #[test]
    fn day5_verify_part1() {
        let cases: Vec<_> = vec![
            TestCase {
                input: "",
                output: "todo",
            },
            TestCase {
                input: "",
                output: "todo",
            },
        ];

        let solver = Day5;
        for c in cases {
            assert_eq!(c.output, solver.solve_part1(c.input));
        }
    }

    #[test]
    fn day5_verify_part2() {
        let cases: Vec<_> = vec![
            TestCase {
                input: "",
                output: "todo",
            },
            TestCase {
                input: "",
                output: "todo",
            },
        ];

        let solver = Day5;
        for c in cases {
            assert_eq!(c.output, solver.solve_part2(c.input));
        }
    }
}
