#![allow(unused_variables)]

use crate::AdventOfCode;

#[derive(Debug)]
pub struct Day21;

impl AdventOfCode for Day21 {
    fn solve_part1(&self, input: &str) -> String {
        todo!()
    }

    fn solve_part2(&self, input: &str) -> String {
        todo!()
    }
}

#[cfg(test)]
mod tests {
    use crate::{day21::Day21, AdventOfCode};

    struct TestCase {
        input: &'static str,
        output: &'static str,
    }

    #[test]
    fn day21_verify_part1() {
        let cases: Vec<_> = vec![
            TestCase {
                input: "",
                output: "todo",
            },
            TestCase {
                input: "",
                output: "todo",
            },
        ];

        let solver = Day21;
        for c in cases {
            assert_eq!(c.output, solver.solve_part1(c.input));
        }
    }

    #[test]
    fn day21_verify_part2() {
        let cases: Vec<_> = vec![
            TestCase {
                input: "",
                output: "todo",
            },
            TestCase {
                input: "",
                output: "todo",
            },
        ];

        let solver = Day21;
        for c in cases {
            assert_eq!(c.output, solver.solve_part2(c.input));
        }
    }
}
