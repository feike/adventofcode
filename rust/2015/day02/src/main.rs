use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();

    for i in args.iter().skip(1) {
        println!("Reading file {}", &i);
        let contents = fs::read_to_string(i)
            .expect("Something went wrong reading the file")
            .trim_end()
            .to_string();

        part1(&contents);
        part2(&contents);
    }
}

fn part1(input: &str) {
    let mut size = 0;
    for line in input.split_whitespace() {
        // By sorting, we ensure that the first two elements contain the smallest side,
        // which we need to have 3x the wrapping paper for instead of 2
        let mut dims: Vec<isize> = line
            .split('x')
            .map(|x| x.parse::<isize>().unwrap())
            .collect();
        dims.sort();

        size += dims[0] * dims[1] * 3 + dims[0] * dims[2] * 2 + dims[1] * dims[2] * 2;
    }
    println!("Required square feet of wrapping paper: {}", size);
}

fn part2(input: &str) {
    let mut size = 0;
    for line in input.split_whitespace() {
        // By sorting, we ensure that the first two elements contain the smallest side,
        // which we need to have 3x the wrapping paper for instead of 2
        let mut dims: Vec<isize> = line
            .split('x')
            .map(|x| x.parse::<isize>().unwrap())
            .collect();
        dims.sort();

        size += dims[0] * 2 + dims[1] * 2 + dims[0] * dims[1] * dims[2];
    }
    println!("Required amount of ribbon: {}", size);
}
