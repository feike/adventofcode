use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();

    for i in args.iter().skip(1) {
        println!("Reading file {}", &i);
        let contents = fs::read_to_string(i)
            .expect("Something went wrong reading the file")
            .trim_end()
            .to_string();

        part1(&contents);
        part2(&contents);
    }
}

fn part1(_input: &str) {
    println!("Part 1")
}

fn part2(_input: &str) {
    println!("Part 2")
}
