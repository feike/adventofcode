use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();

    for i in args.iter().skip(1) {
        println!("Reading file {}", &i);
        let contents = fs::read_to_string(i)
            .expect("Something went wrong reading the file")
            .trim_end()
            .to_string();

        part1(&contents);
        part2(&contents);
    }
}

fn part1(input: &str) {
    let mut floor = 0;
    for c in input.chars() {
        match c {
            '(' => floor += 1,
            ')' => floor -= 1,
            _ => panic!("Unknown character"),
        }
    }
    println!("Result floor: {}", floor);
}

fn part2(input: &str) {
    let mut floor = 0;
    for (i, c) in input.chars().enumerate() {
        match c {
            '(' => floor += 1,
            ')' => floor -= 1,
            _ => panic!("Unknown character"),
        }
        if floor == -1 {
            println!("Character position when reaching floor -1: {}", i + 1);
            break;
        }
    }
}
