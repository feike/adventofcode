use std::collections::HashMap;
use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();

    for i in args.iter().skip(1) {
        println!("Reading file {}", &i);
        let contents = fs::read_to_string(i)
            .expect("Something went wrong reading the file")
            .trim_end()
            .to_string();

        part1(&contents);
        part2(&contents);
    }
}

fn part1(input: &str) {
    let mut visited: HashMap<(isize, isize), usize> = HashMap::new();
    let mut location = (0, 0);
    visited.insert(location, 1);
    for direction in input.chars().map(|x| match x {
        '^' => (0, 1),
        'v' => (0, -1),
        '>' => (1, 0),
        '<' => (-1, 0),
        _ => panic!("Unknown character: {}", x),
    }) {
        location = (location.0 + direction.0, location.1 + direction.1);
        let counter = visited.entry(location).or_insert(0);
        *counter += 1;
    }
    println!("Houses visited: {}", visited.len());
}

fn part2(input: &str) {
    let mut visited: HashMap<(isize, isize), usize> = HashMap::new();
    let mut locations = [(0, 0), (0, 0)];
    visited.insert(locations[0], 2);
    for (idx, direction) in input
        .chars()
        .map(|x| match x {
            '^' => (0, 1),
            'v' => (0, -1),
            '>' => (1, 0),
            '<' => (-1, 0),
            _ => panic!("Unknown character: {}", x),
        })
        .enumerate()
    {
        let agent = idx % locations.len();
        locations[agent] = (
            locations[agent].0 + direction.0,
            locations[agent].1 + direction.1,
        );
        let counter = visited.entry(locations[agent]).or_insert(0);
        *counter += 1;
    }
    println!("Houses visited: {}", visited.len());
}
