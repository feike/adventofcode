use crate::AdventOfCode;

pub struct Puzzle {}

impl AdventOfCode for Puzzle {
    fn solve(&self, part: usize, input: &str) -> Result<String, String> {
        if part == 0 {
            part1(input)
        } else {
            part2(input)
        }
    }
}

fn part1(_input: &str) -> Result<String, String> {
    Err("unimplemented!".to_owned())
}

fn part2(_input: &str) -> Result<String, String> {
    Err("unimplemented!".to_owned())
}
