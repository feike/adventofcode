use crate::AdventOfCode;

pub struct Puzzle {}

impl AdventOfCode for Puzzle {
    fn solve(&self, part: usize, input: &str) -> Result<String, String> {
        if part == 1 {
            Ok(part1(input))
        } else {
            Ok(part2(input))
        }
    }
}

fn part1(input: &str) -> String {
    let mut high = 0;
    let mut current = 0;
    for line in input.lines() {
        if line.trim().is_empty() {
            high = std::cmp::max(high, current);
            current = 0;
        } else {
            let calories = line.parse::<usize>().unwrap();
            current += calories;
        }
    }
    format!("{high}")
}

fn part2(input: &str) -> String {
    fn process_line(line: &str, high: &mut [usize], current: &mut usize) {
        println!("Hey! {line}: {high:?}, {current}");
        if line.trim().is_empty() {
            for h in high {
                if *h <= *current {
                    *h = *current;
                    println!("Gotcha!: {current}, {h:?}");
                    break;
                }
            }
            *current = 0;
        } else {
            let calories = line.parse::<usize>().unwrap();
            *current += calories;
        }
    }

    let mut high: [usize; 3] = [0, 0, 0];
    let mut current = 0;

    for line in input.lines() {
        process_line(line, &mut high, &mut current);
    }
    process_line("", &mut high, &mut current);

    format!("{high:?}")
}
