#![deny(clippy::all, clippy::pedantic, clippy::suspicious)]
#![warn(clippy::complexity, clippy::perf, clippy::nursery)]
#![allow(clippy::option_if_let_else)] // I think this linter is not great for readability
#![allow(clippy::cast_sign_loss)]
#![allow(clippy::cast_possible_wrap)]

use std::collections::HashMap;

mod day1;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day2;
mod day20;
mod day21;
mod day22;
mod day23;
mod day24;
mod day25;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

trait AdventOfCode: Sync + Send {
    fn solve(&self, part: usize, input: &str) -> Result<String, String>;
}

fn main() {
    let mut puzzles: HashMap<usize, Box<dyn AdventOfCode>> = HashMap::with_capacity(25);
    puzzles.insert(1, Box::new(day1::Puzzle {}));
    puzzles.insert(2, Box::new(day2::Puzzle {}));
    puzzles.insert(3, Box::new(day3::Puzzle {}));
    puzzles.insert(4, Box::new(day4::Puzzle {}));
    puzzles.insert(5, Box::new(day5::Puzzle {}));
    puzzles.insert(6, Box::new(day6::Puzzle {}));
    puzzles.insert(7, Box::new(day7::Puzzle {}));
    puzzles.insert(8, Box::new(day8::Puzzle {}));
    puzzles.insert(9, Box::new(day9::Puzzle {}));
    puzzles.insert(10, Box::new(day10::Puzzle {}));
    puzzles.insert(11, Box::new(day11::Puzzle {}));
    puzzles.insert(12, Box::new(day12::Puzzle {}));
    puzzles.insert(13, Box::new(day13::Puzzle {}));
    puzzles.insert(14, Box::new(day14::Puzzle {}));
    puzzles.insert(15, Box::new(day15::Puzzle {}));
    puzzles.insert(16, Box::new(day16::Puzzle {}));
    puzzles.insert(17, Box::new(day17::Puzzle {}));
    puzzles.insert(18, Box::new(day18::Puzzle {}));
    puzzles.insert(19, Box::new(day19::Puzzle {}));
    puzzles.insert(20, Box::new(day20::Puzzle {}));
    puzzles.insert(21, Box::new(day21::Puzzle {}));
    puzzles.insert(22, Box::new(day22::Puzzle {}));
    puzzles.insert(23, Box::new(day23::Puzzle {}));
    puzzles.insert(24, Box::new(day24::Puzzle {}));
    puzzles.insert(25, Box::new(day25::Puzzle {}));

    for arg in std::env::args().skip(1) {
        let idx = arg.parse::<usize>().unwrap();
        let puzzle = puzzles.get(&idx).unwrap();
        let input = std::fs::read_to_string(format!("input/day{idx}")).unwrap();
        for part in [1, 2] {
            match puzzle.solve(part, &input) {
                Ok(ok) => println!("==== Day {idx}, part {part}: OK ====\n{ok}"),
                Err(err) => println!("==== Day {idx}, part {part}: ERROR ====\n{err}"),
            }
        }
    }
}
