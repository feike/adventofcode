# Advent of Code

This is my attempt at solving some (perhaps even most) puzzles of [Advent of Code](https://adventofcode.com/).

# My rules of SQL coding

I'm trying to solve all challenges using only SQL, with some help of utility functions (like generate\_series).
As this definition is probably open for interpretation, this is my validation environment:

All these solutions can run on a PostgreSQL cluster in replication, on a pristine template1 database using a
regular (non-superuser) user.
