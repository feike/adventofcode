DEALLOCATE problem3_1;

-- I could also use the point and box datatypes, with some of their overlap operators
-- I decided against it for now
PREPARE problem3_1 AS
WITH input AS (
    SELECT
        line,
        matches[1] AS itemno,
        x,
        y
    FROM
        regexp_split_to_table(trim($1, E' \n'), '\n') WITH ORDINALITY AS sub(line, lineno)
    CROSS JOIN
        regexp_matches(line, '#(\d+)\s+@\s+(\d+),(\d+):\s+(\d+)x(\d+)') AS matches
    CROSS JOIN
        generate_series(matches[2]::int, matches[2]::int+matches[4]::int-1) AS x(x)
    CROSS JOIN
        generate_series(matches[3]::int, matches[3]::int+matches[5]::int-1) AS y(y)
)
SELECT
    x,
    y,
    count(*)
FROM
    input
GROUP BY
    x,
    y
HAVING
    count(*) > 1
;

EXECUTE problem3_1($$#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2$$);

\set input `cat 3.input`
EXECUTE problem3_1(:'input');