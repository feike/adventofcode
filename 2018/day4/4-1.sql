DEALLOCATE problem4_1;

PREPARE problem4_1 AS
-- First extract all the data without any processing
WITH input AS (
    SELECT
        matches[1]::timestamptz AS time,
        matches[3]::int AS guard,
        CASE
            WHEN matches[2] = 'falls asleep'
            THEN false
            ELSE true
        END AS is_awake
    FROM
        regexp_split_to_table(trim($1, E' \n'), '\n') WITH ORDINALITY AS sub(line, lineno)
    CROSS JOIN
        regexp_matches(line, '\[(.*)\]\s+(wakes up|falls asleep|Guard #(\d+) begins shift)') AS matches
),  ranges AS (
    SELECT
        time::date AS day,
        int4range(
            extract('minute' FROM time)::int,
            coalesce(
                extract('minute' FROM lead(time) OVER (PARTITION BY guard, time::date ORDER BY time))::int - 1,
                60),
            '[)') AS period,
        coalesce(
            guard,
            (SELECT
                guard
            FROM
                input b
            WHERE
                b.time < a.time 
                AND guard NOTNULL
            ORDER BY
                time DESC
            LIMIT
                1)) AS guard,
        is_awake
    FROM
        input a
    ORDER BY
        a.time
)
SELECT
    guard * minute
FROM
    ranges
JOIN
    (
        SELECT
            guard,
            is_awake,
            sum(upper(period) - lower(period))
        FROM
            ranges
        WHERE
            NOT is_awake
        GROUP BY
            guard,
            is_awake
        ORDER BY
            sum(upper(period) - lower(period)) DESC
        LIMIT
            1
    ) laziest_guard USING (guard, is_awake)
CROSS JOIN
    generate_series(lower(period), upper(period)) AS s(minute)
GROUP BY
    guard,
    minute
ORDER BY
    count(*) DESC
LIMIT
    1
;

EXECUTE problem4_1($$[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up$$);

\set input `cat 4.input`
EXECUTE problem4_1(:'input');