DEALLOCATE problem4_2;

PREPARE problem4_2 AS
-- First extract all the data without any processing
WITH input AS (
    SELECT
        matches[1]::timestamptz AS time,
        matches[3]::int AS guard,
        CASE
            WHEN matches[2] = 'falls asleep'
            THEN false
            ELSE true
        END AS is_awake
    FROM
        regexp_split_to_table(trim($1, E' \n'), '\n') WITH ORDINALITY AS sub(line, lineno)
    CROSS JOIN
        regexp_matches(line, '\[(.*)\]\s+(wakes up|falls asleep|Guard #(\d+) begins shift)') AS matches
),  ranges AS (
    SELECT
        time::date AS day,
        int4range(
            extract('minute' FROM time)::int,
            coalesce(
                extract('minute' FROM lead(time) OVER (PARTITION BY guard, time::date ORDER BY time))::int - 1,
                60),
            '[)') AS period,
        coalesce(
            guard,
            (SELECT
                guard
            FROM
                input b
            WHERE
                b.time < a.time 
                AND guard NOTNULL
            ORDER BY
                time DESC
            LIMIT
                1)) AS guard,
        is_awake
    FROM
        input a
    ORDER BY
        a.time
)
SELECT
    guard * minute
FROM
    ranges
CROSS JOIN
    generate_series(lower(period), upper(period)) AS s(minute)
WHERE
    NOT is_awake
GROUP BY
    guard,
    minute
ORDER BY
    count(*) DESC
LIMIT
    1
;

EXECUTE problem4_2($$[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up$$);

\set input `cat 4.input`
EXECUTE problem4_2(:'input');

/*
EXPLAIN (ANALYZE) EXECUTE problem4_2(:'input');

                                                                     QUERY PLAN
-----------------------------------------------------------------------------------------------------------------------------------------------------
 Limit  (cost=101075.59..101075.59 rows=1 width=20) (actual time=125.661..125.662 rows=1 loops=1)
   CTE input
     ->  Nested Loop  (cost=0.01..42.50 rows=1000 width=13) (actual time=0.976..46.645 rows=1067 loops=1)
           ->  Function Scan on regexp_split_to_table sub  (cost=0.00..10.00 rows=1000 width=32) (actual time=0.904..1.035 rows=1067 loops=1)
           ->  Function Scan on regexp_matches matches  (cost=0.00..0.01 rows=1 width=32) (actual time=0.041..0.041 rows=1 loops=1067)
   CTE ranges
     ->  Sort  (cost=24324.66..24327.16 rows=1000 width=53) (actual time=121.769..121.813 rows=1067 loops=1)
           Sort Key: a."time"
           Sort Method: quicksort  Memory: 132kB
           ->  WindowAgg  (cost=72.33..24274.83 rows=1000 width=53) (actual time=48.219..121.465 rows=1067 loops=1)
                 ->  Sort  (cost=72.33..74.83 rows=1000 width=17) (actual time=48.142..48.305 rows=1067 loops=1)
                       Sort Key: a.guard, ((a."time")::date), a."time"
                       Sort Method: quicksort  Memory: 132kB
                       ->  CTE Scan on input a  (cost=0.00..22.50 rows=1000 width=17) (actual time=0.978..47.279 rows=1067 loops=1)
                 SubPlan 2
                   ->  Limit  (cost=24.16..24.16 rows=1 width=12) (actual time=0.089..0.089 rows=1 loops=800)
                         ->  Sort  (cost=24.16..24.99 rows=332 width=12) (actual time=0.088..0.088 rows=1 loops=800)
                               Sort Key: b."time" DESC
                               Sort Method: top-N heapsort  Memory: 25kB
                               ->  CTE Scan on input b  (cost=0.00..22.50 rows=332 width=12) (actual time=0.002..0.076 rows=133 loops=800)
                                     Filter: ((guard IS NOT NULL) AND ("time" < a."time"))
                                     Rows Removed by Filter: 934
   ->  Sort  (cost=76705.93..76802.93 rows=38800 width=20) (actual time=125.660..125.660 rows=1 loops=1)
         Sort Key: (count(*)) DESC
         Sort Method: top-N heapsort  Memory: 25kB
         ->  GroupAggregate  (cost=71026.93..76511.93 rows=38800 width=20) (actual time=124.746..125.545 rows=1130 loops=1)
               Group Key: ranges.guard, s.minute
               ->  Sort  (cost=71026.93..72276.93 rows=500000 width=8) (actual time=124.741..124.956 rows=6091 loops=1)
                     Sort Key: ranges.guard, s.minute
                     Sort Method: quicksort  Memory: 478kB
                     ->  Nested Loop  (cost=0.01..10020.01 rows=500000 width=8) (actual time=121.785..123.228 rows=6091 loops=1)
                           ->  CTE Scan on ranges  (cost=0.00..20.00 rows=500 width=36) (actual time=121.773..122.010 rows=400 loops=1)
                                 Filter: (NOT is_awake)
                                 Rows Removed by Filter: 667
                           ->  Function Scan on generate_series s  (cost=0.01..10.01 rows=1000 width=4) (actual time=0.001..0.002 rows=15 loops=400)
 Planning Time: 0.477 ms
 Execution Time: 125.800 ms

*/