DEALLOCATE problem6_2;

PREPARE problem6_2 AS
WITH input (vals, id, x, y) AS (
    SELECT
        vals,
        id,
        coordinates[1]::integer,
        coordinates[2]::integer
    FROM
        regexp_split_to_table(trim($1, E' \n'), '\n') WITH ordinality AS vals(vals, id)
    CROSS JOIN
        regexp_matches(vals, '(\d+),\s+(\d+)') AS coordinates
), boundaries (minx, maxx, miny, maxy) AS (
    SELECT
        min(x) - 1,
        max(x) + 1,
        min(y) - 1,
        max(y) + 1
    FROM
        input
),
/* By using generate_series and a lateral left join I'm trying to achieve
   that this query does not take any large space complexity, only time
   complexity
*/
area AS (
    SELECT
        fx,
        fy,
        sum(abs(x - fx) + abs(y - fy)) AS manhattan_distance
    FROM
        boundaries
    CROSS JOIN
        generate_series(minx, maxx) as gs1(fx)
    CROSS JOIN
        generate_series(miny, maxy) as gs2(fy)
    CROSS JOIN
        input
    GROUP BY
        fx,
        fy
    -- commented out to help in visualizing
    -- HAVING
    --    sum(abs(x - fx) + abs(y - fy)) < $2
)
/* To visualize:

SELECT
    string_agg(CASE WHEN manhattan_distance < $2 THEN 'X' ELSE ' ' END, ''  ORDER BY fx)
FROM
    area
GROUP BY
    fy
ORDER BY
    fy
;
*/
SELECT
    count(*)
FROM
    area
WHERE
    manhattan_distance < $2
;

EXECUTE problem6_2($$1, 1
1, 6
8, 3
3, 4
5, 5
8, 9$$, 32);


\set input `cat 6.input`
EXECUTE problem6_2(:'input', 10000);

/*
EXPLAIN (ANALYZE, BUFFERS) EXECUTE problem6_2(:'input', 10000);

                                                                          QUERY PLAN
--------------------------------------------------------------------------------------------------------------------------------------------------------------
 Aggregate  (cost=32523943.38..32523943.39 rows=1 width=8) (actual time=1856.540..1856.540 rows=1 loops=1)
   CTE input
     ->  Nested Loop  (cost=0.01..40.00 rows=1000 width=48) (actual time=0.061..0.312 rows=50 loops=1)
           ->  Function Scan on regexp_split_to_table vals  (cost=0.00..10.00 rows=1000 width=40) (actual time=0.040..0.044 rows=50 loops=1)
           ->  Function Scan on regexp_matches coordinates  (cost=0.00..0.01 rows=1 width=32) (actual time=0.005..0.005 rows=1 loops=50)
   CTE boundaries
     ->  Aggregate  (cost=30.00..30.02 rows=1 width=16) (actual time=0.277..0.277 rows=1 loops=1)
           ->  CTE Scan on input  (cost=0.00..20.00 rows=1000 width=8) (actual time=0.000..0.265 rows=50 loops=1)
   CTE area
     ->  HashAggregate  (cost=32522540.03..32522940.03 rows=40000 width=16) (actual time=1814.007..1833.259 rows=94743 loops=1)
           Group Key: gs1.fx, gs2.fy
           ->  Nested Loop  (cost=0.01..12522540.03 rows=1000000000 width=16) (actual time=0.372..686.201 rows=4737150 loops=1)
                 ->  CTE Scan on input input_1  (cost=0.00..20.00 rows=1000 width=8) (actual time=0.062..0.188 rows=50 loops=1)
                 ->  Materialize  (cost=0.01..25020.03 rows=1000000 width=8) (actual time=0.006..4.566 rows=94743 loops=50)
                       ->  Nested Loop  (cost=0.01..20020.03 rows=1000000 width=8) (actual time=0.308..20.103 rows=94743 loops=1)
                             ->  Nested Loop  (cost=0.00..20.02 rows=1000 width=12) (actual time=0.294..0.412 rows=319 loops=1)
                                   ->  CTE Scan on boundaries  (cost=0.00..0.02 rows=1 width=16) (actual time=0.278..0.279 rows=1 loops=1)
                                   ->  Function Scan on generate_series gs1  (cost=0.00..10.00 rows=1000 width=4) (actual time=0.014..0.067 rows=319 loops=1)
                             ->  Function Scan on generate_series gs2  (cost=0.00..10.00 rows=1000 width=4) (actual time=0.015..0.032 rows=297 loops=319)
   ->  CTE Scan on area  (cost=0.00..900.00 rows=13333 width=0) (actual time=1814.010..1854.151 rows=46554 loops=1)
         Filter: (manhattan_distance < '10000'::bigint)
         Rows Removed by Filter: 48189
*/