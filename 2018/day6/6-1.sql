DEALLOCATE problem6_1;

PREPARE problem6_1 AS
WITH input (vals, id, x, y) AS (
    SELECT
        vals,
        id,
        coordinates[1]::integer,
        coordinates[2]::integer
    FROM
        regexp_split_to_table(trim($1, E' \n'), '\n') WITH ordinality AS vals(vals, id)
    CROSS JOIN
        regexp_matches(vals, '(\d+),\s+(\d+)') AS coordinates
), boundaries (minx, maxx, miny, maxy) AS (
    SELECT
        min(x) - 1,
        max(x) + 1,
        min(y) - 1,
        max(y) + 1
    FROM
        input
),
/* By using generate_series and a lateral left join I'm trying to achieve
   that this query does not take any large space complexity, only time
   complexity
*/
area AS (
    SELECT
        fx,
        fy,
        array_agg(id) AS ids
    FROM
        boundaries
    CROSS JOIN
        generate_series(minx, maxx) as gs1(fx)
    CROSS JOIN
        generate_series(miny, maxy) as gs2(fy)
    LEFT JOIN LATERAL
        (SELECT
            id,
            rank() OVER (ORDER BY abs(fx - i.x) + abs(fy - i.y) ASC)
        FROM
            input AS i
        ) closest
        ON (rank = 1 OR rank IS NULL)
    GROUP BY
        fx,
        fy
)
/* To visualize:

SELECT
    string_agg(CASE WHEN array_length(ids, 1) = 1 THEN v[ids[1]] ELSE '.' END, '' ORDER BY fx)
FROM
    area
CROSS JOIN
    regexp_split_to_array('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890-=[],./<>?', '') as vis(v)
GROUP BY
    fy
ORDER BY
    fy
;
*/
SELECT
    ids,
    count(*)
FROM
    area
CROSS JOIN
    boundaries
WHERE
    array_length(ids, 1) = 1
GROUP BY
    ids,
    minx,
    maxx,
    miny,
    maxy
HAVING
    min(fx) > minx
    AND min(fy) > miny
    AND max(fx) < maxx
    AND max(fy) < maxy
ORDER BY
    count(*) DESC
LIMIT
    1;

EXECUTE problem6_1($$1, 1
1, 6
8, 3
3, 4
5, 5
8, 9$$);

\set input `cat 6.input`
EXECUTE problem6_1(:'input');

/*
EXPLAIN (ANALYZE, BUFFERS) EXECUTE problem6_1(:'input');

                                                                               QUERY PLAN
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Limit  (cost=126213204.02..126213204.02 rows=1 width=56) (actual time=4906.991..4906.994 rows=1 loops=1)
   CTE input
     ->  Nested Loop  (cost=0.01..40.00 rows=1000 width=48) (actual time=0.064..0.342 rows=50 loops=1)
           ->  Function Scan on regexp_split_to_table vals  (cost=0.00..10.00 rows=1000 width=40) (actual time=0.041..0.045 rows=50 loops=1)
           ->  Function Scan on regexp_matches coordinates  (cost=0.00..0.01 rows=1 width=32) (actual time=0.005..0.005 rows=1 loops=50)
   CTE boundaries
     ->  Aggregate  (cost=30.00..30.02 rows=1 width=16) (actual time=0.371..0.371 rows=1 loops=1)
           ->  CTE Scan on input  (cost=0.00..20.00 rows=1000 width=8) (actual time=0.065..0.358 rows=50 loops=1)
   CTE area
     ->  GroupAggregate  (cost=126111616.28..126212116.28 rows=40000 width=40) (actual time=4710.697..4755.300 rows=94743 loops=1)
           Group Key: gs1.fx, gs2.fy
           ->  Sort  (cost=126111616.28..126136616.28 rows=10000000 width=16) (actual time=4710.683..4715.528 rows=97451 loops=1)
                 Sort Key: gs1.fx, gs2.fy
                 Sort Method: quicksort  Memory: 7641kB
                 ->  Nested Loop Left Join  (cost=82.33..124948941.45 rows=10000000 width=16) (actual time=0.057..4688.171 rows=97451 loops=1)
                       ->  Nested Loop  (cost=0.01..20020.03 rows=1000000 width=8) (actual time=0.032..27.686 rows=94743 loops=1)
                             ->  Nested Loop  (cost=0.00..20.02 rows=1000 width=12) (actual time=0.017..0.292 rows=319 loops=1)
                                   ->  CTE Scan on boundaries boundaries_1  (cost=0.00..0.02 rows=1 width=16) (actual time=0.000..0.001 rows=1 loops=1)
                                   ->  Function Scan on generate_series gs1  (cost=0.00..10.00 rows=1000 width=4) (actual time=0.016..0.167 rows=319 loops=1)
                             ->  Function Scan on generate_series gs2  (cost=0.00..10.00 rows=1000 width=4) (actual time=0.020..0.046 rows=297 loops=319)
                       ->  Subquery Scan on closest  (cost=82.33..124.83 rows=10 width=8) (actual time=0.018..0.049 rows=1 loops=94743)
                             Filter: ((closest.rank = 1) OR (closest.rank IS NULL))
                             Rows Removed by Filter: 49
                             ->  WindowAgg  (cost=82.33..112.33 rows=1000 width=20) (actual time=0.018..0.045 rows=50 loops=94743)
                                   ->  Sort  (cost=82.33..84.83 rows=1000 width=12) (actual time=0.017..0.019 rows=50 loops=94743)
                                         Sort Key: ((abs((gs1.fx - i.x)) + abs((gs2.fy - i.y))))
                                         Sort Method: quicksort  Memory: 27kB
                                         ->  CTE Scan on input i  (cost=0.00..32.50 rows=1000 width=12) (actual time=0.000..0.008 rows=50 loops=94743)
   ->  Sort  (cost=1017.71..1017.72 rows=2 width=56) (actual time=4906.989..4906.989 rows=1 loops=1)
         Sort Key: (count(*)) DESC
         Sort Method: top-N heapsort  Memory: 25kB
         ->  GroupAggregate  (cost=1009.66..1017.70 rows=2 width=56) (actual time=4873.850..4906.962 rows=33 loops=1)
               Group Key: area.ids, boundaries.minx, boundaries.maxx, boundaries.miny, boundaries.maxy
               Filter: ((min(area.fx) > boundaries.minx) AND (min(area.fy) > boundaries.miny) AND (max(area.fx) < boundaries.maxx) AND (max(area.fy) < boundaries.maxy))
               Rows Removed by Filter: 17
               ->  Sort  (cost=1009.66..1010.16 rows=200 width=56) (actual time=4873.203..4878.783 rows=92057 loops=1)
                     Sort Key: area.ids, boundaries.minx, boundaries.maxx, boundaries.miny, boundaries.maxy
                     Sort Method: quicksort  Memory: 16018kB
                     ->  Nested Loop  (cost=0.00..1002.02 rows=200 width=56) (actual time=4711.075..4796.210 rows=92057 loops=1)
                           ->  CTE Scan on boundaries  (cost=0.00..0.02 rows=1 width=16) (actual time=0.372..0.373 rows=1 loops=1)
                           ->  CTE Scan on area  (cost=0.00..1000.00 rows=200 width=40) (actual time=4710.701..4786.031 rows=92057 loops=1)
                                 Filter: (array_length(ids, 1) = 1)
                                 Rows Removed by Filter: 2686

*/