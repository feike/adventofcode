DEALLOCATE problem5_1;

-- regular expressions AND a recursive CTE
-- pretty brute force, so I'll call it the first iteration, to
-- be revisited when time is in abundance
PREPARE problem5_1 AS
WITH RECURSIVE permutations(pattern) AS (
    SELECT
        string_agg(format('%s%s|%s%s', u.c, l.c, l.c, u.c), '|')
    FROM
        regexp_split_to_table('abcdefghijklmnopqrstuvwxyz', '') as l(c)
    CROSS JOIN
        upper(c) as u(c)
), input (vals, iteration) AS (
    SELECT
        vals,
        0
    FROM
        trim($1, E' \n') AS v(vals)
    UNION ALL
    SELECT
        regexp_replace(vals, pattern, '', 'g'),
        iteration + 1
    FROM
        input
    CROSS JOIN
        permutations
    WHERE
        regexp_replace(vals, pattern, '', 'g') != vals
)
SELECT
    length(vals)
FROM
    input
ORDER BY
    iteration DESC
LIMIT 1;

EXECUTE problem5_1($$dabAcCaCBAcCcaDA$$);

\set input `cat 5.input`
EXECUTE problem5_1(:'input');
