DEALLOCATE problem1_1;
PREPARE problem1_1 AS
WITH input(line) AS (
	SELECT
		CAST(line AS bigint)
	FROM
		regexp_split_to_table($1, '\s+') AS sub(line)
)
SELECT
	sum(line)
FROM
	input
;

\set input `cat 1.input`
EXECUTE problem1_1(:'input');

/*
EXPLAIN EXECUTE problem1_1(:'input');

                                        QUERY PLAN                                        
------------------------------------------------------------------------------------------
 Aggregate  (cost=37.51..37.52 rows=1 width=32)
   CTE input
     ->  Function Scan on regexp_split_to_table sub  (cost=0.00..15.00 rows=1000 width=8)
   ->  CTE Scan on input  (cost=0.00..20.00 rows=1000 width=8)
(4 rows)

Time: 0.369 ms
/*