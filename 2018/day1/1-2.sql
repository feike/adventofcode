DEALLOCATE problem1_2;
PREPARE problem1_2 AS
WITH input(freq_delta, freq_sum, loop_delta, lineno, lines) AS (
	SELECT
		CAST(line AS bigint),
		coalesce(
			sum(CAST(line AS bigint)) OVER (ORDER BY rank ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING)
			, 0),
		sum(CAST(line AS bigint)) OVER (),
		rank,
		count(*) OVER ()
	FROM
		regexp_split_to_table($1, ',?\s+') WITH ORDINALITY AS sub(line, rank)
)
-- We assume that if any value is repeated, that it will be within the values
-- of the first iteration. Therefore we create a list of frequencies that were
-- touched on the first iteration (visited), and we calculate the number of loops
-- it will take for the whole process to be outside the range of values of the
-- first iteration (num_loops).
-- Due to the first assumption, once we're definitely outside the range of values
-- of the first iteration, we can stop, as we won't find any repeated frequencies.
, loopinfo (loop_delta, visited, num_loops) AS (
	SELECT
		loop_delta,
		array_agg(freq_sum::bigint),
		coalesce(ceil((max(freq_sum) - min(freq_sum))
			 /
			 nullif(loop_delta, 0)), 1)
	FROM
		input
	GROUP BY
		loop_delta
)
-- Many subqueries, but as we do a LIMIT 1 at the bottom, together with a UNION ALL
-- we know that if the repeated frequency is found in the first iteration, we're
-- not even executing the second part of the UNION ALL


SELECT * FROM (



SELECT * FROM
(
	-- This is how we find if we come across the same frequency *within*
	-- the first iteration of the loop
	SELECT
		freq_sum + freq_delta,
		null::bigint
	FROM
		input
	GROUP BY
		freq_sum + freq_delta
	HAVING
		count(*) > 1
	ORDER BY
		min(lineno)
	LIMIT
		1
) inner_subquery1(frequency, visited)
UNION ALL
SELECT * FROM (
	SELECT
		i*li.loop_delta+freq_delta+freq_sum,
		i*lines+lineno
	FROM
		loopinfo AS li
	-- We do LEFT JOIN ON (true) to force the planner a little
	LEFT JOIN
		generate_series(1, num_loops) AS series(i) ON (true)
	LEFT JOIN
		input ON (true)
	WHERE
		i*li.loop_delta+freq_delta+freq_sum = ANY(visited)
	ORDER BY
		i*lines+lineno ASC
	LIMIT
		1
) inner_subquery2
) outer_subquery
;

EXECUTE problem1_2('+1, -1');

--XPLAIN (ANALYZE) EXECUTE problem1_2('+1, -1');


EXECUTE problem1_2('+3, +3, +4, -2, -4');
EXECUTE problem1_2('-6, +3, +8, +5, -6');
EXECUTE problem1_2('+7, +7, -2, -7, -4');
EXPLAIN (ANALYZE) EXECUTE problem1_2('+7, +7, -2, -7, -4');

\set input `cat 1.input`
EXECUTE problem1_2(:'input');

/*
EXPLAIN (ANALYZE, BUFFERS) EXECUTE problem1_2(:'input');

                                                                           QUERY PLAN                                                                           
----------------------------------------------------------------------------------------------------------------------------------------------------------------
 Append  (cost=35.67..12749009.08 rows=2 width=64) (actual time=13073.273..13073.276 rows=1 loops=1)
   CTE input
     ->  WindowAgg  (cost=0.00..55.00 rows=1000 width=88) (actual time=1.619..1.852 rows=1023 loops=1)
           ->  WindowAgg  (cost=0.00..30.00 rows=1000 width=72) (actual time=0.683..1.320 rows=1023 loops=1)
                 ->  Function Scan on regexp_split_to_table sub  (cost=0.00..10.00 rows=1000 width=40) (actual time=0.679..0.769 rows=1023 loops=1)
   CTE loopinfo
     ->  HashAggregate  (cost=32.50..37.00 rows=200 width=96) (actual time=0.555..0.556 rows=1 loops=1)
           Group Key: input_2.loop_delta
           ->  CTE Scan on input input_2  (cost=0.00..20.00 rows=1000 width=64) (actual time=0.000..0.078 rows=1023 loops=1)
   ->  Subquery Scan on "*SELECT* 1"  (cost=35.67..35.70 rows=1 width=64) (actual time=2.751..2.751 rows=0 loops=1)
         ->  Subquery Scan on inner_subquery1  (cost=35.67..35.68 rows=1 width=40) (actual time=2.750..2.750 rows=0 loops=1)
               ->  Limit  (cost=35.67..35.67 rows=1 width=48) (actual time=2.750..2.750 rows=0 loops=1)
                     ->  Sort  (cost=35.67..35.84 rows=67 width=48) (actual time=2.749..2.749 rows=0 loops=1)
                           Sort Key: (min(input.lineno))
                           Sort Method: quicksort  Memory: 25kB
                           ->  HashAggregate  (cost=32.50..35.34 rows=67 width=48) (actual time=2.746..2.746 rows=0 loops=1)
                                 Group Key: (input.freq_sum + (input.freq_delta)::numeric)
                                 Filter: (count(*) > 1)
                                 Rows Removed by Filter: 1023
                                 ->  CTE Scan on input  (cost=0.00..25.00 rows=1000 width=40) (actual time=1.624..2.360 rows=1023 loops=1)
   ->  Limit  (cost=12748973.35..12748973.36 rows=1 width=64) (actual time=13070.521..13070.524 rows=1 loops=1)
         ->  Sort  (cost=12748973.35..12773418.29 rows=9777974 width=64) (actual time=13070.521..13070.521 rows=1 loops=1)
               Sort Key: (((series.i * (input_1.lines)::numeric) + (input_1.lineno)::numeric))
               Sort Method: top-N heapsort  Memory: 25kB
               ->  Nested Loop  (cost=0.00..12700083.48 rows=9777974 width=64) (actual time=13.926..13069.748 rows=433 loops=1)
                     Join Filter: ((((series.i * li.loop_delta) + (input_1.freq_delta)::numeric) + input_1.freq_sum) = ANY ((li.visited)::numeric[]))
                     Rows Removed by Join Filter: 146879
                     ->  CTE Scan on input input_1  (cost=0.00..20.00 rows=1000 width=56) (actual time=0.000..0.691 rows=1023 loops=1)
                     ->  Materialize  (cost=0.00..5004.00 rows=200000 width=96) (actual time=0.001..0.010 rows=144 loops=1023)
                           ->  Nested Loop  (cost=0.00..4004.00 rows=200000 width=96) (actual time=0.581..0.633 rows=144 loops=1)
                                 ->  CTE Scan on loopinfo li  (cost=0.00..4.00 rows=200 width=96) (actual time=0.557..0.559 rows=1 loops=1)
                                 ->  Function Scan on generate_series series  (cost=0.00..10.00 rows=1000 width=32) (actual time=0.022..0.042 rows=144 loops=1)
 Planning Time: 0.014 ms
 Execution Time: 13073.413 ms
(34 rows)
*/