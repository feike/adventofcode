
DEALLOCATE problem7_1;

PREPARE problem7_1 AS
WITH RECURSIVE input (step, depends_on) AS (
    SELECT
    	matches[2] AS step,
    	array_agg(matches[1] ORDER BY matches[1]) AS depends_on
    FROM
        regexp_split_to_table(trim($1, E' \n'), '\n') AS vals(vals)
    CROSS JOIN
    	regexp_matches(vals, 'Step\s+(\w+).*must be finished.*step\s+(\w+)') AS rm(matches)
    GROUP BY
    	step
), roots (step) AS (
	SELECT DISTINCT
		d
	FROM
		input a
	CROSS JOIN
		unnest(a.depends_on) AS d(d)
	LEFT JOIN
		input b ON (d = b.step)
	WHERE
		b IS NULL
), all_steps (step, duration) AS (
	SELECT
		step,
		rank() OVER (ORDER BY step) + $2
	FROM (
		SELECT
			step
		FROM
			input
		UNION -- Implies distinct, handy
		SELECT
			step
		FROM
			roots
		) sub
), walk (chain, iteration) AS (
	SELECT
		*,
		0
	FROM (
		SELECT
			step
		FROM
			roots
		ORDER BY
			step ASC
		LIMIT 1
	) first_root
	UNION ALL
	SELECT
		*
	FROM (
		SELECT
			chain||c.step AS chain,
			iteration + 1
		FROM
			walk AS w
		LEFT JOIN
			roots AS r ON (w.chain !~ r.step)
		LEFT JOIN
			-- All depends_on should be in the chain, and the step itself
			-- should not yet be in the chain
			input AS i ON (w.chain ~ ALL(i.depends_on) AND w.chain !~ i.step)
		JOIN
			least(r.step, i.step) AS c(step) ON (c.step IS NOT NULL)
		ORDER BY
			c.step ASC
		LIMIT
			1
	) next_one
)
SELECT
	*,
	$3
FROM
	all_steps
;

EXECUTE problem7_1($$
Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.
$$, 0, 2);

\set input `cat 7.input`
EXECUTE problem7_1(:'input', 60, 5);

/*
EXPLAIN (ANALYZE) EXECUTE problem7_1(:'input');

                                                                        QUERY PLAN
-----------------------------------------------------------------------------------------------------------------------------------------------------------
 CTE Scan on walk  (cost=100.18..100.40 rows=11 width=36) (actual time=3.578..4.453 rows=26 loops=1)
   CTE input
     ->  GroupAggregate  (cost=79.83..87.35 rows=1 width=64) (actual time=3.426..3.481 rows=22 loops=1)
           Group Key: (rm.matches[2])
           ->  Sort  (cost=79.83..82.33 rows=1000 width=64) (actual time=3.415..3.419 rows=101 loops=1)
                 Sort Key: (rm.matches[2])
                 Sort Method: quicksort  Memory: 32kB
                 ->  Nested Loop  (cost=0.01..30.00 rows=1000 width=64) (actual time=0.220..3.380 rows=101 loops=1)
                       ->  Function Scan on regexp_split_to_table vals  (cost=0.00..10.00 rows=1000 width=32) (actual time=0.162..0.170 rows=101 loops=1)
                       ->  Function Scan on regexp_matches rm  (cost=0.00..0.01 rows=1 width=32) (actual time=0.031..0.031 rows=1 loops=101)
   CTE roots
     ->  Unique  (cost=1.46..1.47 rows=1 width=32) (actual time=3.566..3.569 rows=4 loops=1)
           ->  Sort  (cost=1.46..1.46 rows=1 width=32) (actual time=3.566..3.567 rows=20 loops=1)
                 Sort Key: d.d
                 Sort Method: quicksort  Memory: 25kB
                 ->  Nested Loop  (cost=0.04..1.45 rows=1 width=32) (actual time=3.510..3.560 rows=20 loops=1)
                       ->  CTE Scan on input a  (cost=0.00..0.02 rows=1 width=32) (actual time=3.427..3.429 rows=22 loops=1)
                       ->  Hash Left Join  (cost=0.04..1.42 rows=1 width=32) (actual time=0.005..0.006 rows=1 loops=22)
                             Hash Cond: (d.d = b.step)
                             Filter: (b.* IS NULL)
                             Rows Removed by Filter: 4
                             ->  Function Scan on unnest d  (cost=0.00..1.00 rows=100 width=32) (actual time=0.001..0.001 rows=5 loops=22)
                             ->  Hash  (cost=0.02..0.02 rows=1 width=88) (actual time=0.073..0.073 rows=22 loops=1)
                                   Buckets: 1024  Batches: 1  Memory Usage: 11kB
                                   ->  CTE Scan on input b  (cost=0.00..0.02 rows=1 width=88) (actual time=0.003..0.068 rows=22 loops=1)
   CTE walk
     ->  Recursive Union  (cost=0.03..11.36 rows=11 width=36) (actual time=3.577..4.445 rows=26 loops=1)
           ->  Subquery Scan on first_root  (cost=0.03..0.05 rows=1 width=36) (actual time=3.577..3.578 rows=1 loops=1)
                 ->  Limit  (cost=0.03..0.04 rows=1 width=32) (actual time=3.576..3.577 rows=1 loops=1)
                       ->  Sort  (cost=0.03..0.04 rows=1 width=32) (actual time=3.575..3.576 rows=1 loops=1)
                             Sort Key: roots.step
                             Sort Method: top-N heapsort  Memory: 25kB
                             ->  CTE Scan on roots  (cost=0.00..0.02 rows=1 width=32) (actual time=3.567..3.570 rows=4 loops=1)
           ->  Subquery Scan on next_one  (cost=1.10..1.11 rows=1 width=36) (actual time=0.032..0.033 rows=1 loops=26)
                 ->  Limit  (cost=1.10..1.10 rows=1 width=68) (actual time=0.032..0.033 rows=1 loops=26)
                       ->  Sort  (cost=1.10..1.12 rows=10 width=68) (actual time=0.032..0.032 rows=1 loops=26)
                             Sort Key: c.step
                             Sort Method: quicksort  Memory: 25kB
                             ->  Nested Loop  (cost=0.00..1.05 rows=10 width=68) (actual time=0.017..0.031 rows=2 loops=26)
                                   ->  Nested Loop Left Join  (cost=0.00..0.80 rows=10 width=100) (actual time=0.016..0.029 rows=2 loops=26)
                                         Join Filter: ((w.chain !~ i.step) AND (w.chain ~ ALL (i.depends_on)))
                                         Rows Removed by Join Filter: 25
                                         ->  Nested Loop Left Join  (cost=0.00..0.43 rows=10 width=68) (actual time=0.003..0.003 rows=1 loops=26)
                                               Join Filter: (w.chain !~ r.step)
                                               Rows Removed by Join Filter: 3
                                               ->  WorkTable Scan on walk w  (cost=0.00..0.20 rows=10 width=36) (actual time=0.000..0.000 rows=1 loops=26)
                                               ->  CTE Scan on roots r  (cost=0.00..0.02 rows=1 width=32) (actual time=0.000..0.000 rows=4 loops=26)
                                         ->  CTE Scan on input i  (cost=0.00..0.02 rows=1 width=64) (actual time=0.000..0.002 rows=22 loops=32)
                                   ->  Function Scan on c  (cost=0.00..0.01 rows=1 width=32) (actual time=0.000..0.000 rows=1 loops=46)
                                         Filter: (step IS NOT NULL)
                                         Rows Removed by Filter: 0
*/