
DEALLOCATE problem2_1;
PREPARE problem2_1 AS
WITH input AS (
	SELECT
		line,
		c,
		count(*),
		count(*) OVER (PARTITION BY line, count(*)) AS entries,
		lineno
	FROM
		regexp_split_to_table(trim($1, E' \n'), '\s+') WITH ORDINALITY AS sub(line, lineno)
	CROSS JOIN
		regexp_split_to_table(line, '') AS sub2(c)
	GROUP BY
		line,
		lineno,
		c
	HAVING
		count(*) BETWEEN 2 AND 3
)
SELECT
	count(DISTINCT lineno) FILTER (WHERE count=2)
	*
	count(DISTINCT lineno) FILTER (WHERE count=3)
FROM
	input
;

EXECUTE problem2_1($$
abcdef
bababc
abbcde
abcccd
aabcdd
abcdee
ababab
$$);

\set input `cat 2.input`
EXECUTE problem2_1(:'input');

/*
EXPLAIN (ANALYZE, BUFFERS) EXECUTE problem2_1(:'input');

                                                                              QUERY PLAN                                                                               
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Aggregate  (cost=220292.30..220292.31 rows=1 width=8) (actual time=28.567..28.567 rows=1 loops=1)
   CTE input
     ->  WindowAgg  (cost=220070.10..220158.98 rows=4444 width=88) (actual time=27.491..28.014 rows=663 loops=1)
           ->  Sort  (cost=220070.10..220081.21 rows=4444 width=80) (actual time=27.458..27.514 rows=663 loops=1)
                 Sort Key: sub.line, (count(*))
                 Sort Method: quicksort  Memory: 76kB
                 ->  GroupAggregate  (cost=201700.85..219800.85 rows=4444 width=80) (actual time=24.108..26.611 rows=663 loops=1)
                       Group Key: sub.line, sub.lineno, sub2.c
                       Filter: ((count(*) >= 2) AND (count(*) <= 3))
                       Rows Removed by Filter: 5145
                       ->  Sort  (cost=201700.85..204200.85 rows=1000000 width=72) (actual time=24.097..24.606 rows=6500 loops=1)
                             Sort Key: sub.line, sub.lineno, sub2.c
                             Sort Method: quicksort  Memory: 700kB
                             ->  Nested Loop  (cost=0.01..20010.01 rows=1000000 width=72) (actual time=0.391..7.584 rows=6500 loops=1)
                                   ->  Function Scan on regexp_split_to_table sub  (cost=0.00..10.00 rows=1000 width=40) (actual time=0.361..0.400 rows=250 loops=1)
                                   ->  Function Scan on regexp_split_to_table sub2  (cost=0.00..10.00 rows=1000 width=32) (actual time=0.021..0.024 rows=26 loops=250)
   ->  CTE Scan on input  (cost=0.00..88.88 rows=4444 width=16) (actual time=27.496..28.359 rows=663 loops=1)
 Planning Time: 0.276 ms
 Execution Time: 28.714 ms
(19 rows)

/*