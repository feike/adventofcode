DEALLOCATE problem2_2;
PREPARE problem2_2 AS
WITH input AS (
	SELECT
		line,
		lineno,
		col,
		colno
	FROM
		regexp_split_to_table(trim($1, E' \n'), '\s+') WITH ORDINALITY AS sub(line, lineno)
	CROSS JOIN
		regexp_split_to_table(line, '') WITH ORDINALITY AS sub2(col, colno)
)
SELECT
	a.line,
	b.line,
	count(*),
	string_agg(a.col, '' ORDER BY a.colno) AS in_common
FROM
	input a
JOIN
    input b ON (a.col=b.col AND a.colno=b.colno AND a.lineno!=b.lineno)
GROUP BY
	a.line,
	b.line
ORDER BY
	count(*) DESC
LIMIT 1
;

EXECUTE problem2_2($$
abcde
fghij
klmno
pqrst
fguij
axcye
wvxyz
$$);

\set input `cat 2.input`
EXECUTE problem2_2(:'input');

/*
                                                                     QUERY PLAN                                                                     
----------------------------------------------------------------------------------------------------------------------------------------------------
 Limit  (cost=4258607.22..4258607.22 rows=1 width=104) (actual time=4418.222..4418.226 rows=1 loops=1)
   CTE input
     ->  Nested Loop  (cost=0.01..20010.01 rows=1000000 width=80) (actual time=0.777..12.788 rows=6500 loops=1)
           ->  Function Scan on regexp_split_to_table sub  (cost=0.00..10.00 rows=1000 width=40) (actual time=0.730..0.792 rows=250 loops=1)
           ->  Function Scan on regexp_split_to_table sub2  (cost=0.00..10.00 rows=1000 width=40) (actual time=0.034..0.039 rows=26 loops=250)
   ->  Sort  (cost=4238597.21..4238630.54 rows=13333 width=104) (actual time=4418.220..4418.220 rows=1 loops=1)
         Sort Key: (count(*)) DESC
         Sort Method: quicksort  Memory: 25kB
         ->  GroupAggregate  (cost=3802484.71..4238530.55 rows=13333 width=104) (actual time=3948.807..4418.214 rows=2 loops=1)
               Group Key: a.line, b.line
               Filter: (count(*) > (max(a.colno) - 1))
               Rows Removed by Filter: 62248
               ->  Sort  (cost=3802484.71..3864672.21 rows=24875000 width=104) (actual time=3623.582..3759.241 rows=1267266 loops=1)
                     Sort Key: a.line, b.line
                     Sort Method: quicksort  Memory: 227362kB
                     ->  Merge Join  (cost=239315.69..746815.69 rows=24875000 width=104) (actual time=33.189..369.435 rows=1267266 loops=1)
                           Merge Cond: ((a.col = b.col) AND (a.colno = b.colno))
                           Join Filter: (a.lineno <> b.lineno)
                           Rows Removed by Join Filter: 6500
                           ->  Sort  (cost=119657.84..122157.84 rows=1000000 width=80) (actual time=25.336..26.375 rows=6500 loops=1)
                                 Sort Key: a.col, a.colno
                                 Sort Method: quicksort  Memory: 700kB
                                 ->  CTE Scan on input a  (cost=0.00..20000.00 rows=1000000 width=80) (actual time=0.782..18.534 rows=6500 loops=1)
                           ->  Sort  (cost=119657.84..122157.84 rows=1000000 width=80) (actual time=7.836..78.221 rows=1273766 loops=1)
                                 Sort Key: b.col, b.colno
                                 Sort Method: quicksort  Memory: 700kB
                                 ->  CTE Scan on input b  (cost=0.00..20000.00 rows=1000000 width=80) (actual time=0.003..1.785 rows=6500 loops=1)
 Planning Time: 0.679 ms
 Execution Time: 4427.651 ms
(29 rows)

*/