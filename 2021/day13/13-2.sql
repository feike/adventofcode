DEALLOCATE problem13_2;

PREPARE problem13_2 AS
WITH RECURSIVE input(line, line_no) AS (
    SELECT
        *
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, line_no)
    WHERE
        line != ''
), instructions (axis, value, id) AS (
    SELECT
        matches[1],
        matches[2]::smallint,
        rank() OVER (ORDER BY line_no)::integer
    FROM
        input
    CROSS JOIN
        regexp_matches(line, '^fold along (.)=(\d+)') AS _(matches)
    WHERE
        line LIKE 'fold%'
    ORDER BY
        line_no
), fold(x, y, id) AS (
    SELECT
        coordinates[1]::smallint AS x,
        coordinates[2]::smallint AS y,
        0
    FROM
        input
    CROSS JOIN
        string_to_array(line, ',') AS c(coordinates)
    WHERE
        line NOT LIKE 'fold%'
    
    UNION ALL

    SELECT
        CASE 
            WHEN axis = 'x' AND x > value
            THEN value - (x - value)
            ELSE x
        END,
        CASE 
            WHEN axis = 'y' AND y > value
            THEN value - (y - value)
            ELSE y
        END,
        instructions.id
    FROM
        fold
    JOIN
        instructions ON (fold.id + 1 = instructions.id)
), final AS (
    SELECT
        DISTINCT
        x,
        y
    FROM
        fold
    WHERE
        id = (SELECT max(id) FROM instructions)
), display AS (
    SELECT
        y,
        string_agg(
            CASE
                WHEN final.y IS NULL THEN ' '
                ELSE '█'
            END,
            '' ORDER BY x)
    FROM
        generate_series(0, (SELECT max(x) FROM final)) AS x(x)
    CROSS JOIN
        generate_series(0, (SELECT max(y) FROM final)) AS y(y)
    LEFT JOIN
        final USING (x, y)
    GROUP BY
        y
)
SELECT
    *
FROM
    display;

EXECUTE problem13_2($$6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5$$);

\set input `cat 13.input`
EXECUTE problem13_2(:'input');

set track_io_timing to 'on';
\o 13-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem13_2(:'input');
\o
