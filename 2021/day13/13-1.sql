DEALLOCATE problem13_1;

PREPARE problem13_1 AS
WITH RECURSIVE input(line) AS (
    SELECT
        *
    FROM
        regexp_split_to_table($1, '\n') AS _(line)
    WHERE
        line != ''
), coords(x, y) AS (
    SELECT
        coordinates[1]::smallint AS x,
        coordinates[2]::smallint AS y
    FROM
        input
    CROSS JOIN
        string_to_array(line, ',') AS c(coordinates)
    WHERE
        line NOT LIKE 'fold%'
), first_fold (axis, value) AS (
    SELECT
        matches[1],
        matches[2]::smallint
    FROM
        input
    CROSS JOIN
        regexp_matches(line, '^fold along (.)=(\d+)') WITH ORDINALITY AS _(matches, line_no)
    WHERE
        line LIKE 'fold%'
    ORDER BY
        line_no
    LIMIT
        1
), fold_once(x, y) AS (
    SELECT
        DISTINCT
        CASE 
            WHEN axis != 'x' THEN x
            WHEN x < value THEN x
            ELSE value - (x - value)
        END,
        CASE 
            WHEN axis != 'y' THEN y
            WHEN y < value THEN y
            ELSE value - (y - value)
        END
    FROM
        first_fold
    CROSS JOIN
        coords
), display AS (
    SELECT
        y,
        string_agg(
            CASE
                WHEN fold_once.y IS NULL THEN '.'
                ELSE '#'
            END,
            '' ORDER BY x)
    FROM
        generate_series(0, (SELECT max(x) FROM coords)) AS x(x)
    CROSS JOIN
        generate_series(0, (SELECT max(y) FROM coords)) AS y(y)
    LEFT JOIN
        fold_once USING (x, y)
    GROUP BY
        y
)
SELECT
    count(*)
FROM
    fold_once;

EXECUTE problem13_1($$6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5$$);

\set input `cat 13.input`
EXECUTE problem13_1(:'input');

set track_io_timing to 'on';
\o 13-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem13_1(:'input');
\o
