DEALLOCATE problem05_2;

PREPARE problem05_2 AS
WITH input AS (
    SELECT
        lineno,
        m[1]::int as x1,
        m[2]::int as y1,
        m[3]::int as x2,
        m[4]::int as y2
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS sub(line, lineno)
    CROSS JOIN
        regexp_matches(line, '(\d+),(\d+)\s*->\s*(\d+),(\d+)') AS rm(m)
)
SELECT
    count('🎄') OVER ()
FROM
    input
CROSS JOIN LATERAL
    generate_series(x1, x2, CASE WHEN x1<x2 THEN 1 ELSE -1 END) WITH ORDINALITY AS all_x(x, idx)
JOIN LATERAL
    generate_series(y1, y2, CASE WHEN y1<y2 THEN 1 ELSE -1 END) WITH ORDINALITY AS all_y(y, idx) ON (
        CASE
            WHEN x1 != x2 AND y1 != y2
            THEN all_x.idx = all_y.idx
            ELSE true
        END
    )
GROUP BY
    all_x.x,
    all_y.y
HAVING
    count('🧝') > 1
LIMIT
    1;

EXECUTE problem05_2($$0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
$$);

\set input `cat 05.input`
EXECUTE problem05_2(:'input');

set track_io_timing to 'on';
\o 05-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem05_2(:'input');
\o
