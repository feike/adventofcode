DEALLOCATE problem11_1;

PREPARE problem11_1 AS
WITH RECURSIVE input AS (
    SELECT
        array_agg(energy::int ORDER BY y, x) AS field
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS s(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS s2(energy, x)
), directions (dx, dy) AS (
    VALUES
      (-1, -1),
      (-1,  0),
      (-1,  1),
      ( 0, -1),
      ( 0,  1),
      ( 1, -1),
      ( 1,  0),
      ( 1,  1)
), combos(a, b) AS MATERIALIZED (
    SELECT
        1 + y*10 + x,
        1 + (y+dy)*10 + (x+dx)
    FROM
        generate_series(0, 9) AS x(x)
    CROSS JOIN
        generate_series(0, 9) AS y(y)
    CROSS JOIN
        directions
    WHERE
        x+dx BETWEEN 0 AND 9       
        AND y+dy BETWEEN 0 AND 9
), game AS (
    SELECT
        1 as round,
        field
    FROM
        input

    UNION ALL

    SELECT
        round+1,
        field
    FROM (
        WITH RECURSIVE single_round AS (
            SELECT
                round,
                0 as inc,
                (
                    SELECT
                        array_agg(energy + 1 ORDER BY idx)
                    FROM
                        unnest(field) WITH ORDINALITY AS _(energy, idx)
                ) AS field
            FROM
                game

            UNION ALL

            SELECT
                round,
                inc + 1,
                (
                    SELECT
                        array_agg(energy::int ORDER BY idx)
                    FROM (
                        SELECT
                            idx,
                            CASE
                                WHEN energy > 9
                                THEN 0
                                WHEN energy = 0
                                THEN 0
                                ELSE energy + count(*) FILTER (WHERE field[b]>9)
                            END AS energy
                        FROM
                            unnest(field) WITH ORDINALITY AS _(energy, idx)
                        JOIN
                            combos ON (idx=a)
                        GROUP BY
                            idx,
                            energy
                    ) AS neighour_flashes
                )
            FROM
                single_round
            WHERE
                9 < ANY (field)
        )
        SELECT DISTINCT ON (round)
            round,
            field
        FROM
            single_round
        ORDER BY
            round,
            inc DESC
    ) _
    WHERE
        NOT ARRAY[0] @> field
)
SELECT
    max(round)
FROM
    game;

EXECUTE problem11_1($$5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526$$);

\set input `cat 11.input`
EXECUTE problem11_1(:'input');

set track_io_timing to 'on';
\o 11-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem11_1(:'input');
\o
