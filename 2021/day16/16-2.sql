DEALLOCATE problem16_1;

PREPARE problem16_1 AS
WITH RECURSIVE input(packets) AS MATERIALIZED (
    SELECT
        cast(packets AS bit varying)
    FROM
        rtrim(concat('x', $1::text), E' \n\t\r') AS _(packets)
), walk AS (
    SELECT
        1::smallint            AS position, -- index of the bit string we want to process next
        0::smallint            AS id,       -- sequence generator for identifying new packets
        ARRAY[]::smallint[]    AS parents,  -- parentage, [] for the root packet, [1] for the children of the root packet, etc
        null::smallint         AS version,  -- version of this packet
        null::smallint         AS operator, -- operator of this packet
        null::int              AS value,    -- value of this packet (if literal)
        ARRAY[]::smallint[]    AS lengths   -- position (positive) where the parent packet ends, or children (negative) the parent packet still requires

    UNION ALL

    SELECT
        len.position::smallint,
        new.id,
        len.parents,
        new.version,
        new.operator,
        literal.value,
        len.lengths
    FROM
        walk
    CROSS JOIN
        input
    CROSS JOIN LATERAL (
        SELECT
            CASE
                WHEN is_packet_boundary
                THEN position
                ELSE (position + 6)::smallint
            END,
            CASE
                WHEN is_packet_boundary
                THEN id
                ELSE (id + 1)::smallint
            END,
            CASE
                WHEN is_packet_boundary
                THEN null
                ELSE substring(packets, position, 3)::int::smallint
            END,
            CASE
                WHEN is_packet_boundary
                THEN null
                ELSE substring(packets, position + 3, 3)::int::smallint
            END,
            len_len,
            CASE
                WHEN is_packet_boundary
                THEN lengths[1:len_len-1]
                ELSE lengths
            END,
            is_packet_boundary
        FROM (
            SELECT
                len_len,
                len_len > 0 AND lengths[array_length(lengths, 1)] IN (position, 0) AS is_packet_boundary
            FROM
                array_length(lengths, 1) AS _(len_len)
            ) _
        ) AS new(position, id, version, operator, len_len, lengths, is_packet_boundary)
    CROSS JOIN LATERAL (
        WITH RECURSIVE get_literal(position, keep_processing, literal) AS (
            SELECT
                new.position,
                new.operator = 4,
                CASE WHEN new.operator = 4 THEN 0 END AS literal
            UNION ALL

            SELECT
                (position + 5)::smallint,
                substring(packets, position, 1) = B'1',
                (literal<<4) + substring(packets, position + 1, 4)::bit(4)::int
            FROM
                get_literal
            CROSS JOIN
                input
            WHERE
                keep_processing
        )
        SELECT
            literal::integer,
            position
        FROM
            get_literal
        ORDER BY
            position DESC
        LIMIT
            1
        ) AS literal(value, position)
    CROSS JOIN LATERAL (
        SELECT
            CASE
                WHEN is_packet_length
                THEN literal.position + 12
                WHEN NOT is_packet_length
                THEN literal.position + 16
                ELSE literal.position
            END,
            CASE
                WHEN is_packet_length
                THEN walk.lengths || (-1 * substring(packets, new.position + 1, 11)::bit(11)::integer)::smallint
                WHEN NOT is_packet_length
                THEN walk.lengths || (literal.position + 16 + substring(packets, new.position + 1, 15)::bit(15)::integer)::smallint
                WHEN is_packet_boundary
                THEN walk.lengths[1:len_len-1]
                WHEN walk.lengths[len_len] < 0
                THEN walk.lengths[1:len_len-1]||(walk.lengths[len_len] + 1)::smallint
                ELSE walk.lengths
            END,
            CASE
                WHEN is_packet_length IS NOT NULL
                THEN walk.parents||new.id
                ELSE walk.parents
            END
        FROM ( VALUES (
            CASE
                WHEN new.operator = 4 OR is_packet_boundary
                THEN null
                WHEN substring(packets, new.position, 1) = B'1'
                THEN true
                ELSE false
            END
            ) ) _(is_packet_length)
    ) len(position, lengths, parents)
    WHERE
        walk.position < length(packets)
        AND  bit_count(substring(packets, walk.position)) > 0
)

-- id        id of the packet
-- parent    parent_id of this packet
-- version   version of this packet
-- operator  operator of this packet
-- value     the value that this packet resolves into
, all_packets AS MATERIALIZED (
    SELECT
        id,
        CASE
            WHEN parents[p_len] = id
            THEN parents[p_len - 1]
            ELSE parents[p_len]
        END::smallint AS parent,
        version,
        operator,
        value
    FROM
        walk
    CROSS JOIN
        array_length(parents, 1) AS _(p_len)
    WHERE
        version IS NOT NULL
), solve AS (
    SELECT
        parent,
        value,
        0 AS level
    FROM
        all_packets
    WHERE
        operator = 4

    UNION

    SELECT
        p.parent,
        p.value,
        level + 1
    FROM
        solve AS c
    JOIN
        all_packets AS p ON (c.parent=p.id)       
)
SELECT
    *
FROM
    solve
;

--EXECUTE problem16_1('D2FE28');
--EXECUTE problem16_1('38006F45291200');
--EXECUTE problem16_1('EE00D40C823060');
--EXECUTE problem16_1('8A004A801A8002F478');
--EXECUTE problem16_1('620080001611562C8802118E34 ');
EXECUTE problem16_1('C0015000016115A2E0802F182340');
EXECUTE problem16_1('9C0141080250320F1802104A08');



/*
\set input `cat 16.input`
EXECUTE problem16_1(:'input');
*/

/*
set track_io_timing to 'on';
\o 16-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem16_1(:'input');
\o

*/