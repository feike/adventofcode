DEALLOCATE problem16_1;

PREPARE problem16_1 AS
WITH RECURSIVE input(packets) AS MATERIALIZED (
    SELECT
        cast(packets AS bit varying)
    FROM
        rtrim(concat('x', $1::text), E' \n\t\r') AS _(packets)
), walk AS (
    SELECT
        1::smallint            AS position, -- index of the bit string we want to process next
        0::smallint            AS id,       -- sequence generator for identifying new packets
        ARRAY[]::smallint[]    AS parents,  -- parentage, [] for the root packet, [1] for the children of the root packet, etc
        null::smallint         AS version,  -- version of this packet
        null::smallint         AS operator, -- operator of this packet
        null::int              AS value,    -- value of this packet (if literal)
        ARRAY[]::smallint[]    AS lengths  -- position (positive) where the parent packet ends, or children (negative) the parent packet still requires

    UNION ALL

    SELECT
        len.position::smallint,
        new.id,
        parents,
        new.version,
        new.operator,
        literal.value,
        len.lengths
    FROM
        walk
    CROSS JOIN
        input
    CROSS JOIN LATERAL (
        SELECT
            (position + 6)::smallint,
            (id + 1)::smallint,
            substring(packets, position, 3)::int::smallint,
            substring(packets, position + 3, 3)::int::smallint
        ) AS new(position, id, version, operator)
    CROSS JOIN LATERAL (
        WITH RECURSIVE get_literal(position, keep_processing, literal) AS (
            SELECT
                new.position,
                new.operator = 4,
                0 AS literal

            UNION ALL

            SELECT
                (position + 5)::smallint,
                substring(packets, position, 1) = B'1',
                (literal<<4) + substring(packets, position + 1, 4)::bit(4)::int
            FROM
                get_literal
            CROSS JOIN
                input
            WHERE
                keep_processing
        )
        SELECT
            literal::integer,
            position
        FROM
            get_literal
        ORDER BY
            position DESC
        LIMIT
            1
        ) AS literal(value, position)
    CROSS JOIN LATERAL (
        SELECT
            CASE
                WHEN is_packet_length
                THEN literal.position + 12
                WHEN NOT is_packet_length
                THEN literal.position + 16
                ELSE literal.position
            END,
            CASE
                WHEN is_packet_length
                THEN walk.lengths || (-1 * substring(packets, new.position + 1, 11)::bit(11)::integer)::smallint
                WHEN NOT is_packet_length
                THEN walk.lengths || substring(packets, new.position + 1, 15)::bit(15)::integer::smallint
                ELSE walk.lengths
            END
        FROM ( VALUES (
            CASE
                WHEN new.operator = 4
                THEN null
                WHEN substring(packets, new.position, 1) = B'1'
                THEN true
                ELSE false
            END
            ) ) _(is_packet_length)
    ) len(position, lengths)
    WHERE
        walk.position < length(packets)
        AND  bit_count(substring(packets, walk.position)) > 0
)
SELECT
    sum(version)
FROM
    walk;

EXECUTE problem16_1('D2FE28');
EXECUTE problem16_1('38006F45291200');
EXECUTE problem16_1('EE00D40C823060');
EXECUTE problem16_1('8A004A801A8002F478');
EXECUTE problem16_1('620080001611562C8802118E34 ');
EXECUTE problem16_1('C0015000016115A2E0802F182340');
EXECUTE problem16_1('A0016C880162017C3686B18A3D4780');


\set input `cat 16.input`
EXECUTE problem16_1(:'input');

set track_io_timing to 'on';
\o 16-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem16_1(:'input');
\o
