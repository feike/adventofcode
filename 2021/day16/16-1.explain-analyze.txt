                                                                                      QUERY PLAN                                                                                      
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Aggregate  (cost=71.17..71.18 rows=1 width=8) (actual time=1.725..1.726 rows=1 loops=1)
   CTE input
     ->  Function Scan on rtrim _  (cost=0.01..0.02 rows=1 width=32) (actual time=0.008..0.009 rows=1 loops=1)
   CTE walk
     ->  Recursive Union  (cost=0.00..70.90 rows=11 width=76) (actual time=0.001..1.593 rows=269 loops=1)
           ->  Result  (cost=0.00..0.01 rows=1 width=76) (actual time=0.000..0.001 rows=1 loops=1)
           ->  Nested Loop  (cost=6.40..7.07 rows=1 width=76) (actual time=0.005..0.005 rows=1 loops=269)
                 ->  Nested Loop  (cost=0.00..0.47 rows=1 width=100) (actual time=0.001..0.001 rows=1 loops=269)
                       Join Filter: ((walk_1."position" < length((input_1.packets)::"bit")) AND (bit_count("substring"((input_1.packets)::"bit", (walk_1."position")::integer)) > 0))
                       Rows Removed by Join Filter: 0
                       ->  CTE Scan on input input_1  (cost=0.00..0.02 rows=1 width=32) (actual time=0.000..0.000 rows=1 loops=269)
                       ->  WorkTable Scan on walk walk_1  (cost=0.00..0.20 rows=10 width=68) (actual time=0.000..0.000 rows=1 loops=269)
                 ->  Limit  (cost=6.40..6.40 rows=1 width=6) (actual time=0.004..0.004 rows=1 loops=268)
                       CTE get_literal
                         ->  Recursive Union  (cost=0.00..5.12 rows=51 width=7) (actual time=0.000..0.002 rows=3 loops=268)
                               ->  Result  (cost=0.00..0.03 rows=1 width=7) (actual time=0.000..0.000 rows=1 loops=268)
                               ->  Nested Loop  (cost=0.00..0.41 rows=5 width=7) (actual time=0.000..0.000 rows=1 loops=737)
                                     ->  CTE Scan on input  (cost=0.00..0.02 rows=1 width=32) (actual time=0.000..0.000 rows=1 loops=737)
                                     ->  WorkTable Scan on get_literal  (cost=0.00..0.20 rows=5 width=6) (actual time=0.000..0.000 rows=1 loops=737)
                                           Filter: keep_processing
                                           Rows Removed by Filter: 0
                       ->  Sort  (cost=1.28..1.40 rows=51 width=6) (actual time=0.003..0.003 rows=1 loops=268)
                             Sort Key: get_literal_1."position" DESC
                             Sort Method: top-N heapsort  Memory: 25kB
                             ->  CTE Scan on get_literal get_literal_1  (cost=0.00..1.02 rows=51 width=6) (actual time=0.001..0.003 rows=3 loops=268)
   ->  CTE Scan on walk  (cost=0.00..0.22 rows=11 width=2) (actual time=0.001..1.698 rows=269 loops=1)
 Planning Time: 0.003 ms
 Execution Time: 1.772 ms
(28 rows)

