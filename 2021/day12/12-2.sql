DEALLOCATE problem12_2;

PREPARE problem12_2 AS
WITH RECURSIVE input(a, b, id) AS (
    SELECT
        matches[1],
        matches[2],
        id::integer
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        string_to_array(line, '-') AS s(matches)
    WHERE
        line != ''
), paths AS (
    SELECT
        0 as level,
        null::text AS a,
        'start' AS b,
        ARRAY['start']::text[] AS path,
        null::text AS double_visit,
        -1 AS id

    UNION ALL

    SELECT
        level + 1,
        paths.b,
        next,
        path||next,
        CASE
            WHEN double_visit IS NULL AND path @> array[lower(next)]
            THEN next
            ELSE double_visit
        END,
        input.id
    FROM
        paths
    JOIN
        input ON (paths.b=input.a OR paths.b=input.b)
    JOIN
        cast(
            CASE WHEN paths.b=input.a THEN input.b ELSE input.a END
            AS text
        ) AS _(next) ON (double_visit IS NULL OR NOT path @> array[lower(next)])
)
SELECT
    count(*)
FROM
    paths
WHERE
    (double_visit IS NULL OR double_visit NOT IN ('start', 'end'))
    AND path[array_length(path,1)] = 'end'
;

EXECUTE problem12_2($$fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW
$$);

\set input `cat 12.input`
EXECUTE problem12_2(:'input');

set track_io_timing to 'on';
\o 12-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem12_2(:'input');
\o
