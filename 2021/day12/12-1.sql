DEALLOCATE problem12_1;

PREPARE problem12_1 AS
WITH RECURSIVE input(a, b, id) AS (
    SELECT
        matches[1],
        matches[2],
        id::integer
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        string_to_array(line, '-') AS s(matches)
    WHERE
        line != ''
), paths AS (
    SELECT
        0 as level,
        null::text AS a,
        'start' AS b,
        ARRAY['start']::text[] AS path,
        -1 AS id

    UNION ALL

    SELECT
        level + 1,
        paths.b,
        next,
        path||next,
        input.id
    FROM
        paths
    JOIN
        input ON (paths.b=input.a OR paths.b=input.b)
    JOIN
        cast(
            CASE WHEN paths.b=input.a THEN input.b ELSE input.a END
            AS text
        ) AS _(next) ON (NOT path @> array[lower(next)])
)
SELECT
    count(*)
FROM
    paths
WHERE
    path[array_length(path,1)] = 'end'
;

EXECUTE problem12_1($$fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW$$);

\set input `cat 12.input`
EXECUTE problem12_1(:'input');

set track_io_timing to 'on';
\o 12-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem12_1(:'input');
\o
