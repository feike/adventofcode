DEALLOCATE problem01_1;

PREPARE problem01_1 AS
WITH input(increasing) AS (
	SELECT
		lag(depth::bigint) OVER (ORDER BY lineno) < depth::bigint
	FROM
		regexp_split_to_table($1, '\s+') WITH ORDINALITY AS sub(depth, lineno)
	WHERE
		depth != ''
)
SELECT
	count(*) FILTER (WHERE increasing)
FROM
	input
;

EXECUTE problem01_1($$
199
200
208
210
200
207
240
269
260
263
$$);

\set input `cat 01.input`
EXECUTE problem01_1(:'input');

set track_io_timing to 'on';
\o 01-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_1(:'input');
\o
