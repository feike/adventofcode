DEALLOCATE problem01_2;

PREPARE problem01_2 AS
WITH part2(this, previous, qualifies) AS (
	SELECT
		sum(depth) OVER (ORDER BY lineno ROWS BETWEEN 2 PRECEDING AND CURRENT ROW),
		sum(depth) OVER (ORDER BY lineno ROWS BETWEEN 3 PRECEDING AND 1 PRECEDING),
		lag(depth, 3) OVER (ORDER BY lineno) IS NOT NULL
	FROM
		regexp_split_to_table($1, '\s+') WITH ORDINALITY AS sub(line, lineno)
	CROSS JOIN
		cast(line AS bigint) c(depth)
	WHERE
		line != ''
)
SELECT
	count(*) FILTER (WHERE qualifies AND this > previous)
FROM
	part2
;

EXECUTE problem01_2($$
199
200
208
210
200
207
240
269
260
263
$$);

\set input `cat 01.input`
EXECUTE problem01_2(:'input');

set track_io_timing to 'on';
\o 01-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_2(:'input');
\o
