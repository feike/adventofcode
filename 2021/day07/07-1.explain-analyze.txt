                                                             QUERY PLAN                                                              
-------------------------------------------------------------------------------------------------------------------------------------
 Aggregate  (cost=75.03..75.04 rows=1 width=8) (actual time=1.004..1.004 rows=1 loops=1)
   CTE input
     ->  Function Scan on regexp_split_to_table s  (cost=0.00..15.00 rows=1000 width=4) (actual time=0.477..0.575 rows=1000 loops=1)
   ->  Nested Loop  (cost=22.50..52.52 rows=1000 width=8) (actual time=0.851..0.951 rows=1000 loops=1)
         ->  Aggregate  (cost=22.50..22.51 rows=1 width=4) (actual time=0.850..0.850 rows=1 loops=1)
               ->  CTE Scan on input input_1  (cost=0.00..20.00 rows=1000 width=4) (actual time=0.478..0.741 rows=1000 loops=1)
         ->  CTE Scan on input  (cost=0.00..20.00 rows=1000 width=4) (actual time=0.000..0.043 rows=1000 loops=1)
 Planning Time: 0.044 ms
 Execution Time: 1.025 ms
(9 rows)

