DEALLOCATE problem07_1;

PREPARE problem07_1 AS
WITH RECURSIVE input(crab) AS (
    SELECT
        crab::int
    FROM
        regexp_split_to_table($1, ',') AS s(crab)
)
SELECT
    sum(abs(median - crab))
FROM (
    SELECT
        percentile_disc(0.5) within group (order by crab) AS median
    FROM
        input
    ) sub
CROSS JOIN
    input
;

EXECUTE problem07_1($$16,1,2,0,4,2,7,1,2,14$$);

\set input `cat 07.input`
EXECUTE problem07_1(:'input');

set track_io_timing to 'on';
\o 07-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_1(:'input');
\o
