DEALLOCATE problem07_2;

PREPARE problem07_2 AS
WITH RECURSIVE input(crab) AS (
    SELECT
        crab::int
    FROM
        regexp_split_to_table($1, ',') AS s(crab)
), fuel_costs(distance, costs) AS (
    SELECT
        i,
        sum(i) OVER (order by i)
    FROM
        generate_series(1, abs((SELECT max(crab) - min(crab) FROM input))) AS s(i)
), diverge AS (
    -- We start at the median position
    SELECT
        position,
        direction,
        sum(costs) as costs
    FROM (
        SELECT
            percentile_disc(0.5) within group (order by crab) AS position
        FROM
            input
    ) sub
    CROSS JOIN
        input
    -- And move left and right from the median position
    CROSS JOIN
        (VALUES (-1), (1)) AS d(direction)
    JOIN
        fuel_costs ON (abs(crab - position) = distance)
    GROUP BY
        position,
        direction
        
    UNION ALL

    SELECT
        position + direction,
        direction,
        sub.costs
    FROM
        diverge
    CROSS JOIN LATERAL (
        SELECT
            sum(costs)
        FROM
            input
        JOIN
            fuel_costs ON (abs(crab - (position + direction)) = distance)
    ) sub(costs)
    -- until the costs no longer decrease
    WHERE
        sub.costs < diverge.costs
)
SELECT
    min(costs)
FROM
    diverge;

EXECUTE problem07_2($$16,1,2,0,4,2,7,1,2,14$$);

\set input `cat 07.input`
EXECUTE problem07_2(:'input');

set track_io_timing to 'on';
\o 07-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_2(:'input');
\o
