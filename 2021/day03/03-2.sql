DEALLOCATE problem03_2;

PREPARE problem03_2 AS
WITH RECURSIVE input AS (
    SELECT
        bit::int,
        substr(line, 1, bit::int) AS significant,
        count(*)
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS sub(line, lineno)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS sub2(value, bit)
    WHERE
        line != ''
    GROUP BY
        bit,
        significant
    ORDER BY
        bit,
        significant
), fields (max) AS (
    SELECT max(bit) FROM input
), walk AS (
    SELECT
        bit,
        CASE
            WHEN rank() OVER (ORDER BY count, significant) = 1
            THEN significant
        END AS co2,
        CASE
            WHEN rank() OVER (ORDER BY count DESC, significant DESC) = 1
            THEN significant
        END as oxy
    FROM
        input
    WHERE
        bit = 1
    
    UNION ALL
    
    SELECT
        i.bit,
        CASE
            WHEN co2 IS NULL
            THEN null
            WHEN rank() OVER (PARTITION BY co2 ORDER BY count, significant) = 1
            THEN significant
        END,
        CASE
            WHEN oxy IS NULL
            THEN null
            WHEN rank() OVER (PARTITION BY oxy ORDER BY count DESC, significant DESC) = 1
            THEN significant
        END
    FROM
        walk AS w
    JOIN
        input AS i ON (
            w.bit + 1 = i.bit
            AND substr(i.significant, 1, w.bit) IN (w.co2, w.oxy)
        )
)
SELECT
    lpad((SELECT co2 FROM walk WHERE bit = max AND co2 IS NOT NULL), 31, '0')::bit(31)::int
    *
    lpad((SELECT oxy FROM walk WHERE bit = max AND oxy IS NOT NULL), 31, '0')::bit(31)::int
FROM
    fields
;

EXECUTE problem03_2($$
00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
$$);

\set input `cat 03.input`
EXECUTE problem03_2(:'input');

set track_io_timing to 'on';
\o 03-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_2(:'input');
\o
