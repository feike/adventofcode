DEALLOCATE problem03_1;

PREPARE problem03_1 AS
WITH RECURSIVE input AS (
    SELECT
        max(bit) OVER () - bit AS bit,
        value::int
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS sub(line, lineno)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS sub2(value, bit)
    WHERE
        line != ''
), mcv(bit, gamma, epsilon) AS (
    SELECT
        DISTINCT ON (bit)
        bit,
        value,
        (value + 1) % 2
    FROM
        input
    GROUP BY
        bit,
        value
    ORDER BY
        bit,
        count(*) DESC
)
SELECT
    sum(gamma * decimal)
    *
    sum(epsilon * decimal)
FROM
    mcv
CROSS JOIN
    pow(2, bit) AS v(decimal)
;

EXECUTE problem03_1($$
00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
$$);

\set input `cat 03.input`
EXECUTE problem03_1(:'input');

set track_io_timing to 'on';
\o 03-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_1(:'input');
\o
