DEALLOCATE problem17_1;

/*
PREPARE problem17_1 AS
WITH RECURSIVE input(packets) AS MATERIALIZED (
    SELECT
        cast(packets AS bit varying)
    FROM
        rtrim(concat('x', $1::text), E' \n\t\r') AS _(packets)
)
SELECT
    *
FROM
    input;

EXECUTE problem17_1('target area: x=20..30, y=-10..-5');


x_t = min(0, x_0 * t - (t - 1) * x_0)
*/

WITH initial (x, y) AS (
    SELECT
        7,
        4
)
SELECT
    t,
    CASE
        WHEN x - t <= 0
        THEN 0
        ELSE x - t
    END AS v_x,
    y - t AS v_y,
    CASE
        WHEN x - t <= 0
        THEN x * x - (x * x)/2 - 1
        ELSE t * x - (t * t)/2 - 1
    END AS d_x,
    t * y - (t * t)/2 - 1 AS d_y
FROM
    generate_series(0, 60) AS _(t)
CROSS JOIN
    initial



/*

\set input `cat 17.input`
EXECUTE problem17_1(:'input');

set track_io_timing to 'on';
\o 17-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem17_1(:'input');
\o

*/
