DEALLOCATE problem14_2;

PREPARE problem14_2 AS
WITH RECURSIVE input(line, id) AS (
    SELECT
        line,
        rank() OVER (ORDER BY line_no)
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, line_no)
    WHERE
        line != ''
), rules(pair, east, west, id) AS MATERIALIZED (
    SELECT
        matches[1]||matches[2],
        matches[1]||matches[3],
        matches[3]||matches[2],
        rank() OVER (ORDER BY id)
    FROM
        input
    CROSS JOIN
        regexp_matches(line, '^(.)(.)\s*->\s*(.)$') AS _(matches)
    WHERE
        id > 1
), just_ids (id) AS MATERIALIZED (
    SELECT
        id
    FROM
        rules
), replication AS MATERIALIZED (
    SELECT
        pair.id AS pair_id,
        east.id AS child_id
    FROM
        rules AS pair
    JOIN
        rules AS east ON (pair.east=east.pair)

    UNION ALL

    SELECT
        pair.id,
        west.id
    FROM
        rules AS pair
    JOIN
        rules AS west ON (pair.west=west.pair)
), template(polymer) AS MATERIALIZED (
    SELECT
        line
    FROM
        input
    CROSS JOIN
        regexp_split_to_array(line, '')
    WHERE
        id = 1
), template_tokens AS (
    SELECT
        id,
        count(sub)
    FROM
        rules
    LEFT JOIN (
            SELECT
                substring(polymer, i, 2) AS pair
            FROM
                template
            CROSS JOIN
                generate_series(1, length(polymer) - 1) AS  _(i)
        ) sub USING (pair)
    GROUP BY
        id
), process AS (
    SELECT
        0 AS inc,
        array_agg(count::numeric ORDER BY id) AS counts
    FROM
        template_tokens

    UNION ALL

    SELECT
        inc + 1,
        child_counts
    FROM
        process
    CROSS JOIN LATERAL (
        SELECT
            array_agg(coalesce(count, 0) ORDER BY id)
        FROM
            just_ids
        LEFT JOIN (
            SELECT
                child_id,
                sum(count) AS count
            FROM
                unnest(counts) WITH ORDINALITY AS _(count, id)
            JOIN
                replication ON (id=pair_id)
            GROUP BY
                child_id
        ) _ ON (id=child_id)
    ) _(child_counts)
    WHERE
        inc < 40
), element_count (amount) AS (
    SELECT
        -- We are counting every element twice, as they are part of 2 pairs always,
        -- except for the single elements on the head and the tail of the
        -- polymer, so we need to adjust for those 2 elements
        CASE
            WHEN substring(polymer,1,1) = element
            THEN 1
            WHEN substring(polymer, length(polymer)) = element
            THEN 1
            ELSE 0
        END + sum(amount)::bigint/2
    FROM
        template
    CROSS JOIN
        process
    CROSS JOIN
        unnest(counts) WITH ORDINALITY AS _(amount, id)
    JOIN
        rules USING (id)
    CROSS JOIN
        regexp_split_to_table(pair, '') AS rstt(element)
    WHERE
        inc = 40
    GROUP BY
        inc,
        element,
        polymer
)
SELECT
    max(amount)
    -
    min(amount)
FROM
    element_count;


EXECUTE problem14_2($$
NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C$$); 

\set input `cat 14.input`
EXECUTE problem14_2(:'input');

set track_io_timing to 'on';
\o 14-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem14_2(:'input');
\o
