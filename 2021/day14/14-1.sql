DEALLOCATE problem14_1;

PREPARE problem14_1 AS
WITH RECURSIVE input(line, id) AS (
    SELECT
        line,
        rank() OVER (ORDER BY line_no)
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, line_no)
    WHERE
        line != ''
), template(polymer) AS MATERIALIZED (
    SELECT
        line
    FROM
        input
    WHERE
        id = 1
), rules(east, west, middle) AS MATERIALIZED (
    SELECT
        matches[1],
        matches[2],
        matches[3]
    FROM
        input
    CROSS JOIN
        regexp_matches(line, '^(.)(.)\s*->\s*(.)$') AS _(matches)
    WHERE
        id > 1
), steps(east, west, path, step) AS (
    SELECT
        substring(polymer, i, 1),
        substring(polymer, i+1, 1),
        ARRAY[i]::int[],
        0
    FROM
        template
    CROSS JOIN
        generate_series(1, length(polymer)) AS  _(i)

    UNION ALL

    SELECT
        CASE
            WHEN rules.middle IS NULL THEN east
            WHEN i = 1 THEN east
            WHEN i = 2 THEN middle
        END,
        CASE
            WHEN rules.middle IS NULL THEN west
            WHEN i = 1 THEN middle
            WHEN i = 2 THEN west
        END,
        path||i,
        step + 1
    FROM
        steps
    LEFT JOIN
        rules USING (east, west)
    CROSS JOIN
        generate_series(1, CASE WHEN rules.middle IS NULL THEN 1 ELSE 2 END) AS _(i)
    WHERE
        step < 10
)
SELECT
    max(count(*)) OVER ()
    -
    min(count(*)) OVER ()
FROM
    steps
WHERE
    step = 10
GROUP BY
    step,
    east
LIMIT
    1;


EXECUTE problem14_1($$
NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C$$);

\set input `cat 14.input`
EXECUTE problem14_1(:'input');

set track_io_timing to 'on';
\o 14-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem14_1(:'input');
\o
