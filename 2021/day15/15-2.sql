DEALLOCATE problem15_2;

PREPARE problem15_2 AS
WITH RECURSIVE input(x, y, risk, width, height) AS MATERIALIZED (
    SELECT
        (x-1)::smallint,
        (y-1)::smallint,
        risk::smallint,
        max(x) OVER (),
        max(y) OVER ()
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS sub(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS sub2(risk, x)
    WHERE
        line != ''
), grid AS MATERIALIZED (
    SELECT
        x,
        y,
        (y * width * 5 + x + 1)::integer AS position,
        risk
    FROM (
        SELECT
            (x + width  * grid_x)::smallint AS x,
            (y + height * grid_y)::smallint AS y,
            CASE
                WHEN risk + grid_x + grid_y > 9
                THEN risk + grid_x + grid_y - 9
                ELSE risk + grid_x + grid_y
            END::smallint AS risk,
            width,
            height
        FROM
            input
        CROSS JOIN
            generate_series(0, 4) AS gsx(grid_x)
        CROSS JOIN
            generate_series(0, 4) AS gsy(grid_y)
    ) _
), bounds AS MATERIALIZED (
    SELECT
        max(x)::smallint AS max_x,
        max(y)::smallint AS max_y,
        max(position)::integer AS max_position
    FROM
        grid
), walk AS (
    SELECT
        array_agg(CASE WHEN x=0 AND y=0 THEN 0 ELSE 30000 END::integer) AS nodes,
        1::integer AS position,
        0::smallint AS x,
        0::smallint AS y,
        0 AS level
    FROM
        grid
    CROSS JOIN
        bounds
    UNION ALL
    SELECT
        new_nodes,
        new_position,
        new_x,
        new_y,
        (level + 1)
    FROM
        walk
    CROSS JOIN LATERAL (
        WITH new_nodes AS (
            SELECT
                position::integer,
                x,
                y,
                CASE
                    WHEN old_node IS NULL
                    THEN null
                    WHEN grid.position = walk.position
                    THEN null
                    WHEN abs(grid.y - walk.y) + abs(grid.x - walk.x) = 1
                    THEN least(old_node, grid.risk + nodes[walk.position])
                    ELSE old_node
                END AS risk
            FROM
                grid
            JOIN
                unnest(nodes) WITH ORDINALITY AS _(old_node, position) USING (position)
        ), next_position(new_position, new_x, new_y) AS (
            SELECT
                position,
                x,
                y
            FROM
                new_nodes
            WHERE
                risk IS NOT NULL
            ORDER BY
                risk ASC,
                position DESC
            LIMIT
                1
        )
        SELECT
            (SELECT array_agg(risk ORDER BY position) FROM new_nodes),
            *
        FROM
            next_position
    ) _(new_nodes, new_position)
    WHERE
        position != (SELECT max_position FROM bounds)
)
SELECT
    nodes[max_position]
FROM
    walk
CROSS JOIN
    bounds
ORDER BY
    level DESC
LIMIT
    1;

EXECUTE problem15_2(
$$1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581$$);

\set input `cat 15.input`
EXECUTE problem15_2(:'input');

set track_io_timing to 'on';
\o 15-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem15_2(:'input');
\o
