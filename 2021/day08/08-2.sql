DEALLOCATE problem08_2;

PREPARE problem08_2 AS
WITH RECURSIVE input(entry, patterns, output) AS (
    SELECT
        entry,
        patterns,
        output
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS s(signals, entry)
    CROSS JOIN
        regexp_split_to_array(signals, '\|') AS rsta(components)
    CROSS JOIN
        regexp_split_to_array(trim(both ' ' from components[1]), '\s') AS rsta2(patterns)
    CROSS JOIN
        regexp_split_to_array(trim(both ' ' from components[2]), '\s') AS rsta3(output)
), first_three(entry, pattern, digit) AS (
    SELECT
        entry,
        regexp_split_to_array(pattern, ''),
        CASE length(pattern)
            WHEN 2 THEN 1
            WHEN 3 THEN 7
            WHEN 4 THEN 4
            WHEN 7 THEN 8
        END
    FROM
        input
    CROSS JOIN
        unnest(patterns) AS u(pattern)
), next_three(entry, pattern, digit) AS (
    SELECT
        k1.entry,
        k1.pattern,
        CASE count(1) FILTER (WHERE k1.pattern @> k2.pattern)
            WHEN 3 THEN 9
            WHEN 2 THEN 0
            WHEN 0 THEN 6
        END
    FROM
        first_three AS k1
    JOIN
        first_three AS k2 ON (k1.entry = k2.entry AND k2.digit IN (1, 4, 7))
    WHERE
        array_length(k1.pattern, 1) = 6
    GROUP BY
        k1.entry,
        k1.pattern
), last_three(entry, pattern, digit) AS (
    SELECT
        k1.entry,
        k1.pattern,
        CASE
            WHEN nine.pattern @> k1.pattern THEN
                CASE
                    WHEN six.pattern @> k1.pattern THEN 5
                    ELSE 3
                END
            ELSE 2
        END
    FROM
        first_three AS k1
    JOIN
        next_three AS nine ON (k1.entry=nine.entry AND nine.digit = 9)
    JOIN
        next_three AS six ON (k1.entry=six.entry AND six.digit=6)
    WHERE
        array_length(k1.pattern, 1) = 5
), all_digits AS (
    SELECT
        *
    FROM
        first_three
    WHERE
        digit IS NOT NULL
    UNION ALL
    SELECT
        *
    FROM
        next_three
    UNION ALL
    SELECT
        *
    FROM
        last_three
)
SELECT
    sum(string_agg(digit::text, '' ORDER BY idx)::int) OVER ()
FROM
    all_digits
JOIN
    input USING (entry)
CROSS JOIN
    unnest(output) WITH ORDINALITY AS u(output, idx)
JOIN
    regexp_split_to_array(u.output, '') AS rstt(signal) ON (all_digits.pattern @> rstt.signal AND rstt.signal @> all_digits.pattern)
GROUP BY
    entry
LIMIT
    1
;

EXECUTE problem08_2($$be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce$$);

\set input `cat 08.input`
EXECUTE problem08_2(:'input');

set track_io_timing to 'on';
\o 08-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_2(:'input');
\o
