DEALLOCATE problem08_1;

PREPARE problem08_1 AS
WITH RECURSIVE input(entry, patterns, output) AS (
    SELECT
        entry,
        patterns,
        digits
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS s(signals, entry)
    CROSS JOIN
        regexp_split_to_array(signals, '\|') AS rsta(components)
    CROSS JOIN
        regexp_split_to_array(trim(both ' ' from components[1]), '\s') AS rsta2(patterns)
    CROSS JOIN
        regexp_split_to_array(trim(both ' ' from components[2]), '\s') AS rsta3(digits)
)
SELECT
    sum(count(*)) OVER ()
FROM
    input
CROSS JOIN
    unnest(output) AS u(digit)
WHERE
    length(digit) IN (2, 4, 3, 7)
GROUP BY
    entry
LIMIT
    1
;

EXECUTE problem08_1($$be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce$$);

\set input `cat 08.input`
EXECUTE problem08_1(:'input');

set track_io_timing to 'on';
\o 08-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_1(:'input');
\o
