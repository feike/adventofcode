DEALLOCATE problem06_2;

PREPARE problem06_2 AS
WITH RECURSIVE input AS (
    SELECT
        timer,
        count(fish)
    FROM
        generate_series(0,8) AS days(timer)
    LEFT JOIN
        regexp_split_to_table($1, ',') AS r(fish) ON (fish::int=timer)
    GROUP BY
        timer
), to_list(timers) AS (
    SELECT
        array_agg(count ORDER BY timer)
    FROM
        input
), breed AS (
    SELECT
        0 AS day,
        *
    FROM
        to_list
    
    UNION ALL

    SELECT
        day + 1,
        -- sql 99: arrays in SQL are 1-based, *not* 0-based
        ARRAY[
            timers[2],
            timers[3],
            timers[4],
            timers[5],
            timers[6],
            timers[7],
            timers[8] + timers[1],
            timers[9],
            timers[1]
        ]
    FROM
        breed
    WHERE
        day < 257
)
SELECT
    sum(timer)
FROM
    breed
CROSS JOIN
    unnest(timers) AS sub(timer)
WHERE
    day = 256
;

EXECUTE problem06_2($$3,4,3,1,2$$);

\set input `cat 06.input`
EXECUTE problem06_2(:'input');

set track_io_timing to 'on';
\o 06-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_2(:'input');
\o
