DEALLOCATE problem04_2;

PREPARE problem04_2 AS
WITH input(line, card, y, x, value) AS (
    SELECT
        lineno::int,
        ((lineno-3)/6)::smallint,
        ((lineno-3)%6)::smallint,
        (col - 1)::smallint,
        value::smallint
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS sub(line, lineno)
    CROSS JOIN
        regexp_split_to_table(ltrim(line, ' '), '(\s+|,)') WITH ORDINALITY AS sub2(value, col)
    WHERE
        line != ''
        AND value != ''
), draw(draw, value) AS (
    SELECT
        x,
        value
    FROM
        input
    WHERE
        line = 1
), cards (card, n_draws) AS (
    SELECT
        card,
        max(draw.draw)
    FROM
        input
    JOIN
        draw ON (input.value=draw.value)
    WHERE
        line > 1
    GROUP BY
        GROUPING SETS ((card, x), (card, y))
), slowest_card (card, n_draws) as (
    SELECT
        card,
        min(n_draws)
    FROM
        cards
    GROUP BY
        card
    ORDER BY
        min(n_draws) DESC
    LIMIT 1
)
SELECT
    sum(draw.value) * last_draw.value
FROM
    slowest_card
JOIN
    input USING (card)
LEFT JOIN
    draw ON (draw.value=input.value AND draw > n_draws)
JOIN
    draw as last_draw ON (last_draw.draw = n_draws)
GROUP BY
    last_draw.value;

EXECUTE problem04_2($$7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7
$$);

\set input `cat 04.input`
EXECUTE problem04_2(:'input');

set track_io_timing to 'on';
\o 04-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem04_2(:'input');
\o
