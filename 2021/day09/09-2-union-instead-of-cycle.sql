DEALLOCATE problem09_2;

PREPARE problem09_2 AS
WITH RECURSIVE input AS (
    SELECT
        height::smallint,
        x::smallint,
        y::smallint
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS s(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt(height, x)
), find_low_points(height, x, y, is_low_point, basin) AS (
    SELECT
        height,
        x,
        y,
        height < least(
            lag(height)  OVER (horizontal),
            lead(height) OVER (horizontal),
            lag(height)  OVER (vertical),
            lead(height) OVER (vertical)
        ),
        rank() OVER (ORDER BY y, x)::int
    FROM
        input
    WINDOW
        horizontal AS (PARTITION BY y ORDER BY x),
        vertical   AS (PARTITION BY x ORDER BY y)
), basins AS (
    SELECT
        basin,
        x,
        y,
        height
    FROM
        find_low_points
    WHERE
        is_low_point

    UNION

    SELECT
        basin,
        input.x,
        input.y,
        input.height
    FROM
        basins
    CROSS JOIN
        (VALUES (0, 1), (0, -1), (1, 0), (-1, 0)) AS d(delta_x, delta_y)
    JOIN
        input ON (
            input.x = basins.x + delta_x
            AND input.y = basins.y + delta_y
            AND input.height >= basins.height
        )
    WHERE
        input.height < 9

), top(size, rank) AS (
    SELECT
        -- When we simulate the flowing in the recursive term, some points
        -- will be visited
        -- by different neighbours
        -- in which case we would count them more than once.
        -- Therefore, we need a distinct here.
        count(distinct (x, y)) AS size,
        rank() OVER (ORDER BY count(distinct (x, y)) DESC, basin ASC)
    FROM
        basins
    GROUP BY
        basin
    ORDER BY
        rank ASC
    LIMIT
        3
)
SELECT
    t1.size * t2.size * t3.size
FROM
    top AS t1,
    top AS t2,
    top AS t3
WHERE
    t1.rank = 1
    AND t2.rank = 2
    AND t3.rank = 3;

EXECUTE problem09_2($$2199943210
3987894921
9856789892
8767896789
9899965678$$);

\set input `cat 09.input`
EXECUTE problem09_2(:'input');

set track_io_timing to 'on';
\o 09-2-union.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_2(:'input');
\o
