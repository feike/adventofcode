DEALLOCATE problem09_1;

PREPARE problem09_1 AS
WITH RECURSIVE input AS (
    SELECT
        height::smallint,
        x,
        y
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS s(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt(height, x)
), find_low_points(height, is_low_point) AS (
    SELECT
        height,
        height < least(
            lag(height)  OVER (horizontal),
            lead(height) OVER (horizontal),
            lag(height)  OVER (vertical),
            lead(height) OVER (vertical)
        )
    FROM
        input
    WINDOW
        horizontal AS (PARTITION BY y ORDER BY x),
        vertical   AS (PARTITION BY x ORDER BY y)
)
SELECT
    sum(1+height)
FROM
    find_low_points
WHERE
    is_low_point;

EXECUTE problem09_1($$2199943210
3987894921
9856789892
8767896789
9899965678$$);

\set input `cat 09.input`
EXECUTE problem09_1(:'input');

set track_io_timing to 'on';
\o 09-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_1(:'input');
\o
