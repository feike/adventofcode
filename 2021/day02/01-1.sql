DEALLOCATE problem02_1;

PREPARE problem02_1 AS
WITH input AS (
	SELECT
		*
	FROM
		regexp_split_to_table($1, '\s+') WITH ORDINALITY AS sub(line, lineno)
	WHERE
		line != ''
)
SELECT
	*
FROM
	input
;

EXECUTE problem02_1($$
forward 5
down 5
forward 8
up 3
down 8
forward 2
$$);

\set input `cat 02.input`
EXECUTE problem02_1(:'input');

set track_io_timing to 'on';
\o 02-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_1(:'input');
\o
