DEALLOCATE problem02_1;

PREPARE problem02_1 AS
WITH RECURSIVE input AS (
    SELECT
        fields[1] AS direction,
        fields[2]::int AS amount,
        lineno
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS sub(line, lineno)
    CROSS JOIN
        regexp_split_to_array(line, '\s+') AS sub2(fields)
    WHERE
        line != ''
), walk AS (
SELECT
    0 AS x,
    0 AS z,
    (SELECT min(lineno) - 1 FROM input) AS lineno

UNION ALL

SELECT
    CASE direction
        WHEN 'forward' THEN x + amount
        ELSE x
    END,
    CASE direction
        WHEN 'down' THEN z + amount
        WHEN 'up'   THEN z - amount
        ELSE z
    END,
    input.lineno
FROM
    walk
JOIN
    input ON (input.lineno=walk.lineno+1)
)
SELECT
    x * z
FROM
    walk
ORDER BY
    lineno DESC
LIMIT 1
;

EXECUTE problem02_1($$
forward 5
down 5
forward 8
up 3
down 8
forward 2
$$);

\set input `cat 02.input`
EXECUTE problem02_1(:'input');

set track_io_timing to 'on';
\o 02-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_1(:'input');
\o
