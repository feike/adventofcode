DEALLOCATE problem10_1;

PREPARE problem10_1 AS
WITH RECURSIVE input AS (
    SELECT
        line_no::smallint,
        instructions
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS s(instructions, line_no)
), delimiters(open, close, points) AS (
    VALUES 
        ('('::char, ')'::char, 3),
        ('[', ']', 57),
        ('{', '}', 1197),
        ('<', '>', 25137)
), walk AS (
    SELECT
        line_no,
        1 AS position,
        instruction AS stack
    FROM
        input
    CROSS JOIN
        substring(instructions, 1, 1) AS s(instruction)
    JOIN
        delimiters ON (open=instruction)

    UNION ALL

    SELECT
        walk.line_no,
        position + 1,
        CASE
            WHEN open = instruction
            THEN stack||open
            WHEN open = substring(stack, length(stack))
            THEN substring(stack, 0, length(stack))
            ELSE null
        END
    FROM
        walk
    JOIN
        input ON (walk.line_no=input.line_no)
    CROSS JOIN
        substring(instructions, position+1, 1) AS s(instruction)
    JOIN
        delimiters ON (instruction = open OR instruction = close)
    WHERE
        stack IS NOT NULL
)
SELECT
    sum(points)
FROM
    walk
JOIN
    input USING (line_no)
JOIN
    delimiters ON (close = substring(instructions, position, 1))
WHERE
    stack IS NULL;

EXECUTE problem10_1($$[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]$$);

\set input `cat 10.input`
EXECUTE problem10_1(:'input');

set track_io_timing to 'on';
\o 10-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem10_1(:'input');
\o