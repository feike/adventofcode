DEALLOCATE problem10_2;

PREPARE problem10_2 AS
WITH RECURSIVE input AS (
    SELECT
        line_no::smallint,
        instructions
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS s(instructions, line_no)
), delimiters(open, close, points) AS (
    VALUES 
        ('('::char, ')'::char, 1),
        ('[', ']', 2),
        ('{', '}', 3),
        ('<', '>', 4)
), walk AS (
    SELECT
        line_no,
        1 AS position,
        close AS stack
    FROM
        input
    CROSS JOIN
        substring(instructions, 1, 1) AS s(instruction)
    JOIN
        delimiters ON (open=instruction)

    UNION ALL

    SELECT
        walk.line_no,
        position + 1,
        CASE
            WHEN open = instruction
            THEN close||stack
            WHEN close = substring(stack, 1, 1)
            THEN substring(stack, 2)
            ELSE null
        END
    FROM
        walk
    JOIN
        input ON (walk.line_no=input.line_no)
    CROSS JOIN
        substring(instructions, position+1, 1) AS s(instruction)
    JOIN
        delimiters ON (instruction = open OR instruction = close)
    WHERE
        stack IS NOT NULL
), individual_scores AS (
    SELECT
        walk.line_no,
        idx,
        points
    FROM
        walk
    JOIN
        input ON (walk.line_no=input.line_no AND position = length(instructions))
    CROSS JOIN
        regexp_split_to_table(stack, '') WITH ORDINALITY AS u(close, idx)
    JOIN
        delimiters USING (close)
), aggregator AS (
   SELECT
        line_no,
        idx,
        points::bigint
    FROM
        individual_scores
    WHERE
        idx = 1

    UNION ALL

    SELECT
        agg.line_no,
        scores.idx,
        agg.points * 5 + scores.points
    FROM
        aggregator AS agg
    JOIN
        individual_scores AS scores ON (scores.line_no = agg.line_no AND agg.idx+1=scores.idx)
), scored AS (
    SELECT
        DISTINCT ON (line_no)
        line_no,
        points
    FROM
        aggregator
    ORDER BY
        line_no,
        points DESC
)
SELECT
    percentile_disc(0.5) WITHIN GROUP (ORDER BY points)
FROM
    scored;

EXECUTE problem10_2($$[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]$$);

SELECT clock_timestamp();
\set input `cat 10.input`
EXECUTE problem10_2(:'input');
SELECT clock_timestamp();

set track_io_timing to 'on';
\o 10-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem10_2(:'input');
\o
