DEALLOCATE problem09_2;

PREPARE problem09_2 AS
WITH RECURSIVE input(id, history) AS (
    SELECT
        id,
        matches::int[]
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        regexp_split_to_array(line, '\s+') AS rsta(matches)
    WHERE
        line !~ '^\s*$'
), walk AS (
    SELECT
        id,
        history,
        0 AS level
    FROM
        input
        
    UNION ALL

    SELECT * FROM (
        WITH subwalk AS (
            SELECT
                id,
                h - lag(h) OVER (PARTITION BY id ORDER BY col) AS h,
                level + 1 AS level
            FROM
                walk
            CROSS JOIN
                unnest(history) WITH ORDINALITY AS un(h, col)
            WHERE
                NOT history <@ '{0}'::int[]
        )
        SELECT
            id,
            array_agg(h),
            level
        FROM
            subwalk
        WHERE
            h IS NOT NULL
        GROUP BY
            id,
            level
    ) _
), revert_walk AS (
    SELECT * FROM (
        SELECT DISTINCT ON (id)
            id,
            0||history AS history,
            level
        FROM
            walk
        ORDER BY
            id,
            level DESC
    ) _

    UNION ALL

    SELECT
        w.id,
        (w.history[1] - rw.history[1])||w.history,
        w.level
    FROM
        revert_walk AS rw
    JOIN
        walk AS w ON (rw.id=w.id AND rw.level-1=w.level)
    CROSS JOIN
        array_length(w.history,1) AS _(len)
)
SELECT
    sum(history[1])
FROM
    revert_walk
WHERE
    level = 0
;

EXECUTE problem09_2($$
0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
$$);

\set input `cat 09.input`
EXECUTE problem09_2(:'input');

set track_io_timing to 'on';
\o 09-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_2(:'input');
\o
