DEALLOCATE problem25_2;

PREPARE problem25_2 AS
WITH input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    *
FROM
    input
;

EXECUTE problem25_2($$
dummy line
$$);

\set input `cat 25.input`
EXECUTE problem25_2(:'input');

set track_io_timing to 'on';
\o 25-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem25_2(:'input');
\o
