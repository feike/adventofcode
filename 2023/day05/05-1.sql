DEALLOCATE problem05_1;

PREPARE problem05_1 AS
WITH RECURSIVE input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
), seeds AS (
    SELECT
        seed::bigint
    FROM
        input
    CROSS JOIN
        regexp_split_to_table(split_part(line, ':', 2), '\s+') AS rm(seed)
    WHERE
        line ~ 'seeds:'
        AND seed != ''
), maps AS (
    WITH RECURSIVE walk AS (
        SELECT
            id,
            null::text AS map,
            null::bigint[] AS numbers
        FROM
            input
        WHERE
            id = (SELECT min(id) FROM input)

        UNION ALL

        SELECT
            input.id,
            CASE WHEN line ~ ':' THEN split_part(line, ' ', 1) ELSE walk.map END,
            rsta.numbers::bigint[]
        FROM
            walk
        JOIN
            input ON (walk.id+1=input.id)
        LEFT JOIN
            regexp_split_to_array(line, '\s+') AS rsta(numbers) ON (line !~ ':')
    )
    SELECT
        id,
        kinds[1] AS source,
        kinds[2] AS dest,
        numbers[2] AS source_idx,
        numbers[1] AS dest_idx,
        numbers[3] AS len
    FROM
        walk
    CROSS JOIN
        regexp_split_to_array(map, '-to-') AS rsta(kinds)
    WHERE
        numbers IS NOT NULL
), mappings AS (
    SELECT DISTINCT
        source,
        dest
    FROM
        maps
), gardening AS (
    SELECT
        seed,
        seed AS location,
        'seed' AS source,
        0 AS level
    FROM
        seeds

    UNION ALL

    SELECT
        seed,
        coalesce(maps.dest_idx + (g.location - maps.source_idx), g.location),
        m.dest,
        level + 1
    FROM
        gardening AS g
    JOIN
        mappings AS m USING (source)
    LEFT JOIN
        maps ON (m.source=maps.source AND g.location >= maps.source_idx AND g.location < (maps.source_idx+maps.len))
)
SELECT
    min(location)
FROM
    gardening
WHERE
    source = 'location'
;

EXECUTE problem05_1($$
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
$$);

\set input `cat 05.input`
EXECUTE problem05_1(:'input');

set track_io_timing to 'on';
\o 05-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem05_1(:'input');
\o
