DEALLOCATE problem05_2;

PREPARE problem05_2 AS
WITH RECURSIVE input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
), maps AS (
    WITH RECURSIVE walk AS (
        SELECT
            id,
            null::text AS map,
            null::bigint[] AS numbers
        FROM
            input
        WHERE
            id = (SELECT min(id) FROM input)

        UNION ALL

        SELECT
            input.id,
            CASE WHEN line ~ ':' THEN split_part(line, ' ', 1) ELSE walk.map END,
            rsta.numbers::bigint[]
        FROM
            walk
        JOIN
            input ON (walk.id+1=input.id)
        LEFT JOIN
            regexp_split_to_array(line, '\s+') AS rsta(numbers) ON (line !~ ':')
    )
    SELECT
        kinds[1] AS source,
        kinds[2] AS dest,
        int8range(numbers[2], numbers[2]+numbers[3], '[)') AS source_range,
        numbers[1] - numbers[2] AS delta
    FROM
        walk
    CROSS JOIN
        regexp_split_to_array(map, '-to-') AS rsta(kinds)
    WHERE
        numbers IS NOT NULL
), seeds AS (
    SELECT
        int8multirange(VARIADIC
            array_agg(int8range(
                prt[1]::bigint,
                prt[1]::bigint + prt[2]::bigint,
                '[)'
            ))
        ) AS seeds
    FROM
        input
    CROSS JOIN
        regexp_matches(line, '(\d+\s+\d+)', 'g') AS rm(matches)
    CROSS JOIN
        regexp_split_to_array(matches[1], '\s+') AS rsta(prt)
    WHERE
        line ~ 'seeds:'
), mappings AS (
    SELECT
        source,
        dest,
        range_agg(source_range) AS mapping_all_matches
    FROM
        maps
    GROUP BY
        source,
        dest
), walk AS (
    SELECT
        seeds,
        'seed' AS source,
        0::int AS level
    FROM
        seeds

    UNION ALL

    SELECT * FROM (
        WITH abc AS (
            SELECT
                CASE
                    WHEN maps IS NULL
                    THEN seed
                    ELSE int8range(lower(overlap) + delta, upper(overlap) + delta, '[)')
                END AS pivot,
                int8multirange(seed) - mapping_all_matches AS not_matching,
                walk.source,
                level + 1 AS level
            FROM
                walk
            JOIN
                mappings USING (source)
            CROSS JOIN
                unnest(seeds) AS u(seed)
            LEFT JOIN
                maps ON (maps.source=walk.source AND seed && source_range)
            LEFT JOIN
                range_intersect(seed, source_range) AS ri(overlap) ON (true)
        )
        SELECT
            range_agg(abc.not_matching) + range_agg(pivot),
            dest,
            level
        FROM
            abc
        JOIN
            mappings USING (source)
        GROUP BY
            dest,
            level
    ) _
)
SELECT
    min(lower(seeds))
FROM
    walk
WHERE
    source = 'location'
;

EXECUTE problem05_2($$
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
$$);

\set input `cat 05.input`
EXECUTE problem05_2(:'input');

set track_io_timing to 'on';
\o 05-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem05_2(:'input');
\o
