DEALLOCATE problem16_1;

PREPARE problem16_1 AS
WITH input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    *
FROM
    input
;

EXECUTE problem16_1($$
dummy line
$$);

\set input `cat 16.input`
EXECUTE problem16_1(:'input');

set track_io_timing to 'on';
\o 16-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem16_1(:'input');
\o
