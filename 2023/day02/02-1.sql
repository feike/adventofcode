DEALLOCATE problem02_1;

PREPARE problem02_1 AS
WITH input AS (
    SELECT
        split_part(r1[1], 'Game ', 2)::int as game,
        cube[1]::int AS count,
        cube[2] AS color
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        regexp_split_to_array(line, ':') AS r1(r1)
    CROSS JOIN
        regexp_split_to_table(r1[2], ';') WITH ORDINALITY AS r2(sets, set_id)
    CROSS JOIN
        regexp_split_to_table(sets, ',') AS r3(cubes)
    CROSS JOIN
        regexp_split_to_array(ltrim(cubes), '\s+') AS r4(cube)
), cumulative AS (
    SELECT
        game,
        color,
        sum(count),
        max(count)
    FROM
        input
    GROUP BY
        game,
        color
), single_line AS (
    SELECT
        coalesce(r.game, g.game) AS game,
        coalesce(r.max, 0) AS red,
        coalesce(g.max, 0) AS green,
        coalesce(b.max, 0) AS blue
    FROM
        (SELECT distinct game FROM cumulative) AS _(id)
    LEFT JOIN
        cumulative AS r ON (r.game=id AND r.color='red')
    LEFT JOIN
        cumulative AS g ON (g.game=id AND g.color='green')
    LEFT JOIN
        cumulative AS b ON (b.game=id AND b.color='blue')
)
SELECT
    sum(game)
FROM
    single_line
WHERE
    red <= 12
    AND green <= 13
    AND blue <= 14
;

EXECUTE problem02_1($$Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green$$);

\set input `cat 02.input`
EXECUTE problem02_1(:'input');

set track_io_timing to 'on';
\o 02-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_1(:'input');
\o
