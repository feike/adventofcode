DEALLOCATE problem14_1;

PREPARE problem14_1 AS
WITH input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    *
FROM
    input
;

EXECUTE problem14_1($$
dummy line
$$);

\set input `cat 14.input`
EXECUTE problem14_1(:'input');

set track_io_timing to 'on';
\o 14-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem14_1(:'input');
\o
