DEALLOCATE problem12_2;

PREPARE problem12_2 AS
WITH input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    *
FROM
    input
;

EXECUTE problem12_2($$
dummy line
$$);

\set input `cat 12.input`
EXECUTE problem12_2(:'input');

set track_io_timing to 'on';
\o 12-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem12_2(:'input');
\o
