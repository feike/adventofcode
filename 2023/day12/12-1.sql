DEALLOCATE problem12_1;

PREPARE problem12_1 AS
WITH RECURSIVE input AS (
    SELECT
        id,
        length(m[1]) AS total_gears,
        broken::int[],
        (SELECT sum(b::int) FROM unnest(broken) AS _(b)) AS total_broken,
        array_agg(spring::"char" ORDER BY position) AS springs,
        count(*) FILTER (WHERE spring = '?') AS unknown,
        count(*) FILTER (WHERE spring = '#') AS known_broken,
        count(*) FILTER (WHERE spring = '.') AS known_operational
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY _(line, id)
    CROSS JOIN
        regexp_split_to_array(line, ' ') AS rsta(m)
    CROSS JOIN
        regexp_split_to_array(m[2], ',') AS broken(broken)
    CROSS JOIN
        regexp_split_to_table(m[1], '') WITH ORDINALITY AS rstt(spring, position)
    WHERE
        line !~ '^\s*$'
    GROUP BY
        1, 2, 3, 4
), possibilities (p, gear, delta_broken, delta_operational, delta_unknown) AS (
    VALUES
        ('#'::"char", '#'::"char", 0, 0, 0),
        ('.', '.', 0, 0, 0),
        ('?', '.', 0, 1, -1),
        ('?', '#', 1, 0, -1)
), walk AS (
    SELECT
        input.*,
        1::int AS idx,
        '{}'::"char"[] AS gears,
        '{}'::int[] AS broken_groups
    FROM
        input
    
    UNION ALL

    SELECT
        id,
        total_gears,
        broken,
        total_broken,
        springs,
        unknown+delta_unknown,
        known_broken+delta_broken,
        known_operational+delta_operational,
        idx+1,
        gears||possibilities.gear,
        CASE
            WHEN gears[idx-1] IS DISTINCT FROM '#' AND possibilities.gear = '#'
            THEN broken_groups||1
            WHEN possibilities.gear = '#'
            THEN broken_groups[1:broken_groups_len-1]||broken_groups[broken_groups_len]+1
            ELSE broken_groups
        END
    FROM
        walk
    JOIN
        possibilities ON (springs[idx]=p)
    CROSS JOIN
        array_length(broken_groups, 1) AS broken_groups_len
    WHERE
        known_broken <= total_broken
        AND known_operational <= (total_gears - total_broken)
        AND broken_groups <= broken
)
SELECT
    count(*)
FROM
    walk
WHERE
    broken_groups = broken
    AND idx-1=total_gears
;

EXECUTE problem12_1($$
???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1
$$);

\set input `cat 12.input`
EXECUTE problem12_1(:'input');

set track_io_timing to 'on';
\o 12-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem12_1(:'input');
\o
