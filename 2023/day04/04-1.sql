DEALLOCATE problem04_1;

PREPARE problem04_1 AS
WITH RECURSIVE input AS (
    SELECT
        line,
        matches[1]::int AS game,
        win,
        mine
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_matches(line, 'Card\s+(\d+):\s*(.*)\|\s*(.*)') AS rm(matches)
    CROSS JOIN
        regexp_split_to_array(rtrim(matches[2]), ' ') AS winners(win)
    CROSS JOIN
        regexp_split_to_array(rtrim(matches[3]), '\s+') AS m(mine)
    WHERE
        line != ''
)
SELECT
    sum(pow(2, array_length((SELECT ARRAY(SELECT unnest(win) INTERSECT SELECT unnest(mine))), 1) - 1))
FROM
    input
;

EXECUTE problem04_1($$
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
$$);

\set input `cat 04.input`
EXECUTE problem04_1(:'input');

set track_io_timing to 'on';
\o 04-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem04_1(:'input');
\o
