DEALLOCATE problem08_1;

PREPARE problem08_1 AS
WITH RECURSIVE input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
), instructions AS MATERIALIZED (
    SELECT
        i,
        array_length(i, 1) AS i_len
    FROM
        input
    CROSS JOIN
        regexp_split_to_array(line, '') AS rsta(i)
    WHERE
        line ~ '^([RL])+'
    LIMIT
        1
), options (pos, west, east) AS MATERIALIZED (
    SELECT
        match[1],
        match[2],
        match[3]
    FROM
        input
    CROSS JOIN
        regexp_matches(line, '(\w+)\s+=\s+\((\w+),\s+(\w+)\)') AS rm(match)
    WHERE
        match[2] IS NOT NULL
), walk AS(
    SELECT
        1 AS i_idx,
        'AAA' AS pos,
        0::int AS level

    UNION ALL

    SELECT
        CASE
            WHEN i_idx >= i_len
            THEN 1
            ELSE i_idx + 1
        END,
        CASE
            i[i_idx]
            WHEN 'L' THEN west
            ELSE east
        END,
        level + 1
    FROM
        walk
    JOIN
        options USING (pos)
    CROSS JOIN
        instructions
    WHERE
        pos != 'ZZZ'
)
SELECT
    min(level)
FROM
    walk
WHERE
    pos = 'ZZZ'
;


EXECUTE problem08_1($$
RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)
$$);

EXECUTE problem08_1($$
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
$$);

\set input `cat 08.input`
EXECUTE problem08_1(:'input');

set track_io_timing to 'on';
\o 08-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_1(:'input');
\o
