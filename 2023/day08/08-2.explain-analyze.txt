                                                                      QUERY PLAN                                                                      
------------------------------------------------------------------------------------------------------------------------------------------------------
 Limit  (cost=14831.45..14831.45 rows=1 width=12) (actual time=14804.299..14804.322 rows=1 loops=1)
   CTE input
     ->  Function Scan on regexp_split_to_table _  (cost=0.00..12.50 rows=1000 width=40) (actual time=0.339..0.401 rows=708 loops=1)
   CTE instructions
     ->  Limit  (cost=0.00..0.07 rows=1 width=36) (actual time=0.032..0.049 rows=1 loops=1)
           ->  Nested Loop  (cost=0.00..33.75 rows=500 width=36) (actual time=0.032..0.032 rows=1 loops=1)
                 ->  CTE Scan on input  (cost=0.00..22.50 rows=500 width=32) (actual time=0.002..0.002 rows=1 loops=1)
                       Filter: (line ~ '^([RL])+'::text)
                 ->  Function Scan on regexp_split_to_array rsta  (cost=0.00..0.01 rows=1 width=32) (actual time=0.029..0.029 rows=1 loops=1)
   CTE options
     ->  Nested Loop  (cost=0.00..45.00 rows=1000 width=98) (actual time=0.364..5.492 rows=706 loops=1)
           ->  CTE Scan on input input_1  (cost=0.00..20.00 rows=1000 width=32) (actual time=0.340..0.516 rows=708 loops=1)
           ->  Function Scan on regexp_matches rm  (cost=0.00..0.01 rows=1 width=32) (actual time=0.006..0.007 rows=1 loops=708)
                 Filter: (match[2] IS NOT NULL)
   CTE walk
     ->  Recursive Union  (cost=0.00..5776.50 rows=80200 width=72) (actual time=0.571..14677.121 rows=107456 loops=1)
           ->  CTE Scan on options  (cost=0.00..22.50 rows=200 width=72) (actual time=0.571..5.665 rows=6 loops=1)
                 Filter: (pos ~~ '__A'::text)
                 Rows Removed by Filter: 700
           ->  Merge Join  (cost=210.00..415.00 rows=8000 width=72) (actual time=0.593..0.653 rows=5 loops=22412)
                 Merge Cond: (options_1.pos = walk.pos)
                 ->  Sort  (cost=79.85..82.35 rows=1000 width=132) (actual time=0.571..0.589 rows=567 loops=22412)
                       Sort Key: options_1.pos
                       Sort Method: quicksort  Memory: 1773kB
                       ->  Nested Loop  (cost=0.00..30.02 rows=1000 width=132) (actual time=0.000..0.087 rows=706 loops=22412)
                             ->  CTE Scan on instructions  (cost=0.00..0.02 rows=1 width=36) (actual time=0.000..0.000 rows=1 loops=22412)
                             ->  CTE Scan on options options_1  (cost=0.00..20.00 rows=1000 width=96) (actual time=0.000..0.033 rows=706 loops=22412)
                 ->  Sort  (cost=130.15..134.15 rows=1600 width=72) (actual time=0.002..0.003 rows=5 loops=22412)
                       Sort Key: walk.pos
                       Sort Method: quicksort  Memory: 25kB
                       ->  WorkTable Scan on walk  (cost=0.00..45.00 rows=1600 width=72) (actual time=0.000..0.001 rows=5 loops=22412)
                             Filter: (pos !~~ '__Z'::text)
                             Rows Removed by Filter: 0
   CTE first_z
     ->  WindowAgg  (cost=8136.82..8542.82 rows=200 width=100) (actual time=14795.881..14804.182 rows=6 loops=1)
           ->  Unique  (cost=8136.82..8537.82 rows=200 width=68) (actual time=14794.562..14804.169 rows=6 loops=1)
                 ->  Sort  (cost=8136.82..8337.32 rows=80200 width=68) (actual time=14794.561..14798.300 rows=107456 loops=1)
                       Sort Key: walk_1.start, walk_1.level DESC
                       Sort Method: quicksort  Memory: 8949kB
                       ->  CTE Scan on walk walk_1  (cost=0.00..1604.00 rows=80200 width=68) (actual time=0.572..14705.724 rows=107456 loops=1)
   CTE gcd
     ->  Recursive Union  (cost=5.50..449.53 rows=201 width=76) (actual time=14804.194..14804.294 rows=6 loops=1)
           ->  Limit  (cost=5.50..5.50 rows=1 width=76) (actual time=14804.194..14804.194 rows=1 loops=1)
                 ->  Sort  (cost=5.50..6.00 rows=200 width=76) (actual time=14804.193..14804.193 rows=1 loops=1)
                       Sort Key: first_z.start
                       Sort Method: top-N heapsort  Memory: 25kB
                       ->  CTE Scan on first_z  (cost=0.00..4.50 rows=200 width=76) (actual time=14795.884..14804.188 rows=6 loops=1)
           ->  Nested Loop  (cost=35.55..43.80 rows=20 width=76) (actual time=0.015..0.016 rows=1 loops=6)
                 CTE before
                   ->  Hash Join  (cost=0.33..5.22 rows=10 width=84) (actual time=0.003..0.004 rows=1 loops=6)
                         Hash Cond: (b.start = a.lead_start)
                         ->  CTE Scan on first_z b  (cost=0.00..4.00 rows=200 width=68) (actual time=0.000..0.000 rows=6 loops=5)
                         ->  Hash  (cost=0.20..0.20 rows=10 width=44) (actual time=0.001..0.001 rows=1 loops=6)
                               Buckets: 1024  Batches: 1  Memory Usage: 9kB
                               ->  WorkTable Scan on gcd a  (cost=0.00..0.20 rows=10 width=44) (actual time=0.001..0.001 rows=1 loops=6)
                 CTE calc
                   ->  Recursive Union  (cost=0.00..30.32 rows=340 width=16) (actual time=0.004..0.014 rows=6 loops=6)
                         ->  CTE Scan on before  (cost=0.00..0.20 rows=10 width=16) (actual time=0.004..0.004 rows=1 loops=6)
                         ->  WorkTable Scan on calc  (cost=0.00..2.33 rows=33 width=16) (actual time=0.001..0.001 rows=1 loops=36)
                               Filter: (b > 0)
                               Rows Removed by Filter: 0
                 ->  CTE Scan on calc calc_1  (cost=0.00..7.65 rows=2 width=8) (actual time=0.015..0.015 rows=1 loops=6)
                       Filter: (b = 0)
                       Rows Removed by Filter: 5
                 ->  CTE Scan on before before_1  (cost=0.00..0.20 rows=10 width=84) (actual time=0.000..0.000 rows=1 loops=5)
   ->  Sort  (cost=5.03..5.53 rows=201 width=12) (actual time=14804.298..14804.299 rows=1 loops=1)
         Sort Key: gcd.level DESC
         Sort Method: top-N heapsort  Memory: 25kB
         ->  CTE Scan on gcd  (cost=0.00..4.02 rows=201 width=12) (actual time=14804.196..14804.295 rows=6 loops=1)
 Planning Time: 0.307 ms
 Execution Time: 14805.235 ms
(71 rows)

