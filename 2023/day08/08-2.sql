DEALLOCATE problem08_2;

PREPARE problem08_2 AS
WITH RECURSIVE input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
), instructions AS MATERIALIZED (
    SELECT
        i,
        array_length(i, 1) AS i_len
    FROM
        input
    CROSS JOIN
        regexp_split_to_array(line, '') AS rsta(i)
    WHERE
        line ~ '^([RL])+'
    LIMIT
        1
), options (pos, west, east) AS MATERIALIZED (
    SELECT
        match[1],
        match[2],
        match[3],
        match[1] LIKE '__A' AS start,
        match[1] LIKE '__Z' AS finish
    FROM
        input
    CROSS JOIN
        regexp_matches(line, '(\w+)\s+=\s+\((\w+),\s+(\w+)\)') AS rm(match)
    WHERE
        match[2] IS NOT NULL
), walk AS(
    SELECT
        1 AS i_idx,
        pos,
        pos AS start,
        0::int AS level
    FROM
        options
    WHERE
        pos LIKE '__A'

    UNION ALL

    SELECT
        CASE
            WHEN i_idx >= i_len
            THEN 1
            ELSE i_idx + 1
        END,
        CASE
            i[i_idx]
            WHEN 'L' THEN west
            ELSE east
        END,
        walk.start,
        level + 1
    FROM
        walk
    JOIN
        options USING (pos)
    CROSS JOIN
        instructions
    WHERE
        pos NOT LIKE '__Z'
), first_z AS MATERIALIZED (
    SELECT
        start,
        lead(start) OVER (ORDER BY start) AS lead_start,
        pos,
        level
    FROM (
        SELECT DISTINCT ON (start)
            start,
            pos,
            level
        FROM
            walk
        ORDER BY
            start,
            level DESC
    ) _
), gcd AS (
    SELECT * FROM (
        SELECT
            start,
            lead_start,
            level::bigint AS gcd,
            0::int AS level
        FROM
            first_z
        ORDER BY
            start ASC
        LIMIT
            1
    ) _

    UNION ALL
    
    SELECT * FROM (
        WITH RECURSIVE before AS (
            SELECT
                b.start,
                b.lead_start,
                a.gcd AS a,
                b.level::bigint AS b,
                a.level + 1 AS level
            FROM
                gcd AS a
            JOIN
                first_z AS b ON (a.lead_start=b.start)
        ), calc AS (
            SELECT
                a,
                b
            FROM
                before

            UNION ALL

            SELECT
                b,
                mod(a, b)
            FROM
                calc
            WHERE
                b > 0
        )
        SELECT
            start,
            lead_start,
            before.a * (before.b/calc.a),
            level
        FROM
            before
        JOIN
            calc ON (calc.b=0)
    ) _
)
SELECT
    gcd
FROM
    gcd
ORDER BY
    level DESC
LIMIT
    1
;

EXECUTE problem08_2($$
LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)
$$);

\set input `cat 08.input`
EXECUTE problem08_2(:'input');

set track_io_timing to 'on';
\o 08-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_2(:'input');
\o
