DEALLOCATE problem07_1;

PREPARE problem07_1 AS
WITH cards(card, value) AS (
    VALUES
        ('A', 14),
        ('K', 13),
        ('Q', 12),
        ('J', 11),
        ('T', 10),
        ('9',  9),
        ('8',  8),
        ('7',  7),
        ('6',  6),
        ('5',  5),
        ('4',  4),
        ('3',  3),
        ('2',  2)
), input AS (
    SELECT
        id,
        split_part(line, ' ', 1) AS hand,
        array_agg(card ORDER BY position) AS cards,
        array_agg(value ORDER BY position) AS values,
        split_part(line, ' ', 2)::int AS bid
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        regexp_split_to_table(split_part(line, ' ', 1), '') WITH ORDINALITY AS rsta(card, position)
    JOIN
        cards USING (card)
    WHERE
        line != ''
    GROUP BY
        id,
        hand,
        split_part(line, ' ', 2)::int
), categories AS (
    SELECT
        id,
        hand,
        cards,
        values,
        value,
        bid,
        count(*)
    FROM
        input
    CROSS JOIN
        unnest(values) AS u(value)
    GROUP BY
        id,
        hand,
        cards,
        values,
        value,
        bid
), rankings AS (
    SELECT
        id,
        hand,
        cards,
        values,
        bid,
        array_agg(count ORDER BY count DESC, value DESC) AS rank_category,
        array_agg(value ORDER BY count DESC, value DESC) AS ranks
    FROM
        categories
    GROUP BY
        id,
        hand,
        cards,
        values,
        bid
)
SELECT
    sum(winnings)
FROM (
    SELECT
        bid * rank() OVER (ORDER BY rank_category ASC, values ASC) AS winnings
    FROM
        rankings
    ) _
;

EXECUTE problem07_1($$
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
$$);

\set input `cat 07.input`
EXECUTE problem07_1(:'input');

set track_io_timing to 'on';
\o 07-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_1(:'input');
\o
