DEALLOCATE problem07_2;

PREPARE problem07_2 AS
WITH cards(card, value) AS (
    VALUES
        ('A', 14),
        ('K', 13),
        ('Q', 12),
        ('J',  1),
        ('T', 10),
        ('9',  9),
        ('8',  8),
        ('7',  7),
        ('6',  6),
        ('5',  5),
        ('4',  4),
        ('3',  3),
        ('2',  2)
), input AS (
    SELECT
        id,
        split_part(line, ' ', 1) AS hand,
        array_agg(card ORDER BY position) AS cards,
        array_agg(value ORDER BY position) AS values,
        CASE
            WHEN split_part(line, ' ', 1) LIKE '%J%'
            THEN array_agg(distinct card)
            ELSE array[]::char[]
        END AS joker_cards,
        split_part(line, ' ', 2)::int AS bid
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        regexp_split_to_table(split_part(line, ' ', 1), '') WITH ORDINALITY AS rsta(card, position)
    JOIN
        cards USING (card)
    WHERE
        line != ''
    GROUP BY
        id,
        hand,
        split_part(line, ' ', 2)::int
), permutate AS (
    SELECT
        id,
        hand,
        bid,
        coalesce(replacement, 0) AS permutation,
        array_agg(value ORDER BY position) AS values,
        array_agg(
            CASE
                WHEN card = 'J'
                THEN (SELECT value FROM cards WHERE card=joker)
                ELSE value
            END ORDER BY position) AS replace_values,
        array_agg(
            CASE
                WHEN card = 'J'
                THEN joker
                ELSE card
            END ORDER BY position) AS cards
    FROM
        input
    CROSS JOIN
        unnest(cards, values) WITH ORDINALITY AS c(card, value, position)
    LEFT JOIN
        unnest(joker_cards) WITH ORDINALITY AS j(joker, replacement) ON (true)
    GROUP BY
        id,
        hand,
        bid,
        coalesce(replacement, 0)
), categories AS (
    SELECT
        id,
        hand,
        cards,
        permutation,
        values,
        replace_values,
        value,
        bid,
        count(*)
    FROM
        permutate
    CROSS JOIN
        unnest(replace_values) AS u(value)
    GROUP BY
        id,
        hand,
        cards,
        permutation,
        values,
        replace_values,
        value,
        bid
), rankings AS (
    SELECT
        id,
        hand,
        cards,
        permutation,
        values,
        replace_values,
        bid,
        array_agg(count ORDER BY count DESC, value DESC) AS rank_category,
        array_agg(value ORDER BY count DESC, value DESC) AS ranks
    FROM
        categories
    GROUP BY
        id,
        hand,
        cards,
        permutation,
        values,
        replace_values,
        bid
), ranked_permutations AS (
    SELECT
        id,
        hand,
        values,
        rank() OVER (ORDER BY rank_category ASC, values ASC),
        bid
    FROM
        rankings
), best_permutation AS (
    SELECT
        DISTINCT ON (id)
        id,
        hand,
        values,
        rank,
        bid
    FROM
        ranked_permutations
    ORDER BY
        id,
        rank DESC
)
SELECT
    sum(rank * bid)
FROM (
    SELECT
        id,
        hand,
        values,
        rank() OVER (ORDER BY rank),
        bid
    FROM
        best_permutation
    ) _;


EXECUTE problem07_2($$
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
$$);

\set input `cat 07.input`
EXECUTE problem07_2(:'input');

set track_io_timing to 'on';
\o 07-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_2(:'input');
\o
