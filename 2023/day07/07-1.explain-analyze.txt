                                                                                                                               QUERY PLAN                                                                                                                                
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Aggregate  (cost=22015.15..22015.16 rows=1 width=32) (actual time=22.156..22.159 rows=1 loops=1)
   ->  WindowAgg  (cost=22008.14..22012.64 rows=200 width=72) (actual time=21.589..22.115 rows=1000 loops=1)
         ->  Sort  (cost=22008.14..22008.64 rows=200 width=68) (actual time=21.584..21.619 rows=1000 loops=1)
               Sort Key: rankings.rank_category, rankings."values"
               Sort Method: quicksort  Memory: 161kB
               ->  Subquery Scan on rankings  (cost=317.12..22000.50 rows=200 width=68) (actual time=0.920..20.066 rows=1000 loops=1)
                     ->  GroupAggregate  (cost=317.12..21998.50 rows=200 width=172) (actual time=0.919..19.992 rows=1000 loops=1)
                           Group Key: categories.id, categories.hand, categories.cards, categories."values", categories.bid
                           ->  Incremental Sort  (cost=317.12..21966.00 rows=2000 width=120) (actual time=0.914..17.629 rows=3433 loops=1)
                                 Sort Key: categories.id, categories.hand, categories.cards, categories."values", categories.bid
                                 Presorted Key: categories.id, categories.hand, categories.cards, categories."values"
                                 Full-sort Groups: 103  Sort Method: quicksort  Average Memory: 30kB  Peak Memory: 30kB
                                 ->  Subquery Scan on categories  (cost=208.49..21875.90 rows=2000 width=120) (actual time=0.755..16.011 rows=3433 loops=1)
                                       ->  GroupAggregate  (cost=208.49..21855.90 rows=2000 width=120) (actual time=0.755..15.759 rows=3433 loops=1)
                                             Group Key: _.id, (split_part(_.line, ' '::text, 1)), (array_agg(rsta.card ORDER BY rsta."position")), (array_agg("*VALUES*".column2 ORDER BY rsta."position")), u.value, ((split_part(_.line, ' '::text, 2))::integer)
                                             ->  Incremental Sort  (cost=208.49..21800.90 rows=2000 width=112) (actual time=0.750..13.977 rows=5000 loops=1)
                                                   Sort Key: _.id, (split_part(_.line, ' '::text, 1)), (array_agg(rsta.card ORDER BY rsta."position")), (array_agg("*VALUES*".column2 ORDER BY rsta."position")), u.value, ((split_part(_.line, ' '::text, 2))::integer)
                                                   Presorted Key: _.id, (split_part(_.line, ' '::text, 1))
                                                   Full-sort Groups: 143  Sort Method: quicksort  Average Memory: 30kB  Peak Memory: 30kB
                                                   ->  Nested Loop  (cost=100.14..21710.79 rows=2000 width=112) (actual time=0.635..9.749 rows=5000 loops=1)
                                                         ->  GroupAggregate  (cost=100.14..21668.79 rows=200 width=108) (actual time=0.632..8.331 rows=1000 loops=1)
                                                               Group Key: _.id, (split_part(_.line, ' '::text, 1)), ((split_part(_.line, ' '::text, 2))::integer)
                                                               ->  Incremental Sort  (cost=100.14..20855.35 rows=64675 width=88) (actual time=0.623..5.147 rows=5000 loops=1)
                                                                     Sort Key: _.id, (split_part(_.line, ' '::text, 1)), ((split_part(_.line, ' '::text, 2))::integer)
                                                                     Presorted Key: _.id
                                                                     Full-sort Groups: 143  Sort Method: quicksort  Average Memory: 27kB  Peak Memory: 27kB
                                                                     ->  Nested Loop  (cost=0.33..15634.33 rows=64675 width=88) (actual time=0.582..4.121 rows=5000 loops=1)
                                                                           ->  Function Scan on regexp_split_to_table _  (cost=0.00..12.50 rows=995 width=40) (actual time=0.567..0.650 rows=1000 loops=1)
                                                                                 Filter: (line <> ''::text)
                                                                           ->  Hash Join  (cost=0.33..14.73 rows=65 width=44) (actual time=0.001..0.002 rows=5 loops=1000)
                                                                                 Hash Cond: (rsta.card = "*VALUES*".column1)
                                                                                 ->  Function Scan on regexp_split_to_table rsta  (cost=0.01..10.01 rows=1000 width=40) (actual time=0.001..0.002 rows=5 loops=1000)
                                                                                 ->  Hash  (cost=0.16..0.16 rows=13 width=36) (actual time=0.006..0.007 rows=13 loops=1)
                                                                                       Buckets: 1024  Batches: 1  Memory Usage: 9kB
                                                                                       ->  Values Scan on "*VALUES*"  (cost=0.00..0.16 rows=13 width=36) (actual time=0.001..0.003 rows=13 loops=1)
                                                         ->  Function Scan on unnest u  (cost=0.00..0.10 rows=10 width=4) (actual time=0.001..0.001 rows=5 loops=1000)
 Planning Time: 0.335 ms
 Execution Time: 22.208 ms
(38 rows)

