DEALLOCATE problem15_1;

PREPARE problem15_1 AS
WITH input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    *
FROM
    input
;

EXECUTE problem15_1($$
dummy line
$$);

\set input `cat 15.input`
EXECUTE problem15_1(:'input');

set track_io_timing to 'on';
\o 15-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem15_1(:'input');
\o
