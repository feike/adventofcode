DEALLOCATE problem23_1;

PREPARE problem23_1 AS
WITH input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    *
FROM
    input
;

EXECUTE problem23_1($$
dummy line
$$);

\set input `cat 23.input`
EXECUTE problem23_1(:'input');

set track_io_timing to 'on';
\o 23-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem23_1(:'input');
\o
