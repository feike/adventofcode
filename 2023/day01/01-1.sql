DEALLOCATE problem01_1;

PREPARE problem01_1 AS
WITH input(line, y) AS (
    SELECT
        nullif(line, ''),
        y
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
), test AS (
    SELECT
        y,
        x,
        min(x) OVER (PARTITION BY y) AS frst,
        max(x) OVER (PARTITION BY y) AS lst,
        chr::text
    FROM
        input
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS _(chr, x)
    WHERE
        chr ~ '\d'
)
SELECT
    sum((f.chr||l.chr)::int)
FROM
    test AS f
JOIN
    test as l USING (y)
WHERE
    f.x = f.frst
    AND l.x = f.lst
;

EXECUTE problem01_1($$1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet$$);


\set input `cat 01.input`
EXECUTE problem01_1(:'input');

set track_io_timing to 'on';
\o 01-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_1(:'input');
\o

