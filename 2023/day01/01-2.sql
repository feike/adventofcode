DEALLOCATE problem01_2;

PREPARE problem01_2 AS
WITH replace (chrs, digit) AS (
    VALUES
        ('one', '1'),
        ('two', '2'),
        ('three', '3'),
        ('four', '4'),
        ('five', '5'),
        ('six', '6'),
        ('seven', '7'),
        ('eight', '8'),
        ('nine', '9')
), input(y, lft, rgt) AS (
    SELECT
        y,
        coalesce(replace.digit, rl.entry[1]),
        coalesce(r2.digit, rr.entry[1])
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_matches(line, '.*?(one|two|three|four|five|six|seven|eight|nine|\d)') AS rl(entry)
    CROSS JOIN
        regexp_matches(line, '.*(one|two|three|four|five|six|seven|eight|nine|\d)') AS rr(entry)
    LEFT JOIN
        replace ON (rl.entry[1]=replace.chrs)
    LEFT JOIN
        replace AS r2 ON (rr.entry[1]=r2.chrs)
)
SELECT
    sum((lft||rgt)::int)
FROM
    input;

EXECUTE problem01_2($$two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen$$);

\set input `cat 01.input`
EXECUTE problem01_2(:'input');

set track_io_timing to 'on';
\o 01-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_2(:'input');
\o
