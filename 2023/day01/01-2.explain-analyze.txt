                                                                         QUERY PLAN                                                                         
------------------------------------------------------------------------------------------------------------------------------------------------------------
 Aggregate  (cost=88.20..88.22 rows=1 width=8) (actual time=10.088..10.092 rows=1 loops=1)
   CTE replace
     ->  Values Scan on "*VALUES*"  (cost=0.00..0.11 rows=9 width=64) (actual time=0.001..0.003 rows=9 loops=1)
   ->  Hash Left Join  (cost=0.59..78.09 rows=1000 width=128) (actual time=0.465..9.868 rows=1000 loops=1)
         Hash Cond: (rr.entry[1] = r2.chrs)
         ->  Hash Left Join  (cost=0.30..64.05 rows=1000 width=96) (actual time=0.462..9.585 rows=1000 loops=1)
               Hash Cond: (rl.entry[1] = replace.chrs)
               ->  Nested Loop  (cost=0.01..50.01 rows=1000 width=64) (actual time=0.452..9.266 rows=1000 loops=1)
                     ->  Nested Loop  (cost=0.01..30.00 rows=1000 width=64) (actual time=0.445..4.345 rows=1000 loops=1)
                           ->  Function Scan on regexp_split_to_table _  (cost=0.00..10.00 rows=1000 width=32) (actual time=0.437..0.514 rows=1000 loops=1)
                           ->  Function Scan on regexp_matches rl  (cost=0.00..0.01 rows=1 width=32) (actual time=0.003..0.004 rows=1 loops=1000)
                     ->  Function Scan on regexp_matches rr  (cost=0.00..0.01 rows=1 width=32) (actual time=0.005..0.005 rows=1 loops=1000)
               ->  Hash  (cost=0.18..0.18 rows=9 width=64) (actual time=0.006..0.007 rows=9 loops=1)
                     Buckets: 1024  Batches: 1  Memory Usage: 9kB
                     ->  CTE Scan on replace  (cost=0.00..0.18 rows=9 width=64) (actual time=0.002..0.005 rows=9 loops=1)
         ->  Hash  (cost=0.18..0.18 rows=9 width=64) (actual time=0.002..0.003 rows=9 loops=1)
               Buckets: 1024  Batches: 1  Memory Usage: 9kB
               ->  CTE Scan on replace r2  (cost=0.00..0.18 rows=9 width=64) (actual time=0.000..0.001 rows=9 loops=1)
 Planning Time: 0.148 ms
 Execution Time: 10.126 ms
(20 rows)

