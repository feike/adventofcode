DEALLOCATE problem11_2;

PREPARE problem11_2 AS
WITH input(x, y, g) AS (
    SELECT
        (x - min(x) OVER ()) AS x,
        (y - min(y) OVER ()) AS y,
        g
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt(g, x)
    WHERE
        line !~ '^\s*$'
), expanded_x AS (
    SELECT
        x,
        x + sum(CASE
            WHEN bool_and(g='.')
            THEN 1000000 - 1
            ELSE 0
        END) OVER (ORDER BY x) AS x_new
    FROM
        input
    GROUP BY
        x
), expanded_y AS (
    SELECT
        y,
        y + sum(CASE
            WHEN bool_and(g='.')
            THEN 1000000 - 1
            ELSE 0
        END) OVER (ORDER BY y) AS y_new
    FROM
        input
    GROUP BY
        y
), stars(x, y) AS (
    SELECT
        x_new,
        y_new
    FROM
        input
    JOIN
        expanded_x USING (x)
    JOIN
        expanded_y USING (y)
    WHERE
        g = '#'
), distance AS (
    SELECT
        abs(b.x - a.x) + abs(b.y - a.y) AS distance
    FROM
        stars a
    JOIN
        stars b ON (NOT(a.x=b.x AND a.y=b.y))
)
SELECT
    (sum(distance)/2)::bigint
FROM
    distance;

EXECUTE problem11_2($$
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
$$);

\set input `cat 11.input`
EXECUTE problem11_2(:'input');

set track_io_timing to 'on';
\o 11-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem11_2(:'input');
\o
