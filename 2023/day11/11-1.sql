DEALLOCATE problem11_1;

PREPARE problem11_1 AS
WITH input(x, y, g) AS (
    SELECT
        -- * 2 allows for expansion in between
        (x - min(x) OVER ())*2 AS x,
        (y - min(y) OVER ())*2 AS y,
        g
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt(g, x)
    WHERE
        line !~ '^\s*$'
), empty_cols AS (
    SELECT
        x,
        y,
        g
    FROM (
        SELECT
            x+1 AS x,
            array_agg(y) AS ys,
            array_agg(g) AS gs
        FROM
            input
        GROUP BY
            x
        HAVING
            bool_and(g='.')
    ) _
    CROSS JOIN
        unnest(ys, gs) AS u(y, g)
    
    UNION ALL

    SELECT
        *
    FROM
        input
), expansion AS MATERIALIZED (
    SELECT
        dense_rank() OVER (ORDER BY x) - 1 AS x,
        dense_rank() OVER (ORDER BY y) - 1 AS y,
        g
    FROM (
        SELECT
            x,
            y,
            g
        FROM (
            SELECT
                y+1 AS y,
                array_agg(x) AS xs,
                array_agg(g) AS gs
            FROM
                empty_cols
            GROUP BY
                y
            HAVING
                bool_and(g='.')
        ) _
        CROSS JOIN
            unnest(xs, gs) AS u(x, g)
        
        UNION ALL

        SELECT
            *
        FROM
            empty_cols
    ) _
), paint AS (
    SELECT
        y,
        string_agg(g, '' ORDER BY x)
    FROM
        expansion
    GROUP BY
        y
    ORDER BY
        y
), stars AS MATERIALIZED (
    SELECT
        x,
        y
    FROM
        expansion
    WHERE
        g = '#'
), distance AS (
    SELECT
        abs(b.x - a.x) + abs(b.y - a.y) AS distance
    FROM
        stars a
    JOIN
        stars b ON (NOT(a.x=b.x AND a.y=b.y))
)
SELECT
    sum(distance)/2
FROM
    distance;

EXECUTE problem11_1($$
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
$$);

\set input `cat 11.input`
EXECUTE problem11_1(:'input');

set track_io_timing to 'on';
\o 11-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem11_1(:'input');
\o
