                                                                              QUERY PLAN                                                                               
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Aggregate  (cost=954703.52..954703.54 rows=1 width=8) (actual time=54.035..54.039 rows=1 loops=1)
   CTE input
     ->  WindowAgg  (cost=0.01..36812.51 rows=920000 width=48) (actual time=7.718..9.690 rows=19600 loops=1)
           ->  Nested Loop  (cost=0.01..18412.51 rows=920000 width=48) (actual time=0.154..4.621 rows=19600 loops=1)
                 ->  Function Scan on regexp_split_to_table _  (cost=0.00..12.50 rows=920 width=40) (actual time=0.136..0.184 rows=140 loops=1)
                       Filter: (line !~ '^\s*$'::text)
                 ->  Function Scan on regexp_split_to_table rstt  (cost=0.00..10.00 rows=1000 width=40) (actual time=0.016..0.023 rows=140 loops=140)
   CTE stars
     ->  Hash Join  (cost=50636.29..71360.95 rows=4600 width=16) (actual time=18.757..19.880 rows=436 loops=1)
           Hash Cond: (input.y = expanded_y.y)
           ->  Hash Join  (cost=25318.14..46030.47 rows=4600 width=16) (actual time=3.459..4.526 rows=436 loops=1)
                 Hash Cond: (input.x = expanded_x.x)
                 ->  CTE Scan on input  (cost=0.00..20700.00 rows=4600 width=16) (actual time=0.002..1.011 rows=436 loops=1)
                       Filter: (g = '#'::text)
                       Rows Removed by Filter: 19164
                 ->  Hash  (cost=25315.64..25315.64 rows=200 width=16) (actual time=3.454..3.455 rows=140 loops=1)
                       Buckets: 1024  Batches: 1  Memory Usage: 15kB
                       ->  Subquery Scan on expanded_x  (cost=25309.64..25315.64 rows=200 width=16) (actual time=3.387..3.442 rows=140 loops=1)
                             ->  WindowAgg  (cost=25309.64..25313.64 rows=200 width=16) (actual time=3.387..3.433 rows=140 loops=1)
                                   ->  Sort  (cost=25309.64..25310.14 rows=200 width=9) (actual time=3.384..3.388 rows=140 loops=1)
                                         Sort Key: input_1.x
                                         Sort Method: quicksort  Memory: 32kB
                                         ->  HashAggregate  (cost=25300.00..25302.00 rows=200 width=9) (actual time=3.358..3.369 rows=140 loops=1)
                                               Group Key: input_1.x
                                               Batches: 1  Memory Usage: 48kB
                                               ->  CTE Scan on input input_1  (cost=0.00..18400.00 rows=920000 width=40) (actual time=0.000..0.852 rows=19600 loops=1)
           ->  Hash  (cost=25315.64..25315.64 rows=200 width=16) (actual time=15.296..15.297 rows=140 loops=1)
                 Buckets: 1024  Batches: 1  Memory Usage: 15kB
                 ->  Subquery Scan on expanded_y  (cost=25309.64..25315.64 rows=200 width=16) (actual time=15.227..15.282 rows=140 loops=1)
                       ->  WindowAgg  (cost=25309.64..25313.64 rows=200 width=16) (actual time=15.226..15.272 rows=140 loops=1)
                             ->  Sort  (cost=25309.64..25310.14 rows=200 width=9) (actual time=15.222..15.227 rows=140 loops=1)
                                   Sort Key: input_2.y
                                   Sort Method: quicksort  Memory: 32kB
                                   ->  HashAggregate  (cost=25300.00..25302.00 rows=200 width=9) (actual time=15.194..15.205 rows=140 loops=1)
                                         Group Key: input_2.y
                                         Batches: 1  Memory Usage: 48kB
                                         ->  CTE Scan on input input_2  (cost=0.00..18400.00 rows=920000 width=40) (actual time=7.719..12.614 rows=19600 loops=1)
   ->  Nested Loop  (cost=0.00..529138.00 rows=21159471 width=32) (actual time=18.761..43.509 rows=189660 loops=1)
         Join Filter: ((a.x <> b.x) OR (a.y <> b.y))
         Rows Removed by Join Filter: 436
         ->  CTE Scan on stars a  (cost=0.00..92.00 rows=4600 width=16) (actual time=18.757..18.778 rows=436 loops=1)
         ->  CTE Scan on stars b  (cost=0.00..92.00 rows=4600 width=16) (actual time=0.000..0.021 rows=436 loops=436)
 Planning Time: 0.221 ms
 Execution Time: 54.190 ms
(44 rows)

