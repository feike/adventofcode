DEALLOCATE problem10_2;

PREPARE problem10_2 AS
WITH RECURSIVE niceunicode(o, r) AS (
    VALUES
        ('S', 'S'),
        ('-', '─'),
        ('|', '│'),
        ('F', '╭'),
        ('7', '╮'), 
        ('L', '╰'),
        ('J', '╯')
), input AS MATERIALIZED (
    WITH base AS (
        SELECT
            y::int - min(y) OVER () AS y,
            x::int - min(x) OVER () AS x,
            coalesce(r, ' ') AS pipe
        FROM
            regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
        CROSS JOIN
            regexp_split_to_table(line, '') WITH ORDINALITY AS rstt(pipe, x)
        LEFT JOIN
            niceunicode ON (pipe=o)
        WHERE
            line !~ '^\s*$'
    ), grid AS (
        SELECT
            y,
            x
        FROM (
            SELECT
                min(x) AS min_x,
                max(x) AS max_x,
                min(y) AS min_y,
                max(y) AS max_y
            FROM
                base
        ) _
        CROSS JOIN
            generate_series(min_x - 1, max_x + 1) AS s(x)
        CROSS JOIN
            generate_series(min_y - 1, max_y + 1) AS s2(y)
    )
    SELECT
        x::int,
        y::int,
        coalesce(pipe, '.') AS pipe
    FROM
        grid
    LEFT JOIN
        base USING (x, y)
), directions (pipe, fitting, delta_x, delta_y) AS (
    VALUES
    ('─', '─', -1,  0),
    ('─', '╰', -1,  0),
    ('─', '╭', -1,  0),
    ('─', '─',  1,  0),
    ('─', '╯',  1,  0),
    ('─', '╮',  1,  0),
    ('│', '│',  0, -1),
    ('│', '╮',  0, -1),
    ('│', '╭',  0, -1),
    ('│', '│',  0,  1),
    ('│', '╰',  0,  1),
    ('│', '╯',  0,  1),
    ('╰', '│',  0, -1),
    ('╰', '╮',  0, -1),
    ('╰', '╭',  0, -1),
    ('╰', '─',  1,  0),
    ('╰', '╯',  1,  0),
    ('╰', '╮',  1,  0),
    ('╯', '│',  0, -1),
    ('╯', '╮',  0, -1),
    ('╯', '╭',  0, -1),
    ('╯', '─', -1,  0),
    ('╯', '╰', -1,  0),
    ('╯', '╭', -1,  0),
    ('╮', '│',  0,  1),
    ('╮', '╰',  0,  1),
    ('╮', '╯',  0,  1),
    ('╮', '─', -1,  0),
    ('╮', '╰', -1,  0),
    ('╮', '╭', -1,  0),
    ('╭', '│',  0,  1),
    ('╭', '╰',  0,  1),
    ('╭', '╯',  0,  1),
    ('╭', '─',  1,  0),
    ('╭', '╯',  1,  0),
    ('╭', '╮',  1,  0)
), start_square AS (
    WITH start AS (
        SELECT
            x,
            y
        FROM
            input
        WHERE
            pipe = 'S'
    )
    SELECT
        i.x,
        i.y,
        i.pipe
    FROM
        start AS s
    JOIN
        input AS i ON (i.x BETWEEN s.x-1 AND s.x+1 AND i.y BETWEEN s.y-1 AND s.y+1)       
), start AS MATERIALIZED (
    SELECT
        s.x,
        s.y,
        p.x AS prev_x,
        p.y AS prev_y,
        pd.pipe AS prev_pipe,
        pd.fitting AS pipe
    FROM
        start_square AS s
    CROSS JOIN
        directions AS pd
    JOIN
        start_square AS p ON (p.pipe=pd.pipe AND p.x+delta_x=s.x AND p.y+delta_y=s.y)
    JOIN
        directions AS nd ON (pd.fitting=nd.pipe)
    JOIN
        start_square AS n ON (n.pipe=nd.fitting AND s.x+nd.delta_x=n.x AND s.y+nd.delta_y=n.y)
    WHERE
        s.pipe = 'S'
        AND NOT (n.x=p.x AND n.y=p.y)
    LIMIT
        1        
)
-- We create a smaller representation, all that is needed to run the recursion
-- to its end
, other_pipes AS MATERIALIZED (
    SELECT
        x,
        y,
        pipe
    FROM
        input
    WHERE
        pipe NOT IN ('.', 'S')
), connected AS (
    SELECT
        *,
        1::int AS distance
    FROM
        start

    UNION ALL

    SELECT
        i.x,
        i.y,
        c.x,
        c.y,
        c.pipe,
        i.pipe,
        distance + 1
    FROM
        connected AS c
    JOIN
        directions AS d USING (pipe)
    JOIN
        other_pipes AS i ON (c.x+delta_x=i.x AND c.y+delta_y=i.y AND i.pipe=d.fitting)
    WHERE
        NOT (i.x=c.prev_x AND i.y=prev_y)
), inside AS (
    SELECT
        y,
        x
    FROM (
        SELECT
            y,
            x,
            sum(CASE
                WHEN c.pipe = ANY(ARRAY['│', '╭', '╮'])
                THEN 1
                ELSE 0
            END) OVER (PARTITION BY y ORDER BY x) % 2 = 1 AS is_odd
        FROM
            input
        LEFT JOIN
            connected AS c USING (x, y)
    ) _ 
    LEFT JOIN
        connected USING (x, y)
    WHERE
        is_odd
        AND connected IS NULL
), paint AS (
    SELECT
        y,
        string_agg(
            CASE
                WHEN i IS NOT NULL
                THEN 'i'
                WHEN c IS NOT NULL
                THEN c.pipe
                ELSE ' '
            END
            , '' ORDER BY x
        ) AS picture
    FROM
        input
    LEFT JOIN
        connected AS c USING (x, y)
    LEFT JOIN
        inside AS i USING (x, y)
    GROUP BY
        y
    ORDER BY
        y
)
SELECT
    picture
FROM
    paint

UNION ALL

SELECT
    format('Number of tiles inside: %s', count(*))
FROM
    inside;

\timing on

EXECUTE problem10_2($$
...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........
$$);

EXECUTE problem10_2($$
..........
.S------7.
.|F----7|.
.||OOOO||.
.||OOOO||.
.|L-7F-J|.
.|II||II|.
.L--JL--J.
..........
$$);

EXECUTE problem10_2($$
.F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...
$$);

EXECUTE problem10_2($$
FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L
$$);

\set input `cat 10.input`
EXECUTE problem10_2(:'input');

set track_io_timing to 'on';
\o 10-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem10_2(:'input');
\o
