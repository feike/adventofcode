                                                                             QUERY PLAN                                                                             
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Append  (cost=105816014281.03..105817049814.12 rows=201 width=32) (actual time=278114.177..278126.093 rows=143 loops=1)
   CTE input
     ->  Merge Right Join  (cost=294524.41..818924.41 rows=23000000 width=40) (actual time=30.829..37.057 rows=20164 loops=1)
           Merge Cond: ((base.x = s.x) AND (base.y = s2.y))
           CTE base
             ->  WindowAgg  (cost=0.18..45184.68 rows=920000 width=48) (actual time=11.667..13.835 rows=19600 loops=1)
                   ->  Hash Left Join  (cost=0.18..22184.68 rows=920000 width=48) (actual time=0.233..7.800 rows=19600 loops=1)
                         Hash Cond: (rstt.pipe = "*VALUES*".column1)
                         ->  Nested Loop  (cost=0.01..18412.51 rows=920000 width=48) (actual time=0.226..4.891 rows=19600 loops=1)
                               ->  Function Scan on regexp_split_to_table _  (cost=0.00..12.50 rows=920 width=40) (actual time=0.204..0.263 rows=140 loops=1)
                                     Filter: (line !~ '^\s*$'::text)
                               ->  Function Scan on regexp_split_to_table rstt  (cost=0.00..10.00 rows=1000 width=40) (actual time=0.017..0.024 rows=140 loops=140)
                         ->  Hash  (cost=0.09..0.09 rows=7 width=64) (actual time=0.004..0.005 rows=7 loops=1)
                               Buckets: 1024  Batches: 1  Memory Usage: 9kB
                               ->  Values Scan on "*VALUES*"  (cost=0.00..0.09 rows=7 width=64) (actual time=0.001..0.002 rows=7 loops=1)
           ->  Sort  (cost=109531.86..111831.86 rows=920000 width=48) (actual time=24.912..25.623 rows=19600 loops=1)
                 Sort Key: base.x, base.y
                 Sort Method: quicksort  Memory: 1994kB
                 ->  CTE Scan on base  (cost=0.00..18400.00 rows=920000 width=48) (actual time=11.668..17.228 rows=19600 loops=1)
           ->  Sort  (cost=139807.87..142307.87 rows=1000000 width=16) (actual time=5.914..6.534 rows=20164 loops=1)
                 Sort Key: s.x, s2.y
                 Sort Method: quicksort  Memory: 1871kB
                 ->  Nested Loop  (cost=27600.03..40150.03 rows=1000000 width=16) (actual time=2.232..3.965 rows=20164 loops=1)
                       ->  Nested Loop  (cost=27600.01..27620.03 rows=1000 width=24) (actual time=2.216..2.237 rows=142 loops=1)
                             ->  Aggregate  (cost=27600.00..27600.01 rows=1 width=32) (actual time=2.204..2.205 rows=1 loops=1)
                                   ->  CTE Scan on base base_1  (cost=0.00..18400.00 rows=920000 width=16) (actual time=0.001..0.883 rows=19600 loops=1)
                             ->  Function Scan on generate_series s  (cost=0.01..10.01 rows=1000 width=8) (actual time=0.009..0.019 rows=142 loops=1)
                       ->  Memoize  (cost=0.02..10.02 rows=1000 width=8) (actual time=0.000..0.003 rows=142 loops=142)
                             Cache Key: (min(base_1.y)), (max(base_1.y))
                             Cache Mode: binary
                             Hits: 141  Misses: 1  Evictions: 0  Overflows: 0  Memory Usage: 6kB
                             ->  Function Scan on generate_series s2  (cost=0.01..10.01 rows=1000 width=8) (actual time=0.012..0.017 rows=142 loops=1)
   CTE directions
     ->  Values Scan on "*VALUES*_1"  (cost=0.00..0.45 rows=36 width=72) (actual time=0.002..0.011 rows=36 loops=1)
   CTE start_square
     ->  Nested Loop  (cost=0.00..105800747500.00 rows=32654320988 width=40) (actual time=2.381..2.842 rows=9 loops=1)
           Join Filter: ((i_1.x >= (input_1.x - 1)) AND (i_1.x <= (input_1.x + 1)) AND (i_1.y >= (input_1.y - 1)) AND (i_1.y <= (input_1.y + 1)))
           Rows Removed by Join Filter: 20155
           ->  CTE Scan on input input_1  (cost=0.00..517500.00 rows=115000 width=8) (actual time=0.798..0.937 rows=1 loops=1)
                 Filter: (pipe = 'S'::text)
                 Rows Removed by Filter: 20163
           ->  CTE Scan on input i_1  (cost=0.00..460000.00 rows=23000000 width=40) (actual time=0.000..0.861 rows=20164 loops=1)
   CTE start
     ->  Limit  (cost=1.17..55859.11 rows=1 width=80) (actual time=2.912..2.916 rows=1 loops=1)
           ->  Nested Loop  (cost=1.17..5470015617675620777984.00 rows=97927269726385520 width=80) (actual time=2.912..2.915 rows=1 loops=1)
                 Join Filter: ((nd.pipe = pd.fitting) AND ((n.x <> p.x) OR (n.y <> p.y)) AND ((s_1.x + nd.delta_x) = n.x) AND ((s_1.y + nd.delta_y) = n.y))
                 Rows Removed by Join Filter: 67
                 ->  Nested Loop  (cost=0.00..28790254185597736.00 rows=23991855290532 width=80) (actual time=2.877..2.880 rows=2 loops=1)
                       Join Filter: (((p.x + pd.delta_x) = s_1.x) AND ((p.y + pd.delta_y) = s_1.y))
                       Rows Removed by Join Filter: 9
                       ->  Nested Loop  (cost=0.00..27103086420.40 rows=5877777778 width=80) (actual time=2.386..2.407 rows=11 loops=1)
                             Join Filter: (pd.pipe = p.pipe)
                             Rows Removed by Join Filter: 30
                             ->  CTE Scan on start_square p  (cost=0.00..653086419.76 rows=32654320988 width=40) (actual time=2.382..2.382 rows=2 loops=1)
                             ->  CTE Scan on directions pd  (cost=0.00..0.72 rows=36 width=72) (actual time=0.001..0.010 rows=20 loops=2)
                       ->  CTE Scan on start_square s_1  (cost=0.00..734722222.23 rows=163271605 width=8) (actual time=0.002..0.043 rows=1 loops=11)
                             Filter: (pipe = 'S'::text)
                             Rows Removed by Filter: 8
                 ->  Materialize  (cost=1.17..915366948.30 rows=5877777778 width=48) (actual time=0.006..0.014 rows=34 loops=2)
                       ->  Hash Join  (cost=1.17..834317902.41 rows=5877777778 width=48) (actual time=0.010..0.017 rows=48 loops=1)
                             Hash Cond: (n.pipe = nd.fitting)
                             ->  CTE Scan on start_square n  (cost=0.00..653086419.76 rows=32654320988 width=40) (actual time=0.000..0.001 rows=9 loops=1)
                             ->  Hash  (cost=0.72..0.72 rows=36 width=72) (actual time=0.006..0.006 rows=36 loops=1)
                                   Buckets: 1024  Batches: 1  Memory Usage: 10kB
                                   ->  CTE Scan on directions nd  (cost=0.00..0.72 rows=36 width=72) (actual time=0.000..0.002 rows=36 loops=1)
   CTE other_pipes
     ->  CTE Scan on input input_2  (cost=0.00..517500.00 rows=22770000 width=40) (actual time=0.010..1.927 rows=19599 loops=1)
           Filter: (pipe <> ALL ('{.,S}'::text[]))
           Rows Removed by Filter: 565
   CTE connected
     ->  Recursive Union  (cost=0.00..6256078.79 rows=281 width=84) (actual time=2.915..278003.224 rows=13854 loops=1)
           ->  CTE Scan on start  (cost=0.00..0.02 rows=1 width=84) (actual time=2.914..2.914 rows=1 loops=1)
           ->  Hash Join  (cost=1.49..625607.31 rows=28 width=84) (actual time=10.177..20.064 rows=1 loops=13854)
                 Hash Cond: (d.pipe = c_1.pipe)
                 Join Filter: (((i_2.x <> c_1.prev_x) OR (i_2.y <> c_1.prev_y)) AND ((c_1.x + d.delta_x) = i_2.x) AND ((c_1.y + d.delta_y) = i_2.y))
                 Rows Removed by Join Filter: 19061
                 ->  Hash Join  (cost=1.17..581774.67 rows=4098600 width=80) (actual time=0.000..12.299 rows=114372 loops=13854)
                       Hash Cond: (i_2.pipe = d.fitting)
                       ->  CTE Scan on other_pipes i_2  (cost=0.00..455400.00 rows=22770000 width=40) (actual time=0.000..0.890 rows=19599 loops=13854)
                       ->  Hash  (cost=0.72..0.72 rows=36 width=72) (actual time=0.007..0.008 rows=36 loops=1)
                             Buckets: 1024  Batches: 1  Memory Usage: 10kB
                             ->  CTE Scan on directions d  (cost=0.00..0.72 rows=36 width=72) (actual time=0.000..0.003 rows=36 loops=1)
                 ->  Hash  (cost=0.20..0.20 rows=10 width=52) (actual time=0.001..0.001 rows=1 loops=13854)
                       Buckets: 1024  Batches: 1  Memory Usage: 9kB
                       ->  WorkTable Scan on connected c_1  (cost=0.00..0.20 rows=10 width=52) (actual time=0.000..0.000 rows=1 loops=13854)
   CTE inside
     ->  Merge Left Join  (cost=3272374.11..4340365.46 rows=57500 width=8) (actual time=20.987..33.462 rows=467 loops=1)
           Merge Cond: ((__1.y = connected.y) AND (__1.x = connected.x))
           Filter: (connected.* IS NULL)
           Rows Removed by Filter: 6926
           ->  Subquery Scan on __1  (cost=3272357.06..4281434.62 rows=11500000 width=8) (actual time=11.289..25.259 rows=7393 loops=1)
                 Filter: __1.is_odd
                 Rows Removed by Filter: 12771
                 ->  WindowAgg  (cost=3272357.06..4051434.62 rows=23000000 width=9) (actual time=11.156..24.285 rows=20164 loops=1)
                       ->  Merge Left Join  (cost=3272357.06..3447684.62 rows=23000000 width=40) (actual time=11.149..16.281 rows=20164 loops=1)
                             Merge Cond: ((input_3.y = c_2.y) AND (input_3.x = c_2.x))
                             ->  Sort  (cost=3272340.01..3329840.01 rows=23000000 width=8) (actual time=5.696..6.399 rows=20164 loops=1)
                                   Sort Key: input_3.y, input_3.x
                                   Sort Method: quicksort  Memory: 1714kB
                                   ->  CTE Scan on input input_3  (cost=0.00..460000.00 rows=23000000 width=8) (actual time=0.001..1.294 rows=20164 loops=1)
                             ->  Sort  (cost=17.05..17.75 rows=281 width=40) (actual time=5.451..5.972 rows=13854 loops=1)
                                   Sort Key: c_2.y, c_2.x
                                   Sort Method: quicksort  Memory: 1142kB
                                   ->  CTE Scan on connected c_2  (cost=0.00..5.62 rows=281 width=40) (actual time=0.001..1.048 rows=13854 loops=1)
           ->  Sort  (cost=17.05..17.75 rows=281 width=40) (actual time=5.637..6.096 rows=13852 loops=1)
                 Sort Key: connected.y, connected.x
                 Sort Method: quicksort  Memory: 1792kB
                 ->  CTE Scan on connected  (cost=0.00..5.62 rows=281 width=40) (actual time=0.006..2.482 rows=13854 loops=1)
   ->  Subquery Scan on paint  (cost=3278052.80..4312291.11 rows=200 width=32) (actual time=278114.176..278126.024 rows=142 loops=1)
         ->  GroupAggregate  (cost=3278052.80..4312289.11 rows=200 width=36) (actual time=278114.176..278126.013 rows=142 loops=1)
               Group Key: input.y
               ->  Merge Left Join  (cost=3278052.80..4146974.11 rows=33062500 width=136) (actual time=278114.093..278121.301 rows=20164 loops=1)
                     Merge Cond: ((input.y = i.y) AND (input.x = i.x))
                     ->  Merge Left Join  (cost=3272357.06..3447684.62 rows=23000000 width=104) (actual time=278080.461..278086.020 rows=20164 loops=1)
                           Merge Cond: ((input.y = c.y) AND (input.x = c.x))
                           ->  Sort  (cost=3272340.01..3329840.01 rows=23000000 width=8) (actual time=45.239..46.020 rows=20164 loops=1)
                                 Sort Key: input.y, input.x
                                 Sort Method: quicksort  Memory: 1714kB
                                 ->  CTE Scan on input  (cost=0.00..460000.00 rows=23000000 width=8) (actual time=30.830..40.659 rows=20164 loops=1)
                           ->  Sort  (cost=17.05..17.75 rows=281 width=104) (actual time=278035.219..278035.915 rows=13854 loops=1)
                                 Sort Key: c.y, c.x
                                 Sort Method: quicksort  Memory: 1792kB
                                 ->  CTE Scan on connected c  (cost=0.00..5.62 rows=281 width=104) (actual time=2.919..278020.784 rows=13854 loops=1)
                     ->  Sort  (cost=5695.74..5839.49 rows=57500 width=40) (actual time=33.630..33.644 rows=467 loops=1)
                           Sort Key: i.y, i.x
                           Sort Method: quicksort  Memory: 61kB
                           ->  CTE Scan on inside i  (cost=0.00..1150.00 rows=57500 width=40) (actual time=20.990..33.568 rows=467 loops=1)
   ->  Aggregate  (cost=1293.75..1293.76 rows=1 width=32) (actual time=0.044..0.044 rows=1 loops=1)
         ->  CTE Scan on inside  (cost=0.00..1150.00 rows=57500 width=0) (actual time=0.000..0.020 rows=467 loops=1)
 Planning Time: 0.961 ms
 Execution Time: 278126.730 ms
(131 rows)

