DEALLOCATE problem10_1;

PREPARE problem10_1 AS
WITH RECURSIVE input AS MATERIALIZED (
    SELECT
        y::int - min(y) OVER () AS y,
        x::int - min(x) OVER () AS x,
        pipe
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt(pipe, x)
    WHERE
        line !~ '^\s*$'
), directions (pipe, delta_y, delta_x, fittings) AS (
    VALUES
    ('-',  0, -1, ARRAY['-', 'L', 'F']),
    ('-',  0, +1, ARRAY['-', 'J', '7']),
    ('|', -1,  0, ARRAY['|', '7', 'F']),
    ('|', +1,  0, ARRAY['|', 'L', 'J']),
    ('L', -1,  0, ARRAY['|', '7', 'F']),
    ('L',  0, +1, ARRAY['-', 'J', '7']),
    ('J', -1,  0, ARRAY['|', '7', 'F']),
    ('J',  0, -1, ARRAY['-', 'L', 'F']),
    ('7', +1,  0, ARRAY['|', 'L', 'J']),
    ('7',  0, -1, ARRAY['-', 'L', 'F']),
    ('F', +1,  0, ARRAY['|', 'L', 'J']),
    ('F',  0, +1, ARRAY['-', 'J', '7'])
), connected AS (
    SELECT * FROM (
        SELECT
            n.x,
            n.y,
            s.x AS prev_x,
            s.y AS prev_y,
            1::int AS distance,
            n.pipe
        FROM
            input n
        JOIN
            directions AS d USING (pipe)
        JOIN
            input s ON (n.x = s.x+delta_x AND n.y=s.y+delta_y)
        WHERE
            s.pipe = 'S'
        LIMIT
            1
    ) _

    UNION ALL

    SELECT
        i.x,
        i.y,
        c.x,
        c.y,
        distance + 1,
        i.pipe
    FROM
        connected AS c
    JOIN
        directions USING (pipe)
    JOIN
        input AS i ON (NOT(i.x=c.prev_x AND i.y=c.prev_y) AND c.x+delta_x=i.x AND c.y+delta_y=i.y)
)
SELECT
    max(distance)/2
FROM
    connected;

EXECUTE problem10_1($$
.....
.S-7.
.|.|.
.L-J.
.....
$$);

EXECUTE problem10_1($$
..F7.
.FJ|.
SJ.L7
|F--J
LJ...
$$);

\set input `cat 10.input`
EXECUTE problem10_1(:'input');

set track_io_timing to 'on';
\o 10-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem10_1(:'input');
\o
