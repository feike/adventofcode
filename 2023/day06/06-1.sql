DEALLOCATE problem06_1;

PREPARE problem06_1 AS
WITH RECURSIVE input AS (
    SELECT
        game,
        (array_agg(matches[1]::int) FILTER (WHERE line LIKE 'Time%'))[1] AS "time",
        (array_agg(matches[1]::int) FILTER (WHERE line LIKE 'Distance%'))[1] AS "distance"
    FROM
        regexp_split_to_table($1, '\n') AS _(line)
    CROSS JOIN
        regexp_matches(line, '(\s\d+)+', 'g') WITH ORDINALITY AS rm(matches, game)
    GROUP BY
        game
), velocities AS (
    SELECT
        i,
        game,
        time,
        distance,
        i * (time - i) AS distance_travelled
    FROM
        generate_series(1, (SELECT max(distance) FROM input)) AS sub(i)
    JOIN
        input ON (input.distance > i AND input.time > i AND (i * (time - i)) > distance)
), winning_ways AS (
    SELECT
        game,
        count(*)
    FROM
        velocities
    GROUP BY
        game
), walk AS (
    SELECT * FROM (
        SELECT
            game,
            count AS product
        FROM
            winning_ways
        ORDER BY
            game ASC
        LIMIT
            1
    ) _

    UNION ALL

    SELECT
        winning_ways.game,
        product * count
    FROM
        walk
    JOIN
        winning_ways ON (walk.game+1=winning_ways.game)
)
SELECT
    product
FROM
    walk
ORDER BY
    game DESC
LIMIT
    1;

EXECUTE problem06_1($$
Time:      7  15   30
Distance:  9  40  200
$$);

\set input `cat 06.input`
EXECUTE problem06_1(:'input');

set track_io_timing to 'on';
\o 06-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_1(:'input');
\o
