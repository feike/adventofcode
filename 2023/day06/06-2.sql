DEALLOCATE problem06_2;

PREPARE problem06_2 AS
WITH RECURSIVE input AS (
    SELECT
        max(match::bigint) FILTER (WHERE line LIKE 'Time%') AS "time",
        max(match::bigint) FILTER (WHERE line LIKE 'Distance%') AS "distance"
    FROM
        regexp_split_to_table($1, '\n') AS _(line)
    CROSS JOIN
        regexp_replace(split_part(line, ':', 2), '\s+', '', 'g') AS rr(match)
), winning AS (
    SELECT
        count(*)
    FROM
        input
    CROSS JOIN
        generate_series(distance::bigint/time, time) AS sub(i)
    WHERE
        (time - i) * i > distance
)
SELECT
    *
FROM
    winning;

EXECUTE problem06_2($$
Time:      7  15   30
Distance:  9  40  200
$$);

\set input `cat 06.input`
EXECUTE problem06_2(:'input');

set track_io_timing to 'on';
\o 06-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_2(:'input');
\o
