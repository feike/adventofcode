DEALLOCATE problem22_2;

PREPARE problem22_2 AS
WITH input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    *
FROM
    input
;

EXECUTE problem22_2($$
dummy line
$$);

\set input `cat 22.input`
EXECUTE problem22_2(:'input');

set track_io_timing to 'on';
\o 22-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem22_2(:'input');
\o
