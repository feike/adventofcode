DEALLOCATE problem03_2;

PREPARE problem03_2 AS
WITH RECURSIVE input AS (
    SELECT
        x,
        y,
        v
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt(v, x)
    WHERE
        line != ''
), touching_gears AS (
    SELECT
        x AS gear_x,
        y AS gear_y,
        x+delta_x AS x,
        y+delta_y AS y,
        count(*) OVER (PARTITION BY x, y) AS touching
    FROM (
        SELECT
            s.x,
            s.y,
            delta_y,
            -- We're looking for at most 1 coordinate per touching number
            CASE
                array_agg(delta_x ORDER BY delta_x)
                WHEN ARRAY[-1,0,1] THEN ARRAY[0]
                WHEN ARRAY[-1,0] THEN ARRAY[-1]
                WHEN ARRAY[0,1] THEN ARRAY[1]
                ELSE array_agg(delta_x ORDER BY delta_x)
            END AS direction
        FROM
            input AS s
        CROSS JOIN (
                VALUES
                    (-1,1),
                    (0,1),
                    (1,1),
                    (-1,0),
                    (1,0),
                    (-1,-1),
                    (0,-1),
                    (1,-1)
            ) AS directions(delta_x, delta_y)
        JOIN
            input as i ON (s.y+delta_y=i.y AND s.x+delta_x=i.x AND i.v ~ '\d')
        WHERE
            s.v = '*'
        GROUP BY
            s.x,
            s.y,
            delta_y
        ) _
    CROSS JOIN
        unnest(direction) AS u(delta_x)
), walk AS (
    SELECT
        gear_x,
        gear_y,
        x AS number_x,
        y AS number_y,
        x,
        y,
        v
    FROM
        touching_gears AS tg
    JOIN
        input USING (x, y)
    WHERE
        touching = 2
    
    UNION ALL

    SELECT
        gear_x,
        gear_y,
        number_x,
        number_y,
        i.x,
        i.y,
        i.v
    FROM
        walk AS tg
    JOIN
        input AS i ON (i.v ~ '\d' AND i.y = tg.y AND (i.x=tg.x-1 OR i.x=tg.x+1))
) CYCLE x SET is_cycle USING path
, calc AS (
    SELECT
        gear_x,
        gear_y,
        y,
        number_x,
        string_agg(v, '' ORDER BY x)::bigint AS number
    FROM (
        SELECT
            DISTINCT
            gear_x,
            gear_y,
            number_x,
            y,
            x,
            v
        FROM
            walk
        ) _
    GROUP BY
        gear_x,
        gear_y,
        number_x,
        y
)
SELECT
    sum(min(number)*max(number)) OVER ()
FROM
    calc
GROUP BY
    gear_x,
    gear_y
LIMIT
    1;

EXECUTE problem03_2($$
467..114..
...*......
..35..633.
......#...
617* .....
.....+.58.
..592.....
......755.
...$.*....
.664.598..
$$);

\set input `cat 03.input`
EXECUTE problem03_2(:'input');

set track_io_timing to 'on';
\o 03-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_2(:'input');
\o
