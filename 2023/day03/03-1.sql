DEALLOCATE problem03_1;

PREPARE problem03_1 AS
WITH RECURSIVE input AS (
    SELECT
        x,
        y,
        v
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt(v, x)
    WHERE
        line != ''
), symbols AS (
    SELECT
        *
    FROM
        input
    WHERE
        v !~ '[\d.]'
), parts_adjacent AS (
    SELECT DISTINCT
        i.x,
        i.y
    FROM
        input AS i
    JOIN
        symbols AS s ON (
            (i.x=s.x AND i.y=s.y-1)
            OR (i.x=s.x AND i.y=s.y+1)
            OR (i.x=s.x-1 AND i.y=s.y)
            OR (i.x=s.x+1 AND i.y=s.y)
            OR (i.x=s.x+1 AND i.y=s.y+1)
            OR (i.x=s.x-1 AND i.y=s.y-1)
            OR (i.x=s.x+1 AND i.y=s.y-1)
            OR (i.x=s.x-1 AND i.y=s.y+1)
        )
    WHERE
        i.v ~ '\d'
    GROUP BY
        i.x,
        i.y

    UNION ALL

    SELECT
        i.x,
        i.y
    FROM
        parts_adjacent AS pa
    JOIN
        input AS i ON (i.v ~ '\d' AND i.y = pa.y AND (i.x=pa.x-1 OR i.x=pa.x+1))
) CYCLE x SET is_cycle USING path
, numbers AS (
    SELECT
        y,
        string_agg(CASE WHEN pa IS NULL THEN '.' ELSE v END, '' ORDER BY x) AS numbers
    FROM
        input
    LEFT JOIN
        (SELECT DISTINCT x, y FROM parts_adjacent) AS pa USING (x, y)
    GROUP BY
        y
)
SELECT
    sum(match::int)
FROM
    numbers
CROSS JOIN LATERAL
    (SELECT unnest(regexp_matches(numbers, '\d+', 'g'))) AS _(match);

EXECUTE problem03_1($$
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
$$);


\set input `cat 03.input`
EXECUTE problem03_1(:'input');

set track_io_timing to 'on';
\o 03-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_1(:'input');
\o
