#!/bin/bash

function create_file () {

cat > $1 <<__EOT__
DEALLOCATE problem${2}_${3};

PREPARE problem${2}_${3} AS
WITH input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table(\$1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    *
FROM
    input
;

EXECUTE problem${2}_${3}(\$\$
dummy line
\$\$);

\set input \`cat ${2}.input\`
EXECUTE problem${2}_${3}(:'input');

set track_io_timing to 'on';
\o ${2}-${3}.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem${2}_${3}(:'input');
\o
__EOT__

}

DIRNAME="$(dirname $0)/$1"
for i in $(seq 1 25)
do
	DAY="$(printf "%02d" ${i})"
	mkdir -p "${DIRNAME}/day${DAY}"
    create_file "${DIRNAME}/day${DAY}/${DAY}-1.sql" "${DAY}" 1
    create_file "${DIRNAME}/day${DAY}/${DAY}-2.sql" "${DAY}" 2
    touch "${DIRNAME}/day${DAY}/${DAY}.input"
done
