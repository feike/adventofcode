DEALLOCATE problem09_1;

PREPARE problem09_1 AS
WITH RECURSIVE directions (dir, delta_x, delta_y) AS (
    VALUES 
        ('U',  0, -1),
        ('D',  0,  1),
        ('L', -1,  0),
        ('R',  1,  0)
) , input(id, delta_x, delta_y, amount) AS (
    SELECT
        0,
        0,
        0,
        1
    UNION ALL
    SELECT
        id,
        delta_x,
        delta_y,
        matches[2]::int AS amount
    FROM
        regexp_split_to_table(trim(both E' \n' from $1), '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        regexp_split_to_array(line, ' ') AS rstt(matches)
    JOIN
        directions ON (matches[1]=dir)
), head_walk AS (
    SELECT
        rank() OVER (ORDER BY id, step) AS move_id,
        id,
        delta_x,
        delta_y,
        sum(delta_x) OVER (ORDER BY id, step) AS x,
        sum(delta_y) OVER (ORDER BY id, step) AS y
    FROM
        input
    CROSS JOIN
        generate_series(1, amount) AS _(step)
), tail_walk AS (
    SELECT
        x,
        y,
        move_id
    FROM
        head_walk
    WHERE
        move_id = 1
    UNION ALL
    SELECT
        CASE
            WHEN greatest(x_distance, y_distance) <= 1
            THEN t.x
            WHEN y_distance = 2
            THEN h.x
            ELSE t.x + delta_x
        END,
        CASE
            WHEN greatest(x_distance, y_distance) <= 1
            THEN t.y
            WHEN x_distance = 2
            THEN h.y
            ELSE t.y + delta_y
        END,
        h.move_id
    FROM
        tail_walk AS t
    JOIN
        head_walk AS h ON (t.move_id+1=h.move_id)
    CROSS JOIN
        abs(h.x-t.x) as xd(x_distance)
    CROSS JOIN
        abs(h.y-t.y) AS yd(y_distance)
)
SELECT
    count(distinct (x, y))
FROM
    tail_walk
;

EXECUTE problem09_1($$
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
$$);

\set input `cat 09.input`
EXECUTE problem09_1(:'input');

set track_io_timing to 'on';
\o 09-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_1(:'input');
\o
