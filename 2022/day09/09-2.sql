DEALLOCATE problem09_2;

PREPARE problem09_2 AS
WITH RECURSIVE directions (dir, delta_x, delta_y) AS (
    VALUES 
        ('U',  0, -1),
        ('D',  0,  1),
        ('L', -1,  0),
        ('R',  1,  0)
) , input(id, delta_x, delta_y, amount) AS (
    SELECT
        0,
        0,
        0,
        1
    UNION ALL
    SELECT
        id,
        delta_x,
        delta_y,
        matches[2]::int AS amount
    FROM
        regexp_split_to_table(trim(both E' \n' from $1), '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        regexp_split_to_array(line, ' ') AS rstt(matches)
    JOIN
        directions ON (matches[1]=dir)
), head_walk AS (
    SELECT
        rank() OVER (ORDER BY id, step) AS move_id,
        id,
        delta_x,
        delta_y,
        sum(delta_x) OVER (ORDER BY id, step) AS x,
        sum(delta_y) OVER (ORDER BY id, step) AS y
    FROM
        input
    CROSS JOIN
        generate_series(1, amount) AS _(step)
), tail_walk AS (
    SELECT
        jsonb_agg(
            jsonb_build_object(
                'knot', 0,
                'x', x,
                'y', y,
                'delta_x', delta_x,
                'delta_y', delta_y
            ) ORDER BY move_id
        ) AS game,
        0 AS knot
    FROM
        head_walk
    
    UNION ALL
    
    SELECT
        *
    FROM (
        WITH RECURSIVE tail_knot_walk(game, knot, move_id, x, y, delta_x, delta_y) AS (
            SELECT
                game,
                knot + 1,
                1,
                0,
                0,
                0,
                0
            FROM
                tail_walk

            UNION ALL

            SELECT
                game,
                knot,
                t.move_id + 1,
                t.x + incr.delta_x AS x,
                t.y + incr.delta_y AS y,
                incr.delta_x,
                incr.delta_y
            FROM
                tail_knot_walk AS t
            JOIN
                jsonb_extract_path(game, t.move_id::text) AS s(step) ON (step is not null)
            CROSS JOIN LATERAL (
                VALUES (
                    (step->>'x')::int,
                    (step->>'y')::int,
                    (step->>'delta_x')::int,
                    (step->>'delta_y')::int
                )
            ) AS h(x,y,delta_x,delta_y)
            CROSS JOIN LATERAL (
                SELECT
                    CASE
                        WHEN greatest(x_distance, y_distance) <= 1
                        THEN 0
                        WHEN y_distance = 2 AND x_distance < 2
                        THEN h.x - t.x
                        ELSE h.delta_x
                    END::int,
                    CASE
                        WHEN greatest(x_distance, y_distance) <= 1
                        THEN 0
                        WHEN x_distance = 2 AND y_distance < 2
                        THEN h.y - t.y
                        ELSE h.delta_y
                    END::int
                FROM
                    abs(h.x-t.x) as xd(x_distance)
                CROSS JOIN
                    abs(h.y-t.y) AS yd(y_distance)
                ) AS incr(delta_x, delta_y)
        )
        SELECT
            jsonb_agg(
                jsonb_build_object(
                    'x', x,
                    'y', y,
                    'delta_x', delta_x,
                    'delta_y', delta_y
                ) ORDER BY move_id
            ),
            knot
        FROM
            tail_knot_walk
        WHERE
            knot <= 9
            -- removing those steps that don't do anything helps to
            -- speed up the total running of this thing
            AND (move_id = 1 OR delta_x != 0 OR delta_y != 0)
        GROUP BY
            knot
    ) _
), visited (knot, x, y) AS (
    SELECT distinct
        knot,
        (step->'x')::int,
        (step->'y')::int
    FROM
        tail_walk
    CROSS JOIN
        jsonb_array_elements(game) AS _(step)
), display AS (
    SELECT
        knot,
        x,
        y,
        CASE
            WHEN visited IS NOT NULL
            THEN '#'
            ELSE '.'
        END AS visited
    FROM (
        SELECT
            knot,
            min(x),
            max(x),
            min(y),
            max(y)
        FROM
            visited
        GROUP BY 
            knot
        ) _(knot, min_x, max_x, min_y, max_y) 
    CROSS JOIN LATERAL
        generate_series(min_x, max_x) AS sx(x)
    CROSS JOIN LATERAL
        generate_series(min_y, max_y) as sy(y)
    LEFT JOIN
        visited USING (knot, x, y)
) , lines AS (
    SELECT
        knot,
        y,
        count(*) FILTER (WHERE visited = '#') AS count_visited,
        string_agg(visited, '' ORDER BY x) AS line
    FROM
        display
    GROUP BY
        knot,
        y
)
SELECT
    knot,
    sum(count_visited) AS visited,
    string_agg(format('%5s   %s', y, line), E'\n' ORDER BY y)||E'\n' AS grid
FROM
    lines
GROUP BY
    knot
ORDER BY
    knot DESC
LIMIT
    1
;

EXECUTE problem09_2($$
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2
$$);

EXECUTE problem09_2($$
R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20
$$);

\set input `cat 09.input`
EXECUTE problem09_2(:'input');

set track_io_timing to 'on';
\o 09-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_2(:'input');
\o
