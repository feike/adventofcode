DEALLOCATE problem11_1;

PREPARE problem11_1 AS
WITH RECURSIVE input AS MATERIALIZED (
    SELECT
        monkey[1] AS monkey,
        coalesce(items::int[], '{}') AS items,
        operation[1] AS operation,
        operation[2] AS argument,
        test[1]::int AS test_division,
        true_monkey[1] AS true_monkey,
        false_monkey[1] AS false_monkey
    FROM
        regexp_split_to_table($1, '\n\n') AS _(lines)
    LEFT JOIN
        regexp_matches(lines, 'Monkey\s*(\d+)') AS rmm(monkey) ON (true)
    LEFT JOIN
        regexp_matches(lines, 'Starting items: ([\d ,]+)') AS rmi(its) ON (true)
    LEFT JOIN
        regexp_split_to_array(its[1], ',') AS rsta(items) ON (true)
    LEFT JOIN
        regexp_matches(lines, 'Operation: new = old (.)\s+([^\s]+)') AS rmo(operation) ON (true)
    LEFT JOIN
        regexp_matches(lines, 'Test: divisible by (\d+)') AS rmt(test) ON (true)
    LEFT JOIN
        regexp_matches(lines, 'If true: throw to monkey (\d+)') AS rmtt(true_monkey) ON (true)
    LEFT JOIN
        regexp_matches(lines, 'If false: throw to monkey (\d+)') AS rmtf(false_monkey) ON (true)
), round AS (
    SELECT
        jsonb_object_agg(
            input.monkey,
            jsonb_build_object(
                'items',
                items
            )
        ) AS state,
        0::int AS round_id,
        -1 AS turn_id
    FROM
        input

    UNION ALL

    SELECT
        *
    FROM (
        WITH RECURSIVE turn AS (
            SELECT
                state,
                round_id + 1 AS round_id,
                -1 as turn_id
            FROM
                round
            WHERE
                round_id < 20

            UNION ALL

            SELECT
                new_state,
                round_id,
                turn_id + 1
            FROM
                turn
            JOIN
                input AS tm ON (tm.monkey=(turn_id+1)::text)
            LEFT JOIN LATERAL (
                SELECT
                    (SELECT
                        jsonb_object_agg(
                            key,
                            CASE
                                WHEN key::int = (turn_id+1)
                                THEN value||jsonb_build_object('items', '[]'::jsonb, 'inspected', count)
                                WHEN key = true_monkey
                                THEN value||jsonb_build_object('items', (state#>ARRAY[true_monkey,'items']::text[]||throw_true))
                                WHEN key = false_monkey
                                THEN value||jsonb_build_object('items', (state#>ARRAY[false_monkey,'items']::text[]||throw_false))
                                ELSE value
                            END
                        )
                    FROM
                        jsonb_each(state)
                    )
                FROM
                    jsonb_extract_path(state, tm.monkey, 'items') AS tm(throwing_items)
                LEFT JOIN LATERAL (
                    SELECT
                        jsonb_agg(multi ORDER BY idx) AS throw,
                        coalesce(jsonb_agg(multi/3 ORDER BY idx) FILTER (WHERE qualifies), '[]') AS throw_true,
                        coalesce(jsonb_agg(multi/3 ORDER BY idx) FILTER (WHERE NOT qualifies), '[]') AS throw_false,
                        count(*)
                    FROM
                        jsonb_array_elements(throwing_items) WITH ORDINALITY AS _(item, idx)
                    LEFT JOIN LATERAL (
                        SELECT
                            item::int
                            * CASE
                                WHEN operation =  '*' AND argument = 'old'
                                THEN item::int
                                WHEN operation = '*'
                                THEN argument::int
                                ELSE 1
                            END
                            + CASE
                                WHEN operation = '+' AND argument = 'old'
                                THEN item::int
                                WHEN operation = '+'
                                THEN argument::int
                                ELSE 0
                            END
                        ) AS multi(multi) ON (true)
                    LEFT JOIN
                        cast((multi/3)%test_division=0 AS bool) AS q(qualifies) ON (true)
                ) _ ON (true)
              ) _(new_state) ON (true)
            WHERE
                state IS NOT NULL
        )
        SELECT
            *
        FROM
            turn
        ORDER BY
            turn_id DESC
        FETCH FIRST 1 ROWS ONLY
    ) _
), wip AS (
    SELECT
        sum((value->>'inspected')::bigint),
        dense_rank() OVER (ORDER BY sum((value->>'inspected')::bigint) DESC, key DESC)
    FROM
        round
    CROSS JOIN
        jsonb_each(state)
    GROUP BY
        key
    ORDER BY
        sum((value->>'inspected')::bigint) DESC
    FETCH FIRST 2 ROWS ONLY
)
SELECT
    a.sum * b.sum
FROM
    wip AS a
CROSS JOIN
    wip as b
WHERE
    a.dense_rank=1
    AND b.dense_rank=2;

EXECUTE problem11_1($$
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
$$);

\set input `cat 11.input`
EXECUTE problem11_1(:'input');

set track_io_timing to 'on';
\o 11-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem11_1(:'input');
\o
