DEALLOCATE problem12_2;

PREPARE problem12_2 AS
WITH RECURSIVE elevation AS (
    SELECT
        *
    FROM
        regexp_split_to_table('abcdefghijklmnopqrstuvwxyz', '') WITH ORDINALITY AS _(code, elevation)
    UNION ALL
    SELECT
        'S',
        1
    UNION ALL
    SELECT
        'E',
        26
), input(x, y, elevation, code, idx) AS (
    SELECT
        x-1,
        y-1,
        elevation,
        code,
        rank() OVER (ORDER BY y, x)
    FROM
        regexp_split_to_table(trim(both E' \n' from $1), '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt(code, x)
    JOIN
        elevation USING (code)
), starting_field AS (
    SELECT
        array_agg(CASE WHEN code='S' OR code = 'a' THEN 0::smallint ELSE null END ORDER BY idx)::smallint[] AS state,
        array_agg(elevation::smallint ORDER BY idx)::smallint[] AS elevations,
        array_agg(code ORDER BY idx) AS codes,
        max(y) AS max_y,
        max(x)+1 AS width,
        max(idx) AS max_idx,
        (max(idx) FILTER (WHERE code = 'S')) AS start,
        (max(idx) FILTER (WHERE code = 'E')) AS finish
    FROM
        input
), dijkstra AS (
    SELECT
        state AS visited,
        0 AS cost,
        0::int as level
    FROM
        starting_field

    UNION ALL

    SELECT
        new_visited,
        cost + 1,
        level + 1
    FROM
        dijkstra
    CROSS JOIN
        starting_field
    CROSS JOIN LATERAL (
        SELECT
            array_agg(
                CASE
                    WHEN v = cost OR v = -1
                    THEN -1
                    WHEN visited[north] = cost AND (elevations[north]+1) >= elevations[idx]
                    THEN cost + 1
                    WHEN visited[east] = cost AND  (elevations[east]+1)  >= elevations[idx]
                    THEN cost + 1
                    WHEN visited[south] = cost AND (elevations[south]+1) >= elevations[idx]
                    THEN cost + 1
                    WHEN visited[west] = cost AND  (elevations[west]+1)  >= elevations[idx]
                    THEN cost + 1
                    ELSE v
                END::smallint
                ORDER BY idx
            )
        FROM
            unnest(visited) WITH ORDINALITY AS u(v, idx)
        CROSS JOIN LATERAL (
            VALUES (
                idx - width,
                CASE
                    WHEN idx%width = 0
                    THEN null
                    ELSE idx+1
                END,
                idx + width,
                CASE
                    WHEN (idx-1)%width = 0
                    THEN null
                    ELSE idx-1
                END
            )
        ) _(north, east, south, west)
    ) _(new_visited)
    WHERE
        visited[finish] IS null
        AND level < 400
)
SELECT
    visited[finish]
FROM
    dijkstra
CROSS JOIN
    starting_field
WHERE
    visited[finish] IS NOT null
ORDER BY
    level DESC
FETCH FIRST 1 ROWS ONLY
;

EXECUTE problem12_2($$
Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi
$$);

\set input `cat 12.input`
EXECUTE problem12_2(:'input');

set track_io_timing to 'on';
\o 12-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem12_2(:'input');
\o
