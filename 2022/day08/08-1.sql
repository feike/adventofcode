DEALLOCATE problem08_1;

PREPARE problem08_1 AS
WITH input(x, y, height) AS (
    SELECT
        x-1,
        y-1,
        height::int
    FROM
        regexp_split_to_table(trim(both E' \n' from $1), '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS sub(height, x)
), framed AS (
    SELECT
        x,
        y,
        height,
        max(height) OVER (partition by x order by y asc  rows UNBOUNDED PRECEDING EXCLUDE CURRENT ROW) AS north,
        max(height) OVER (partition by x order by y desc rows UNBOUNDED PRECEDING EXCLUDE CURRENT ROW) AS south,
        max(height) OVER (partition by y order by x asc  rows UNBOUNDED PRECEDING EXCLUDE CURRENT ROW) AS east,
        max(height) OVER (partition by y order by x desc rows UNBOUNDED PRECEDING EXCLUDE CURRENT ROW) AS west
    FROM
        input
)
SELECT
    count(*)
FROM
    framed
WHERE
    least(north,east,south,west) < height
    OR num_nulls(north,east,south,west) > 0
;

EXECUTE problem08_1($$
30373
25512
65332
33549
35390
$$);

\set input `cat 08.input`
EXECUTE problem08_1(:'input');

set track_io_timing to 'on';
\o 08-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_1(:'input');
\o
