DEALLOCATE problem08_2;

PREPARE problem08_2 AS
WITH input(x, y, height) AS (
    SELECT
        x-1,
        y-1,
        height::int
    FROM
        regexp_split_to_table(trim(both E' \n' from $1), '\n') WITH ORDINALITY AS _(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS sub(height, x)
), vision AS (
    SELECT
        x,
        y,
        height,
        -- We'de rather user array_agg(height ORDER BY $col ASC|DESC) here, however
        -- aggregate ORDER BY is not implemented for window functions.
        -- By collecting it in a json object, we retain ordering and values, which we later
        -- can use for calculating the actual vision
        jsonb_object_agg(y, height) OVER (partition by x order by y asc  rows UNBOUNDED PRECEDING EXCLUDE CURRENT ROW) AS north,
        jsonb_object_agg(y, height) OVER (partition by x order by y desc rows UNBOUNDED PRECEDING EXCLUDE CURRENT ROW) AS south,
        jsonb_object_agg(x, height) OVER (partition by y order by x asc  rows UNBOUNDED PRECEDING EXCLUDE CURRENT ROW) AS west,
        jsonb_object_agg(x, height) OVER (partition by y order by x desc rows UNBOUNDED PRECEDING EXCLUDE CURRENT ROW) AS east
    FROM
        input
), weights AS (
    SELECT
        y - (SELECT coalesce(max(key::bigint) FILTER (WHERE height <= value::int), min(key::bigint)) FROM jsonb_each(north)) AS north,
        x - (SELECT coalesce(max(key::bigint) FILTER (WHERE height <= value::int), min(key::bigint)) FROM jsonb_each(west))  AS west,
        (SELECT coalesce(min(key::bigint) FILTER (WHERE height <= value::int), max(key::bigint)) FROM jsonb_each(east))  - x AS east,
        (SELECT coalesce(min(key::bigint) FILTER (WHERE height <= value::int), max(key::bigint)) FROM jsonb_each(south)) - y AS south
    FROM
        vision
    WHERE
        num_nulls(north, east, south, west) = 0
)
SELECT
    max(north * east * south * west)
FROM
    weights
;

EXECUTE problem08_2($$
30373
25512
65332
33549
35390
$$);

\set input `cat 08.input`
EXECUTE problem08_2(:'input');

set track_io_timing to 'on';
\o 08-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_2(:'input');
\o
