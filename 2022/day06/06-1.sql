DEALLOCATE problem06_1;

PREPARE problem06_1 AS
SELECT
    i
FROM
    generate_series(4, length($1)) AS sub(i)
WHERE
    substring($1, i, 1) != substring($1, i-1, 1)
    AND substring($1, i, 1) != substring($1, i-2, 1)
    AND substring($1, i, 1) != substring($1, i-3, 1)
    AND substring($1, i-1, 1) != substring($1, i-2, 1)
    AND substring($1, i-1, 1) != substring($1, i-3, 1)
    AND substring($1, i-2, 1) != substring($1, i-3, 1)
ORDER BY
    i
FETCH FIRST 1 ROWS ONLY;    

EXECUTE problem06_1($$mjqjpqmgbljsphdztnvjfqwrcgsmlb$$);
EXECUTE problem06_1($$bvwbjplbgvbhsrlpgdmjqwftvncz$$);
EXECUTE problem06_1($$nppdvjthqldpwncqszvftbrmjlhg$$);
EXECUTE problem06_1($$nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg$$);
EXECUTE problem06_1($$zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw$$);

\set input `cat 06.input`
EXECUTE problem06_1(:'input');

set track_io_timing to 'on';
\o 06-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_1(:'input');
\o
