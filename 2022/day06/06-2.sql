DEALLOCATE problem06_2;

PREPARE problem06_2 AS
SELECT
    i
FROM
    regexp_split_to_table($1, '') WITH ORDINALITY AS rstt(c, idx)
JOIN
    generate_series(14, length($1)) AS _(i) ON (idx BETWEEN i-13 AND i)
GROUP BY
    i
HAVING
    count(distinct c) = 14
ORDER BY
    i
FETCH FIRST 1 ROWS ONLY;

EXECUTE problem06_2($$mjqjpqmgbljsphdztnvjfqwrcgsmlb$$);
EXECUTE problem06_2($$bvwbjplbgvbhsrlpgdmjqwftvncz$$);
EXECUTE problem06_2($$nppdvjthqldpwncqszvftbrmjlhg$$);
EXECUTE problem06_2($$nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg$$);
EXECUTE problem06_2($$zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw$$);

\set input `cat 06.input`
EXECUTE problem06_2(:'input');

set track_io_timing to 'on';
\o 06-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_2(:'input');
\o
