DEALLOCATE problem07_2;

PREPARE problem07_2 AS
WITH RECURSIVE input(line, id) AS (
    SELECT
        line,
        rank() OVER (order by id)
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
    WHERE
        line != ''
), walk AS (
    SELECT
        '{}'::text[] AS dir,
        null::text AS entry,
        null::bigint AS size,
        min(id) - 1 AS id
    FROM
        input
    UNION ALL
    SELECT
        CASE
            WHEN matches[1] = '$' AND matches[2] = 'cd'
            THEN
                CASE
                    WHEN matches[3] = '..'
                    THEN dir[1:array_length(dir, 1)-1]
                    ELSE array_append(dir, matches[3])
                END
            ELSE dir
        END,
        CASE
            WHEN matches[1] != '$'
            THEN matches[2]
        END,
        CASE
            WHEN matches[1] NOT IN ('$', 'dir')
            THEN matches[1]::bigint
        END,
        input.id
    FROM
        walk
    JOIN
        input ON (walk.id+1=input.id)
    CROSS JOIN
        regexp_split_to_array(line, ' ') AS _(matches)
), candidates AS (
    SELECT
        trimmed.dir,
        sum(size) AS dir_size,
        max(sum(size)) OVER () - sum(size) AS size_after_delete
    FROM
        walk
    CROSS JOIN LATERAL
        generate_series(1, array_length(dir, 1)) AS _(i)
    CROSS JOIN
        trim_array(dir, i-1) AS trimmed(dir)
    WHERE
        size IS NOT NULL
    GROUP BY
        trimmed.dir
)
SELECT
    dir_size
FROM
    candidates
WHERE
    size_after_delete <= (70000000-30000000)
ORDER BY
    dir_size ASC
FETCH FIRST 1 ROWS ONLY
;

EXECUTE problem07_2($$
$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
$$);

\set input `cat 07.input`
EXECUTE problem07_2(:'input');

set track_io_timing to 'on';
\o 07-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_2(:'input');
\o
