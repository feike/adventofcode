DEALLOCATE problem10_1;

PREPARE problem10_1 AS
WITH cycles (instruction, cycles) AS (
    VALUES 
        ('noop', 1),
        ('addx', 2)
), input(instruction, delta, after, before, cycles, id) AS (
    SELECT
        parts[1],
        delta,
        1 + coalesce(sum(delta)  OVER (w), 0),
        1 + coalesce(sum(delta) OVER (w), 0) - coalesce(delta, 0),
        int8range(1+sum(cycles) OVER (w) - cycles, 1+sum(cycles) OVER (w), '[)'),
        id
    FROM
        regexp_split_to_table(trim(both E' \n' from $1), '\n') WITH ORDINALITY AS _(line, id)\
    CROSS JOIN
        regexp_split_to_array(line, ' ') AS rsta(parts)
    CROSS JOIN
        cast(parts[2] AS bigint) AS c(delta)
    JOIN
        cycles ON (parts[1]=instruction)
    WINDOW
        w AS (ORDER BY id ASC)
)
SELECT
    sum(before * i)
FROM
    input
JOIN
    generate_series(20, (SELECT max(upper(cycles)) FROM input), 40) AS s(i) ON (i <@ cycles)
;

EXECUTE problem10_1($$
noop
addx 3
addx -5
$$);

EXECUTE problem10_1($$
addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop
$$);

\set input `cat 10.input`
EXECUTE problem10_1(:'input');

set track_io_timing to 'on';
\o 10-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem10_1(:'input');
\o
