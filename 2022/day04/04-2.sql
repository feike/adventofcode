DEALLOCATE problem04_2;

PREPARE problem04_2 AS
WITH input AS (
    SELECT
        line_no,
        part,
        array_agg(section) AS sections
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, line_no)
    CROSS JOIN
        regexp_split_to_table(line, ',') WITH ORDINALITY AS overlap(sections, part)
    CROSS JOIN
        regexp_split_to_array(sections, '-') AS section(range)
    CROSS JOIN
        generate_series(range[1]::int, range[2]::int) AS s(section)
    WHERE
        line != ''
    GROUP BY
        line_no,
        part
)
SELECT
    count(*)
FROM
    input a
JOIN
    input b ON (a.line_no=b.line_no AND a.part < b.part AND a.sections && b.sections)
;
EXECUTE problem04_2($$
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
$$);

\set input `cat 04.input`
EXECUTE problem04_2(:'input');

set track_io_timing to 'on';
\o 04-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem04_2(:'input');
\o
