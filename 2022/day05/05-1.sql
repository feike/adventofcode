DEALLOCATE problem05_1;

PREPARE problem05_1 AS
WITH RECURSIVE input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
), stacks AS (
    SELECT
        stack_id,
        string_agg(part, '' ORDER BY id ASC) FILTER (WHERE stack_id != part) AS elements
    FROM (
        SELECT
            id,
            part,
            col_idx,
            last_value(part) OVER (PARTITION BY col_idx ORDER BY id RANGE BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS stack_id
        FROM
            input
        CROSS JOIN
            regexp_split_to_table(line, '') WITH ORDINALITY AS parts(part, col_idx)
        WHERE
            line NOT LIKE '%move%from%to%'
        ) _
    WHERE
        stack_id NOT IN (' ', '')
        AND part NOT IN (' ', '')
    GROUP BY
        stack_id
), moves AS (
    SELECT
        matches[1]::int AS amount,
        matches[2] AS stack_pop,
        matches[3] AS stack_push,
        rank() OVER (order by id) AS move_id
    FROM
        input
    CROSS JOIN
        regexp_matches(line, '^\s*move\s+(\d+)\s+from\s+(\d+)\s+to\s+(\d+)\s*$') AS matches
), walk AS (
    SELECT
        1 AS move_id,
        jsonb_object_agg(
            stack_id,
            elements
        ) AS stacks
    FROM
        stacks
    UNION ALL
    SELECT
        walk.move_id + 1,
        game.stacks
    FROM
        walk
    JOIN
        moves USING (move_id)
    CROSS JOIN LATERAL (
        SELECT
            jsonb_object_agg(
                key,
                CASE
                    WHEN key = stack_pop
                    THEN substring(value, amount+1)
                    WHEN key = stack_push
                    THEN reverse(substring(stacks->>stack_pop, 1, amount))||value
                    ELSE value
                END
            )
        FROM
            jsonb_each_text(stacks)
        ) game(stacks)
) , last_game AS (
    SELECT
        *
    FROM
        walk
    ORDER BY
        move_id DESC
    FETCH FIRST 1 ROWS ONLY
)
SELECT
    string_agg(substring(value, 1, 1), '' ORDER BY key)
FROM
    last_game
CROSS JOIN
    jsonb_each_text(stacks)
;

EXECUTE problem05_1($$
    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
$$);

\set input `cat 05.input`
EXECUTE problem05_1(:'input');

set track_io_timing to 'on';
\o 05-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem05_1(:'input');
\o
