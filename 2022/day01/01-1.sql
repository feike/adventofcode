DEALLOCATE problem01_1;

PREPARE problem01_1 AS
WITH inventory AS (
    SELECT
        nullif(line, '')::bigint AS calories,
        count(*) FILTER (WHERE line = '') OVER (ORDER BY id) AS elf,
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    sum(calories)
FROM
    inventory
GROUP BY
    elf
ORDER BY
    sum(calories) DESC NULLS LAST
LIMIT
    1
;

EXECUTE problem01_1($$
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
$$);

\set input `cat 01.input`
EXECUTE problem01_1(:'input');

set track_io_timing to 'on';
\o 01-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_1(:'input');
\o
