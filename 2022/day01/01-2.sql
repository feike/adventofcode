DEALLOCATE problem01_2;

PREPARE problem01_2 AS
WITH inventory AS (
    SELECT
        nullif(line, '')::bigint AS calories,
        count(*) FILTER (WHERE line = '') OVER (ORDER BY id) AS elf,
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
), top_three(calories) AS (
    SELECT
        sum(calories)
    FROM
        inventory
    GROUP BY
        elf
    ORDER BY
        sum(calories) DESC NULLS LAST
    LIMIT
        3
)
SELECT
    sum(calories)
FROM
    top_three;

EXECUTE problem01_2($$
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
$$);


\set input `cat 01.input`
EXECUTE problem01_2(:'input');

set track_io_timing to 'on';
\o 01-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_2(:'input');
\o
