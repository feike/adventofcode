DEALLOCATE problem02_2;

PREPARE problem02_2 AS
WITH play_value(opponent, result, value) AS (
    VALUES
        ('A', 'X', 3+0),
        ('A', 'Y', 1+3),
        ('A', 'Z', 2+6),
        ('B', 'X', 1+0),
        ('B', 'Y', 2+3),
        ('B', 'Z', 3+6),
        ('C', 'X', 2+0),
        ('C', 'Y', 3+3),
        ('C', 'Z', 1+6)
), input AS (
    SELECT
        parts[1] AS opponent,
        parts[2] AS result,
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        regexp_split_to_array(line, ' ') AS parts
    WHERE
        line != ''
)
SELECT
    sum(value)
FROM
    input
JOIN
    play_value USING (opponent, result)
;

EXECUTE problem02_2($$
A Y
B X
C Z
$$);

\set input `cat 02.input`
EXECUTE problem02_2(:'input');

set track_io_timing to 'on';
\o 02-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_2(:'input');
\o
