DEALLOCATE problem02_1;

PREPARE problem02_1 AS
WITH play_value(play, wins, loses, value) AS (
    VALUES
        ('X', 'C', 'B', 1),
        ('Y', 'A', 'C', 2),
        ('Z', 'B', 'A', 3)
), input AS (
    SELECT
        parts[1] AS opponent,
        parts[2] AS me,
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
    CROSS JOIN
        regexp_split_to_array(line, ' ') AS parts
    WHERE
        line != ''
)
SELECT
    sum(
        value
        +
        CASE
            WHEN input.opponent=play_value.wins
            THEN 6
            WHEN input.opponent=play_value.loses
            THEN 0
            ELSE 3
        END
    )
FROM
    input
JOIN
    play_value ON (input.me=play_value.play)
;

EXECUTE problem02_1($$
A Y
B X
C Z
$$);

\set input `cat 02.input`
EXECUTE problem02_1(:'input');

set track_io_timing to 'on';
\o 02-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_1(:'input');
\o
