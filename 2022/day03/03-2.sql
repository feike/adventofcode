DEALLOCATE problem03_2;

PREPARE problem03_2 AS
WITH values AS (
    SELECT
        *
    FROM
        regexp_split_to_table('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', '') WITH ORDINALITY AS _(item, value)
), input AS (
    SELECT
        rucksack,
        (rucksack-1)/3 as group_three,
        item
    FROM
        regexp_split_to_table(ltrim($1, E' \n'), '\n') WITH ORDINALITY AS _(line, rucksack)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS contents(item, idx)
    WHERE
        line != ''
), unique_contents AS (
    SELECT
        group_three,
        rucksack,
        item
    FROM
        input
    GROUP BY
        group_three,
        rucksack,
        item
)
SELECT
    sum(value) OVER ()
FROM
    unique_contents
JOIN
    values USING (item)
GROUP BY
    group_three,
    value
HAVING
    count(*) = 3
LIMIT
    1
;

EXECUTE problem03_2($$
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
$$);

\set input `cat 03.input`
EXECUTE problem03_2(:'input');

set track_io_timing to 'on';
\o 03-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_2(:'input');
\o
