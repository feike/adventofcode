DEALLOCATE problem03_1;

PREPARE problem03_1 AS
WITH values AS (
    SELECT
        *
    FROM
        regexp_split_to_table('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', '') WITH ORDINALITY AS _(item, value)
), input AS (
    SELECT
        rucksack,
        item,
        idx,
        CASE
            WHEN idx <= length(line)/2
            THEN 1
            ELSE 2
        END AS compartment
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, rucksack)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS contents(item, idx)
    WHERE
        line != ''
), appears AS (
    SELECT
        rucksack,
        item
    FROM
        input a
    JOIN
        input b USING (rucksack, item)
    WHERE
        a.compartment < b.compartment
    GROUP BY
        rucksack,
        item
)
SELECT
    sum(value)
FROM
    appears
JOIN
    values USING (item)
;

EXECUTE problem03_1($$
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
$$);

\set input `cat 03.input`
EXECUTE problem03_1(:'input');

set track_io_timing to 'on';
\o 03-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_1(:'input');
\o
