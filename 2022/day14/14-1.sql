DEALLOCATE problem14_1;

PREPARE problem14_1 AS
WITH RECURSIVE sand_start(sx, sy) AS (
    -- PG arrays start with 1
    VALUES (500+1, 0+1)
), input AS (
    SELECT
        line_id,
        part_id,
        coord[1]::smallint AS to_x,
        coord[2]::smallint AS to_y,
        lag(coord[1]::smallint) OVER (PARTITION BY line_id ORDER BY part_id) AS from_x,
        lag(coord[2]::smallint) OVER (PARTITION BY line_id ORDER BY part_id) AS from_y
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, line_id)
    CROSS JOIN
        regexp_split_to_table(line, '->') WITH ORDINALITY AS rstt(part, part_id)
    CROSS JOIN
        regexp_split_to_array(part, ',') WITH ORDINALITY AS coord(coord, idx)
    WHERE
        line != ''
), walls(x, y) AS MATERIALIZED (
    SELECT DISTINCT
        -- PG arrays start with 1
        x + 1,
        y + 1
    FROM
        input
    CROSS JOIN
        generate_series(from_x, to_x, CASE WHEN to_x < from_x THEN -1 ELSE 1 END) AS gsx(x)
    CROSS JOIN
        generate_series(from_y, to_y, CASE WHEN to_y < from_y THEN -1 ELSE 1 END) AS gsy(y)
), initial_grid AS (
    SELECT
        sy,
        sx,
        array_agg(line ORDER BY y) AS grid,
        max(width) AS width,
        count(*) AS height
    FROM (
        SELECT
            y,
            sx - min_x + 1 AS sx,
            sy - min_y + 1 AS sy,
            count(*) AS width,
            array_agg(walls is not null ORDER BY x) AS line
        FROM (
            SELECT min(least(x, sx)), max(greatest(x, sx)), min(least(y, sy)), max(greatest(y, sy)) FROM walls CROSS JOIN sand_start
            ) _(min_x, max_x, min_y, max_y)
        CROSS JOIN
            generate_series(min_x, max_x, 1) AS x(x)
        CROSS JOIN
            generate_series(min_y, max_y, 1) AS y(y)
        LEFT JOIN
            walls USING (x, y)
        CROSS JOIN
            sand_start
        GROUP BY
            y,
            sx - min_x + 1,
            sy - min_y + 1
        ) _
    GROUP BY
        sx,
        sy
), round(level, sand) AS (
    SELECT
        0::int AS level,
        ARRAY[]::smallint[][] AS sand
    FROM
        initial_grid

    UNION ALL

    SELECT
        *
    FROM (
        WITH recursive mysand(level, sand) AS (
            SELECT
                level + 1,
                sand
            FROM
                round
        ), grain(grid, sy, sx) AS (
            SELECT
                newgrid.grid,
                sy,
                sx
            FROM
                mysand
            CROSS JOIN
                initial_grid
            -- We recreate the grid for every recursive step, so we don't need to keep *all*
            -- the grids in memory. The only thing we keep in memory is the initial grid
            -- and the list of sand grains already processed
            CROSS JOIN LATERAL
                (
                    SELECT
                        array_agg(line ORDER BY y_idx)
                    FROM (
                        SELECT
                            y_idx,
                            array_agg(
                                CASE
                                    WHEN gs IS null
                                    THEN grid[y_idx][x_idx]
                                    ELSE null
                                END
                                ORDER BY y_idx
                            ) AS line
                        FROM
                            generate_subscripts(grid, 1) AS gsy(y_idx)
                        CROSS JOIN
                            generate_subscripts(grid, 2) AS gsx(x_idx)
                        LEFT JOIN
                            generate_subscripts(sand, 1) AS gs(idx) ON (y_idx = sand[idx][1] AND x_idx = sand[idx][2])
                        GROUP BY
                            y_idx
                    ) _
                ) newgrid(grid)

            UNION ALL

            SELECT
                grid,
                sy,
                sx
            FROM
                grain
            WHERE
                false
        )
        SELECT
            level,
            sand||ARRAY[sy, sx]::smallint[]
        FROM
            mysand
        CROSS JOIN
            grain
    ) _
    WHERE
        level < 2
), initial_pretty AS (
    SELECT
        y,
        string_agg(
            CASE
                WHEN y = sy AND x = sx THEN '+'
                WHEN grid[y][x] THEN '#'
                WHEN grid[y][x] IS NULL THEN 'o'
                ELSE '.'
            END, ''
            ORDER BY x)
    FROM
        initial_grid
    CROSS JOIN
        generate_subscripts(grid, 1) AS gsy(y)
    CROSS JOIN
        generate_subscripts(grid, 2) AS gsx(x)
    GROUP BY
        y
    ORDER BY
        y
)
SELECT
    *
FROM
    round
;


EXECUTE problem14_1($$
498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9
$$);

\set input `cat 14.input`
--EXECUTE problem14_1(:'input');

set track_io_timing to 'on';
\o 14-1.explain-analyze.txt
--EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem14_1(:'input');
\o
