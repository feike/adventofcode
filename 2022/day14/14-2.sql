DEALLOCATE problem14_2;

PREPARE problem14_2 AS
WITH input(line, id) AS (
    SELECT
        nullif(line, ''),
        id
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS _(line, id)
)
SELECT
    *
FROM
    input
;

EXECUTE problem14_2($$
498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9
$$);

\set input `cat 14.input`
EXECUTE problem14_2(:'input');

set track_io_timing to 'on';
\o 14-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem14_2(:'input');
\o
