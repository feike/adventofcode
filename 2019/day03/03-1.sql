DEALLOCATE problem03_1;

PREPARE problem03_1 AS
WITH RECURSIVE input AS MATERIALIZED (
    SELECT
        wire,
        idx,
        CASE substring(instruction, 1, 1)
            WHEN 'D' THEN -1
            WHEN 'U' THEN 1
            ELSE 0
        END as dy,
        CASE substring(instruction, 1, 1)
            WHEN 'L' THEN -1
            WHEN 'R' THEN 1
            ELSE 0
        END as dx,
        substring(instruction, 2)::int AS length
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(instructions, wire)
    CROSS JOIN LATERAL
        regexp_split_to_table(instructions, ',') WITH ORDINALITY AS rstt2(instruction, idx)
    WHERE
        instructions !~ '^\s*$'
), unwind(wire, length, x, y, idx, ordinality) AS (
    SELECT
        distinct wire,
        0,
        0,
        0,
        0::bigint,
        0::bigint
    FROM
        input
    UNION ALL
    SELECT
        input.wire,
        input.length,
        x.x,
        y.y,
        input.idx,
        xidx + yidx - 2 -- In order to have the recursiveness only match the *last* record
                        -- of the unwind, we keep track of the current path length
    FROM
        unwind
    JOIN
        input ON (unwind.wire = input.wire AND unwind.idx + 1 = input.idx AND (ordinality = unwind.length))
    CROSS JOIN
        generate_series(x, x + dx * input.length, CASE dx WHEN 0 THEN 1 ELSE dx END) WITH ordinality AS x(x, xidx)
    CROSS JOIN
        generate_series(y, y + dy * input.length, CASE dy WHEN 0 THEN 1 ELSE dy END) WITH ordinality AS y(y, yidx)
)
SELECT
    abs(x) + abs(y)
FROM
    unwind
WHERE
    ordinality > 0
GROUP BY
    x,
    y
HAVING
    count(distinct wire) > 1
ORDER BY 
    (abs(x) + abs(y)) ASC
LIMIT
    1
;

EXECUTE problem03_1(
$$R8,U5,L5,D3
U7,R6,D4,L4
$$);

EXECUTE problem03_1(
$$R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83
$$);

EXECUTE problem03_1(
$$R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7
$$);

\set input `cat 03.input`
EXECUTE problem03_1(:'input');

set track_io_timing to 'on';
\o 03-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem03_1(:'input');
\o
