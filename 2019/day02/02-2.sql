DEALLOCATE problem02_2;

PREPARE problem02_2 AS
WITH RECURSIVE program(instructions, pointer, opcode, noun, verb) AS (
    SELECT
        jsonb_object_agg(idx-1, value::int)
            || jsonb_build_object('1', noun, '2', verb),
        0::int,
        0::int,
        noun,
        verb
    FROM
        regexp_split_to_table($1, ',') WITH ORDINALITY AS rsta(value, idx)
    CROSS JOIN
        generate_series(0,99) AS gs(noun),
        generate_series(0,99) AS gs2(verb)
    WHERE
        value <> ''
    GROUP BY
        noun,
        verb
    UNION ALL
    SELECT
        CASE sub.opcode
            WHEN '1'
            THEN jsonb_set(instructions, address, to_jsonb(x + y))
            WHEN '2'
            THEN jsonb_set(instructions, address, to_jsonb(x * y))
            ELSE instructions
        END,
        pointer + 4,
        sub.opcode,
        noun,
        verb
    FROM
        program
    CROSS JOIN LATERAL
        (
            SELECT
                jsonb_path_query(instructions, format('$.%I', pointer)::jsonpath)::int,
                jsonb_path_query(instructions, format('$.%I', pointer+1)::jsonpath)::text,
                jsonb_path_query(instructions, format('$.%I', pointer+2)::jsonpath)::text,
                ARRAY[jsonb_path_query(instructions, format('$.%I', pointer+3)::jsonpath)::text]::text[]
        ) AS sub(opcode, k, l, address)
    CROSS JOIN LATERAL
        (
            SELECT
                jsonb_path_query(instructions, format('$.%I', k)::jsonpath)::int,
                jsonb_path_query(instructions, format('$.%I', l)::jsonpath)::int
        ) AS sub2(x, y)
    WHERE
        pointer IS NOT NULL
        AND program.opcode <> 99
) , correct_instructions AS (
    SELECT
        noun,
        verb,
        instructions
    FROM
        program
    WHERE
        opcode = '99'
        AND (instructions->>'0')::int = 19690720
)
SELECT
    100 * noun + verb AS solution,
    *
FROM
    correct_instructions;

EXECUTE problem02_2('1,0,0,0,99');
EXECUTE problem02_2('2,3,0,3,99');
EXECUTE problem02_2('2,4,4,5,99,0');
EXECUTE problem02_2('1,1,1,4,99,5,6,0,99');

\set input `cat 02.input`
EXECUTE problem02_2(:'input');

set track_io_timing to 'on';
\o 02-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_2(:'input');
\o
