DEALLOCATE problem02_1;

PREPARE problem02_1 AS
WITH RECURSIVE program(instructions, position) AS (
    SELECT
        jsonb_object_agg(idx-1, instruction::int)
        || jsonb_build_object('1', 12, '2', 2),
        0::int
    FROM
        regexp_split_to_table($1, ',') WITH ORDINALITY AS rsta(instruction, idx)
    WHERE
        instruction <> ''
    UNION ALL
    SELECT
        CASE i
            WHEN '1'
            THEN jsonb_set(instructions, path, to_jsonb(x + y))
            WHEN '2'
            THEN jsonb_set(instructions, path, to_jsonb(x * y))
        END,
        position + 4
    FROM
        program
    CROSS JOIN LATERAL
        (
            SELECT
                jsonb_path_query(instructions, format('$.%I', position)::jsonpath),
                jsonb_path_query(instructions, format('$.%I', position+1)::jsonpath)::text,
                jsonb_path_query(instructions, format('$.%I', position+2)::jsonpath)::text,
                ARRAY[jsonb_path_query(instructions, format('$.%I', position+3)::jsonpath)::text]::text[]
        ) AS sub(i, k, l, path)
    CROSS JOIN LATERAL
        (
            SELECT
                jsonb_path_query(instructions, format('$.%I', k)::jsonpath)::int,
                jsonb_path_query(instructions, format('$.%I', l)::jsonpath)::int
        ) AS sub2(x, y)
    WHERE
        i = '1'
        OR i = '2'
)
SELECT
    string_agg(value, ',' ORDER BY key)
FROM
    jsonb_each_text((SELECT instructions FROM program ORDER BY position DESC LIMIT 1))
;

EXECUTE problem02_1('1,0,0,0,99');
EXECUTE problem02_1('2,3,0,3,99');
EXECUTE problem02_1('2,4,4,5,99,0');
EXECUTE problem02_1('1,1,1,4,99,5,6,0,99');

\set input `cat 02.input`
EXECUTE problem02_1(:'input');

set track_io_timing to 'on';
\o 02-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem02_1(:'input');
\o
