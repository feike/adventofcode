DEALLOCATE problem09_1;

PREPARE problem09_1 AS
WITH RECURSIVE stdin(stdin) AS MATERIALIZED (
    SELECT
        *
    FROM
        regexp_split_to_array($2, ',')
), input(instructions) AS MATERIALIZED (
    SELECT
        jsonb_object_agg(idx-1, instruction::bigint)
    FROM
        regexp_split_to_table($1, ',') WITH ORDINALITY AS rsta(instruction, idx)
    WHERE
        instruction <> ''
), program(instructions, pointer, base, mode_opcode, stdin_idx, value, iter) AS (
    SELECT
        instructions,
        0::bigint,
        0::bigint,
        0::bigint,
        1,
        null::text,
        0::bigint
    FROM
        input
    UNION ALL
    SELECT
        CASE opcode
            WHEN 1
            THEN jsonb_set(instructions, path, to_jsonb(x + y))
            WHEN 2
            THEN jsonb_set(instructions, path, to_jsonb(x * y))
            WHEN 3
            THEN jsonb_set(instructions, path, to_jsonb(stdin[stdin_idx]::int))
            WHEN 7
            THEN CASE
                    WHEN x < y
                    THEN jsonb_set(instructions, path, to_jsonb(1))
                    ELSE jsonb_set(instructions, path, to_jsonb(0))
                END
            WHEN 8
            THEN CASE
                    WHEN x = y
                    THEN jsonb_set(instructions, path, to_jsonb(1))
                    ELSE jsonb_set(instructions, path, to_jsonb(0))
                END
            ELSE instructions
        END,
        -- pointer
        CASE opcode
            WHEN 3
            THEN pointer + 2
            WHEN 4
            THEN pointer + 2
            WHEN 9
            THEN pointer + 2
            WHEN 5
            THEN CASE
                    WHEN x <> 0
                    THEN y
                    ELSE pointer + 3
                 END
            WHEN 6
            THEN CASE
                    WHEN x = 0
                    THEN y
                    ELSE pointer + 3
                 END
            ELSE pointer + 4
        END,
        -- base
        CASE opcode
            WHEN 9
            THEN base + x
            ELSE base
        END,
        -- opcode
        i,
        -- stdin_idx
        CASE opcode
            WHEN 3
            THEN stdin_idx + 1
            ELSE stdin_idx
        END,
        -- value
        CASE
            WHEN opcode = '4'
            THEN x::text
        END,
        iter + 1
    FROM
        program
    CROSS JOIN
        stdin
    CROSS JOIN LATERAL
        (
            SELECT
                (instructions->>pointer::text)::bigint,
                coalesce(instructions->>(pointer+1)::text, '0'),
                coalesce(instructions->>(pointer+2)::text, '0'),
                coalesce(instructions->>(pointer+3)::text, '0')
        ) AS sub(i, k, l, j)
    CROSS JOIN LATERAL
        (
            SELECT
                i%100,
                CASE (i/100)%10
                    WHEN 0
                    THEN coalesce((instructions->>k)::bigint, 0)
                    WHEN 1
                    THEN k::bigint
                    WHEN 2
                    THEN coalesce((instructions->>(k::bigint+base)::text)::bigint, 0)
                END,
                CASE (i/1000)%10
                    WHEN 0  
                    THEN coalesce((instructions->>l)::bigint, 0)
                    WHEN 1
                    THEN l::bigint
                    WHEN 2
                    THEN coalesce((instructions->>(l::bigint+base)::text)::bigint, 0)
                END,
                CASE (i/10000)%10
                    WHEN 0  
                    THEN ARRAY[j]::text[]
                    WHEN 2
                    THEN ARRAY[(j::bigint+base)]::text[]
                END
        ) AS sub2(opcode, x, y, path)
    WHERE
        mode_opcode%100 != 99
)
SELECT
    string_agg(value, ',' ORDER BY iter ASC)
FROM
    program
;


\set stdin '1'

\set input '109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99'
EXECUTE problem09_1(:'input', :'stdin');

\set input '1102,34915192,34915192,7,4,7,99,0'
EXECUTE problem09_1(:'input', :'stdin');

\set input '104,1125899906842624,99'
EXECUTE problem09_1(:'input', :'stdin');

\set input `cat 09.input`
EXECUTE problem09_1(:'input', :'stdin');

set track_io_timing to 'on';
\o 09-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem09_1(:'input', :'stdin');
\o
