DEALLOCATE problem06_1;

PREPARE problem06_1 AS
WITH RECURSIVE input(orbited, orbits) AS MATERIALIZED (
    SELECT
        split_part(Line, ')', 1),
        split_part(Line, ')', 2)
    FROM
        regexp_split_to_table($1, '\n') AS rstt(line)
), orbit (orbited, orbits, level) AS (
    SELECT
        null::text,
        orbited,
        0
    FROM
        input a
    WHERE
        NOT EXISTS (
            SELECT
            FROM
                input b
            WHERE
                a.orbited = b.orbits
        )
    UNION ALL
    SELECT
        input.orbited,
        input.orbits,
        level + 1
    FROM
        orbit
    JOIN
        input ON (orbit.orbits = input.orbited)
)
SELECT
    sum(level)
FROM
    orbit
;

EXECUTE problem06_1(
$$COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L$$);

\set input `cat 06.input`
EXECUTE problem06_1(:'input');

set track_io_timing to 'on';
\o 06-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_1(:'input');
\o
