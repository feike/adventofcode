DEALLOCATE problem06_1;

PREPARE problem06_1 AS
WITH RECURSIVE input(orbited, orbits) AS MATERIALIZED (
    SELECT
        split_part(Line, ')', 1),
        split_part(Line, ')', 2)
    FROM
        regexp_split_to_table($1, '\n') AS rstt(line)
), orbit (orbited, orbits, path) AS (
    SELECT
        null::text,
        orbited,
        ARRAY[orbited]::text[]
    FROM
        input a
    WHERE
        NOT EXISTS (
            SELECT
            FROM
                input b
            WHERE
                a.orbited = b.orbits
        )
    UNION ALL
    SELECT
        input.orbited,
        input.orbits,
        path||input.orbits
    FROM
        orbit
    JOIN
        input ON (orbit.orbits = input.orbited)
), you(orbits) AS (
    SELECT
        u.*
    FROM
        orbit
    JOIN LATERAL
        unnest(path) AS u ON orbit.orbits='YOU'
    WHERE
        u <> 'YOU'
), santa(orbits) AS (
    SELECT
        u.*
    FROM
        orbit
    JOIN LATERAL
        unnest(path) AS u ON orbit.orbits='SAN'
    WHERE
        u <> 'SAN'

)
SELECT
    count(*)
FROM
    you
FULL OUTER JOIN
    santa USING (orbits)
WHERE
    you.orbits IS NULL OR santa.orbits IS NULL
;

EXECUTE problem06_1(
$$COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN$$);

\set input `cat 06.input`
EXECUTE problem06_1(:'input');

set track_io_timing to 'on';
\o 06-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem06_1(:'input');
\o
