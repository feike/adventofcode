DEALLOCATE problem13_1;

PREPARE problem13_1 AS
WITH RECURSIVE stdin(stdin) AS MATERIALIZED (
    SELECT
        *
    FROM
        regexp_split_to_array($2, ',')
), input(instructions) AS MATERIALIZED (
    SELECT
        jsonb_object_agg(idx-1, instruction::bigint)
    FROM
        regexp_split_to_table($1, ',') WITH ORDINALITY AS rsta(instruction, idx)
    WHERE
        instruction <> ''
), program(instructions, pointer, base, mode_opcode, stdin_idx, value, iter) AS (
    SELECT
        instructions,
        0::bigint,
        0::bigint,
        0::bigint,
        1,
        null::bigint,
        0::bigint
    FROM
        input
    UNION ALL
    SELECT
        CASE opcode
            WHEN 1
            THEN jsonb_set(instructions, path, to_jsonb(x + y))
            WHEN 2
            THEN jsonb_set(instructions, path, to_jsonb(x * y))
            WHEN 3
            THEN jsonb_set(instructions, path, to_jsonb(stdin[stdin_idx]::int))
            WHEN 7
            THEN CASE
                    WHEN x < y
                    THEN jsonb_set(instructions, path, to_jsonb(1))
                    ELSE jsonb_set(instructions, path, to_jsonb(0))
                END
            WHEN 8
            THEN CASE
                    WHEN x = y
                    THEN jsonb_set(instructions, path, to_jsonb(1))
                    ELSE jsonb_set(instructions, path, to_jsonb(0))
                END
            ELSE instructions
        END,
        -- pointer
        CASE opcode
            WHEN 3
            THEN pointer + 2
            WHEN 4
            THEN pointer + 2
            WHEN 9
            THEN pointer + 2
            WHEN 5
            THEN CASE
                    WHEN x <> 0
                    THEN y
                    ELSE pointer + 3
                 END
            WHEN 6
            THEN CASE
                    WHEN x = 0
                    THEN y
                    ELSE pointer + 3
                 END
            ELSE pointer + 4
        END,
        -- base
        CASE opcode
            WHEN 9
            THEN base + x
            ELSE base
        END,
        -- opcode
        i,
        -- stdin_idx
        CASE opcode
            WHEN 3
            THEN stdin_idx + 1
            ELSE stdin_idx
        END,
        -- value
        CASE
            WHEN opcode = '4'
            THEN x
        END,
        iter + 1
    FROM
        program
    CROSS JOIN
        stdin
    CROSS JOIN LATERAL
        (
            SELECT
                (instructions->>pointer::text)::bigint,
                coalesce(instructions->>(pointer+1)::text, '0'),
                coalesce(instructions->>(pointer+2)::text, '0'),
                coalesce(instructions->>(pointer+3)::text, '0')
        ) AS sub(i, k, l, j)
    CROSS JOIN LATERAL
        (
            SELECT
                i%100,
                CASE (i/100)%10
                    WHEN 0
                    THEN coalesce((instructions->>k)::bigint, 0)
                    WHEN 1
                    THEN k::bigint
                    WHEN 2
                    THEN coalesce((instructions->>(k::bigint+base)::text)::bigint, 0)
                END,
                CASE (i/1000)%10
                    WHEN 0  
                    THEN coalesce((instructions->>l)::bigint, 0)
                    WHEN 1
                    THEN l::bigint
                    WHEN 2
                    THEN coalesce((instructions->>(l::bigint+base)::text)::bigint, 0)
                END,
                CASE (i/10000)%10
                    WHEN 0  
                    THEN ARRAY[j]::text[]
                    WHEN 2
                    THEN ARRAY[(j::bigint+base)]::text[]
                END
        ) AS sub2(opcode, x, y, path)
    WHERE
        mode_opcode%100 != 99
), grid AS (
    SELECT
        values[i*3+1] AS x,
        values[i*3+2] AS y,
        values[i*3+3] as v,
        i
    FROM (
        SELECT
            array_agg(value ORDER BY iter)
        FROM
            program
        WHERE
            value IS NOT NULL
    ) sub(values)
    CROSS JOIN LATERAL
        generate_series(0, array_length(values, 1)/3) AS gs(i)
), last_paint AS (
    SELECT DISTINCT ON (x,y)
        x,
        y,
        v
    FROM
        grid
    ORDER BY
        x,
        y,
        i DESC
)
SELECT
    count(*) FILTER (WHERE v=2)
FROM
    last_paint;

\set stdin ''
\set input `cat 13.input`
EXECUTE problem13_1(:'input', :'stdin');

set track_io_timing to 'on';
\o 13-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem13_1(:'input', :'stdin');
\o
