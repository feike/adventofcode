\set VERBOSITY verbose
DEALLOCATE problem07_2;

PREPARE problem07_2 AS
-- START OF input processing
WITH RECURSIVE phase(phase, amp_count) AS MATERIALIZED (
    SELECT
        value::bigint[],
        array_length(value, 1)
    FROM
        regexp_split_to_array($2, ',') AS rsta(value)
)
-- We explode the phases input by generating all possible combinations
-- the below has been generalized, it supports any input length.
-- Even though the current puzzle does not require this generic stuff, it
-- still seems worthwhile to create this, just to know it can be done with
-- recursive CTE's
, phases(phase, phase_id) AS (
    WITH RECURSIVE walk (vals, idxs, level) AS (
        SELECT
            '{}'::bigint[],
            '{}'::bigint[],
            1
        UNION ALL
        SELECT
            vals||phase[idx],
            idxs||idx,
            level + 1
        FROM
            walk
        CROSS JOIN
            phase
        CROSS JOIN (
            WITH RECURSIVE counter(idx) AS (
                SELECT
                    1::bigint
                UNION ALL
                SELECT
                    idx + 1
                FROM
                    counter
                CROSS JOIN
                    phase
                WHERE
                    idx < array_length(phase, 1)
            )
            TABLE counter
            ) AS gs(idx)
        WHERE
            level <= array_length(phase, 1)
            AND NOT idx = ANY(idxs)
    )
    -- This may seem not necessary, for the current assignment every digit
    -- is supposed to be different, but while we're thinking about it, let's
    -- make sure we only pick unique combinations
    , unq AS (
        SELECT DISTINCT
            vals
        FROM
            walk
        CROSS JOIN
            phase
        WHERE
            level = array_length(phase, 1) + 1
    )
    SELECT
        vals,
        rank() OVER (ORDER BY vals) - 1
    FROM
        unq
), input(instructions) AS MATERIALIZED (
    SELECT
        jsonb_object_agg(idx-1, instruction::int)
    FROM
        regexp_split_to_table($1, ',') WITH ORDINALITY AS rsta(instruction, idx)
    WHERE
        instruction <> ''
), setup_amplifiers(amplifiers) AS MATERIALIZED (
    SELECT
        jsonb_object_agg(idx-1,
            jsonb_build_object(
                'instructions',
                instructions
            )
        )
    FROM
        input
    CROSS JOIN
        phase
    CROSS JOIN
        unnest(phase) WITH ORDINALITY AS u(p, idx)
), phase_run(phase, instructions, io, iter) AS (
    SELECT
        phase,
        array_agg(jsonb_build_object('instructions', instructions) ORDER BY idx),
        array_agg(jsonb_build_object(
            'stdin', 
            CASE
                WHEN idx=1
                THEN ARRAY[p, 0]::bigint[]
                ELSE ARRAY[p]::bigint[]
            END,
            'stdout',
            null::bigint[]
            )
        ),
        -1
    FROM
        phases
    CROSS JOIN
        input
    CROSS JOIN
        unnest(phase) WITH ORDINALITY AS u(p, idx)
    GROUP BY
        phase
    UNION ALL
    SELECT
        phase,
        next_instance||instructions[3:]||last_instance,
        io,
        iter+1
    FROM
        phase_run
    JOIN LATERAL
        (SELECT instructions[1], io[1]->>'stdin', io[1]->>'stdout') AS v1(instance, stdin, stdout)
        ON instance->'instructions'->(instance->>'pointer')::text IS DISTINCT FROM '99'
    CROSS JOIN LATERAL (
-- ==================================================================================================================
WITH RECURSIVE instance_run(instance, stdin, stdout, opcode, iter) AS (
    SELECT
        instance::jsonb,
        stdin,
        '{}'::bigint[],
        null::int,
        0
    UNION ALL
    SELECT
        jsonb_build_object(
            'instructions', 
            CASE ir.opcode
                WHEN 1
                THEN jsonb_set(instructions, target, to_jsonb(x + y))
                WHEN 2
                THEN jsonb_set(instructions, target, to_jsonb(x * y))
                WHEN 3
                THEN jsonb_set(instructions, target, to_jsonb(x))
                WHEN 7
                THEN CASE
                        WHEN x < y
                        THEN jsonb_set(instructions, target, to_jsonb(1))
                        ELSE jsonb_set(instructions, target, to_jsonb(0))
                    END
                WHEN 8
                THEN CASE
                        WHEN x = y
                        THEN jsonb_set(instructions, target, to_jsonb(1))
                        ELSE jsonb_set(instructions, target, to_jsonb(0))
                    END
                ELSE instructions
            END,
            
            'base',
            CASE ir.opcode
                WHEN 9
                THEN base + x
                ELSE base
            END,
            
            'pointer',
            CASE ir.opcode
                WHEN 3
                THEN pointer + 2
                WHEN 4
                THEN pointer + 2
                WHEN 9
                THEN pointer + 2
                WHEN 5
                THEN CASE
                        WHEN x <> 0
                        THEN y
                        ELSE pointer + 3
                    END
                WHEN 6
                THEN CASE
                        WHEN x = 0
                        THEN y
                        ELSE pointer + 3
                    END
                ELSE pointer + 4
            END
        ),
        CASE
            WHEN ir.opcode = 3
            THEN ir.stdin[2:]
            ELSE ir.stdin
        END,
        CASE
            WHEN ir.opcode = 4
            THEN ir.stdout||x
            ELSE ir.stdout
        END,
        ir.opcode,
        iter+1
    FROM
        instance_run AS ir
    CROSS JOIN LATERAL (
        SELECT
            coalesce((ir.instance->>'pointer')::bigint, 0) AS pointer,
            coalesce((ir.instance->>'base')::bigint, 0) AS base,
            coalesce((SELECT array_agg(v::bigint) FROM jsonb_array_elements_text(instance->'input') AS jaet(v)), '{}') AS input,
            coalesce((SELECT array_agg(v::bigint) FROM jsonb_array_elements_text(instance->'output') AS jaet(v)), '{}') AS output,
            (ir.instance->'instructions') AS instructions
    ) AS psub1
    -- This Lateral Cross Join results in us having all the values we need available as 
    -- column names: opcode, x, y, target
    CROSS JOIN LATERAL
        (
            SELECT
                a%100,
                -- Opcodes contain parameter modes
                -- 0 = position  mode (value is a pointer to an instruction element at position value)
                -- 1 = immediate mode (value is the value)
                -- 2 = relative  mode (value is a pointer to an instruction element at position (value + base))
                CASE
                    WHEN a%100 = 3
                    THEN input[1]
                    ELSE 
                        CASE (a/100)%10
                            WHEN 0
                            THEN coalesce((instructions->>b::text)::bigint, 0)
                            WHEN 1
                            THEN b::bigint
                            WHEN 2
                            THEN coalesce((instructions->>(b::bigint+base)::text)::bigint, 0)
                        END
                END,

                CASE (a/1000)%10
                    WHEN 0  
                    THEN coalesce((instructions->>c::text)::bigint, 0)
                    WHEN 1
                    THEN c::bigint
                    WHEN 2
                    THEN coalesce((instructions->>(c::bigint+base)::text)::bigint, 0)
                END,

                CASE (a/10000)%10
                    WHEN 0  
                    THEN ARRAY[d]::text[]
                    WHEN 2
                    THEN ARRAY[(d::bigint+base)]::text[]
                END
            FROM (
                SELECT
                    (instructions->>pointer::text)::bigint,
                    coalesce((instructions->>(pointer+1)::text)::bigint, 0),
                    coalesce((instructions->>(pointer+2)::text)::bigint, 0),
                    coalesce((instructions->>(pointer+3)::text)::bigint, 0)
            ) extractfields(a, b, c, d)
        ) AS sub2(opcode, x, y, target)
    WHERE
        debug(format('p:%3s, x:%4s, y:%4s, t:%3s', ir.opcode, x, y, target))
        AND
        CASE coalesce(ir.opcode, 99)
            WHEN 99
            THEN false
            WHEN 3
            THEN array_length(input, 1) > 0
            ELSE true
        END
)
SELECT
    instance_run.instance
FROM
    instance_run
ORDER BY
    iter DESC
LIMIT
    1
-- ==================================================================================================================
    ) AS sub_instance_run
    CROSS JOIN LATERAL (
        SELECT
            jsonb_set(instructions[2], '{input}'::text[], instructions[2]->'input'||to_jsonb(coalesce(output, '{}'))),
            jsonb_set(sub_instance_run.instance, '{output}'::text[], to_jsonb('{}'::bigint[]))
        FROM (
            SELECT
                array_agg(v::bigint)
            FROM
                jsonb_array_elements_text(sub_instance_run.instance->'output') AS jaet2(v)
            ) AS jaet3(output)
        WHERE
            debug(format('%s, %s', jsonb_pretty(instructions[1]), jsonb_pretty(sub_instance_run.instance)))
    ) AS v2(next_instance, last_instance)
)
SELECT
    phase,
    jsonb_pretty(i)
FROM
    phase_run
CROSS JOIN
    unnest(instructions) WITH ORDINALITY AS sa(i, idx)
ORDER BY 
    phase,
    idx
LIMIT
    5
;

TRUNCATE debug;
\set input '3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5'
\set stdin '9,8,7,6,5'
\x off
EXECUTE problem07_2(:'input', :'stdin');

TRUNCATE debug;
\set input '3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10'
\set stdin '9,7,8,5,6'
EXECUTE problem07_2(:'input', :'stdin');
--TABLE debug;

\set stdin '3,1,2,4,0'
\set input `cat 07.input`
--EXECUTE problem07_2(:'input', :'stdin');

set track_io_timing to 'on';
\o 07-1.explain-analyze.txt
--EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_2(:'input', :'stdin');
\o
