DEALLOCATE problem07_1;

PREPARE problem07_1 AS
WITH RECURSIVE phase(phase) AS MATERIALIZED (
    SELECT DISTINCT
        ARRAY[un1,un2,un3,un4,un5]::int[]
    FROM
        regexp_split_to_array($2, ',') AS rsta(stdin)
    CROSS JOIN LATERAL
        unnest(stdin) WITH ORDINALITY AS un1
    JOIN LATERAL
        unnest(stdin) WITH ORDINALITY AS un2 ON (un2.ordinality NOT IN (un1.ordinality))
    JOIN LATERAL
        unnest(stdin) WITH ORDINALITY AS un3 ON (un3.ordinality NOT IN (un1.ordinality, un2.ordinality))
    JOIN LATERAL
        unnest(stdin) WITH ORDINALITY AS un4 ON (un4.ordinality NOT IN (un1.ordinality, un2.ordinality, un3.ordinality))
    JOIN LATERAL
        unnest(stdin) WITH ORDINALITY AS un5 ON (un5.ordinality NOT IN (un1.ordinality, un2.ordinality, un3.ordinality, un4.ordinality))
), input(instructions) AS MATERIALIZED (
    SELECT
        jsonb_object_agg(idx-1, instruction::int)
    FROM
        regexp_split_to_table($1, ',') WITH ORDINALITY AS rsta(instruction, idx)
    WHERE
        instruction <> ''
), all_phases(phase, stdout) AS (
    SELECT
        phase,
        p.stdout
    FROM
        phase
    CROSS JOIN LATERAL (
        WITH RECURSIVE program(stdin, phase_idx, stdout) AS (
            SELECT
                null::int[],
                1,
                0::int
            UNION ALL
            SELECT
                v.stdin,
                program.phase_idx + 1,
                value::int
            FROM
                program
            JOIN LATERAL
                (VALUES(ARRAY[phase[phase_idx], stdout])) AS v(stdin) ON (program.phase_idx <= array_length(phase, 1))
            CROSS JOIN LATERAL (
                WITH RECURSIVE program_run(instructions, pointer, stdin_idx, value, done) AS (
                    SELECT
                        instructions,
                        0::int,
                        1,
                        '',
                        false
                    FROM
                        input
                    UNION ALL
                    SELECT
                        CASE opcode
                            WHEN 1
                            THEN jsonb_set(instructions, path, to_jsonb(x + y))
                            WHEN 2
                            THEN jsonb_set(instructions, path, to_jsonb(x * y))
                            WHEN 3
                            THEN jsonb_set(instructions, ARRAY[k]::text[], to_jsonb(v.stdin[stdin_idx]::int))
                            WHEN 7
                            THEN CASE
                                    WHEN x < y
                                    THEN jsonb_set(instructions, path, to_jsonb(1))
                                    ELSE jsonb_set(instructions, path, to_jsonb(0))
                                END
                            WHEN 8
                            THEN CASE
                                    WHEN x = y
                                    THEN jsonb_set(instructions, path, to_jsonb(1))
                                    ELSE jsonb_set(instructions, path, to_jsonb(0))
                                END
                            ELSE instructions
                        END,
                        CASE opcode
                            WHEN 3
                            THEN pointer + 2
                            WHEN 4
                            THEN pointer + 2
                            WHEN 5
                            THEN CASE
                                    WHEN x <> 0
                                    THEN y
                                    ELSE pointer + 3
                                END
                            WHEN 6
                            THEN CASE
                                    WHEN x = 0
                                    THEN y
                                    ELSE pointer + 3
                                END
                            ELSE pointer + 4
                        END,
                        CASE opcode
                            WHEN 3
                            THEN stdin_idx + 1
                            ELSE stdin_idx
                        END,
                        CASE
                            WHEN opcode = '4'
                            THEN format('%s%s', value, x)
                            ELSE value
                        END,
                        opcode = 99
                    FROM
                        program_run
                    CROSS JOIN LATERAL
                        (
                            SELECT
                                cast(jsonb_path_query(instructions, format('$.%I', pointer)::jsonpath) as int),
                                jsonb_path_query(instructions, format('$.%I', pointer+1)::jsonpath)::text,
                                jsonb_path_query(instructions, format('$.%I', pointer+2)::jsonpath)::text,
                                ARRAY[jsonb_path_query(instructions, format('$.%I', pointer+3)::jsonpath)::text]::text[]
                        ) AS sub(i, k, l, path)
                    CROSS JOIN LATERAL
                        (
                            SELECT
                                i%100,
                                CASE (i/100)%10
                                    WHEN 0
                                    THEN (instructions->>k)::int
                                    ELSE k::int
                                END,
                                CASE (i/1000)%10
                                    WHEN 0  
                                    THEN (instructions->>l)::int
                                    ELSE l::int
                                END
                        ) AS sub2(opcode, x, y)
                    WHERE
                        sub2.opcode IN (1, 2, 3, 4, 5, 6, 7, 8, 99)
                        AND NOT done
                )
                SELECT
                    *
                FROM
                    program_run
                WHERE
                    done
            ) as inner_loop
        )
        SELECT
            *
        FROM
            program
        WHERE
            phase_idx = 6
    ) as p
)
SELECT
    phase,
    stdout
FROM
    all_phases
ORDER BY
    stdout DESC
LIMIT
    1
;

\set input '3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0'
\set stdin '4,3,2,1,0'
EXECUTE problem07_1(:'input', :'stdin');

\set input '3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0'
EXECUTE problem07_1(:'input', :'stdin');

\set input '3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0'
EXECUTE problem07_1(:'input', :'stdin');

\set stdin '3,1,2,4,0'
\set input `cat 07.input`
EXECUTE problem07_1(:'input', :'stdin');

set track_io_timing to 'on';
\o 07-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem07_1(:'input', :'stdin');
\o
