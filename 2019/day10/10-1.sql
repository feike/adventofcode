DEALLOCATE problem10_1;

PREPARE problem10_1 AS
WITH asteroids(id,x,y) AS MATERIALIZED (
    SELECT
        rank() OVER (ORDER BY x, y),
        x::int-1 AS x,
        y::int-1 AS y
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt2(v, x)
    WHERE
        v ='#'
), grid AS (
    SELECT
        c.x,
        c.y,
        format('%s%s',
            CASE WHEN o.y < c.y THEN 'S' WHEN o.y > c.y THEN 'N' END,
            CASE WHEN o.x < c.x THEN 'W' WHEN o.x > c.x THEN 'E' END
        ) AS direction,
        coalesce(abs(c.x - o.x)::float/nullif(abs(c.y - o.y), 0), 0) AS angle
    FROM
        asteroids AS c
    JOIN
        asteroids AS o ON (c.id != o.id)
)
SELECT
    count(distinct (direction, angle))
FROM
    grid
GROUP BY
    x,
    y
ORDER BY
    count(distinct (direction, angle)) DESC
LIMIT
    1
;

EXECUTE problem10_1(
$$.#..#
.....
#####
....#
...##$$);

EXECUTE problem10_1(
$$......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####$$);

EXECUTE problem10_1(
$$#.#...#.#.
.###....#.
.#....#...
##.#.#.#.#
....#.#.#.
.##..###.#
..#...##..
..##....##
......#...
.####.###.$$);

EXECUTE problem10_1(
$$.#..#..###
####.###.#
....###.#.
..###.##.#
##.##.#.#.
....###..#
..#.#..#.#
#..#.#.###
.##...##.#
.....#.#..$$);

EXECUTE problem10_1(
$$.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##$$);

\set input `cat 10.input`
EXECUTE problem10_1(:'input');

set track_io_timing to 'on';
\o 10-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem10_1(:'input');
\o
