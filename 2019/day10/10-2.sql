DEALLOCATE problem10_2;

PREPARE problem10_2 AS
WITH asteroids(id,x,y) AS MATERIALIZED (
    SELECT
        rank() OVER (ORDER BY x, y),
        x::int-1 AS x,
        y::int-1 AS y
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS rstt(line, y)
    CROSS JOIN
        regexp_split_to_table(line, '') WITH ORDINALITY AS rstt2(v, x)
    WHERE
        v ='#'
), grid AS (
    SELECT
        c.x,
        c.y,
        o.x * 100 + o.y AS other,
        CASE
            WHEN angle+90 < 0
            THEN angle+90 + 360
            ELSE angle+90
        END AS angle,
        abs(o.x - c.x) + abs(o.y - c.y) AS distance
    FROM
        asteroids AS c
    JOIN
        asteroids AS o ON (c.id != o.id)
    CROSS JOIN
        atan2d(o.y - c.y, o.x - c.x) AS angle(angle)
), station (x, y) AS (
    SELECT
        x,
        y
    FROM
        grid
    GROUP BY
        x,
        y
    ORDER BY
        count(distinct angle) DESC
    LIMIT
        1
), shooting_order AS (
    SELECT
        other,
        angle,
        rank() OVER (PARTITION BY ANGLE ORDER BY distance ASC) AS rank
    FROM
        station
    JOIN
        grid USING (x, y)
)
SELECT
    other
FROM
    shooting_order
ORDER BY 
    rank,
    angle
LIMIT
    1
OFFSET
    199;

EXECUTE problem10_2(
$$.#....#####...#..
##...##.#####..##
##...#...#.#####.
..#.....#...###..
..#.#.....#....##$$);

EXECUTE problem10_2(
$$.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##$$);

\set input `cat 10.input`
EXECUTE problem10_2(:'input');

set track_io_timing to 'on';
\o 10-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem10_2(:'input');
\o
