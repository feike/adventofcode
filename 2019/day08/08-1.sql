DEALLOCATE problem08_1;

PREPARE problem08_1 AS
WITH input(layer, x, y, digit) AS MATERIALIZED (
    SELECT
        -- we use 0 as the first index for the layer, or the pixel
        (ordinality-1)/area,
        ((ordinality-1)%area)%width,
        ((ordinality-1)%area)/width,
        v::int
    FROM
        (VALUES ($2::int, $3::int, ($2*$3)::int)) AS dims(width, height, area)
    CROSS JOIN
        regexp_split_to_table($1, '') WITH ORDINALITY AS rstt(v)
    
)
SELECT
    count(digit) FILTER (WHERE digit=1)
    *
    count(digit) FILTER (WHERE digit=2)
FROM
    input
GROUP BY
    layer
ORDER BY
    count(*) FILTER (WHERE digit=0) ASC
LIMIT 1
;

\set dimensions '3,2'
\set input '123456789012'
EXECUTE problem08_1(:'input', 3, 2);


\set dimensions '25,6'
\set input `cat 08.input`
EXECUTE problem08_1(:'input', 25, 6);

set track_io_timing to 'on';
\o 08-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_1(:'input', 25, 6);
\o
