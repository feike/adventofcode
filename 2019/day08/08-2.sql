DEALLOCATE problem08_1;

PREPARE problem08_1 AS
WITH input(layer, x, y, digit) AS MATERIALIZED (
    SELECT
        -- we use 0 as the first index for the layer, or the pixel
        (ordinality-1)/area,
        ((ordinality-1)%area)%width,
        ((ordinality-1)%area)/width,
        v::int
    FROM
        (VALUES ($2::int, $3::int, ($2*$3)::int)) AS dims(width, height, area)
    CROSS JOIN
        regexp_split_to_table($1, '') WITH ORDINALITY AS rstt(v)
    
), winning_digit AS (
    SELECT
        x,
        y,
        min(layer) FILTER (WHERE digit!=2) AS layer
    FROM
        input
    GROUP BY
        x,
        y
)
SELECT
    string_agg(line, E'\n' ORDER BY y) AS image
FROM (
    SELECT
        y,
        string_agg(
            CASE digit
                WHEN 0
                THEN '█'
                WHEN 1
                THEN ' '
            END,
            '' ORDER BY x)
    FROM
        winning_digit
    JOIN
        input USING (x,y,layer)
    GROUP BY
        y
) sub(y, line)
;

\set dimensions '3,2'
\set input '123456789012'
EXECUTE problem08_1(:'input', 3, 2);


\set dimensions '25,6'
\set input `cat 08.input`
EXECUTE problem08_1(:'input', 25, 6);

set track_io_timing to 'on';
\o 08-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem08_1(:'input', 25, 6);
\o
