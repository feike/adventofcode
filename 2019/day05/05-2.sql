DEALLOCATE problem05_2;

PREPARE problem05_2 AS
WITH RECURSIVE stdin(stdin) AS MATERIALIZED (
    SELECT
        *
    FROM
        regexp_split_to_array($2, ',')
), program(instructions, pointer, stdin_idx, value) AS (
    SELECT
        jsonb_object_agg(idx-1, instruction::int),
        0::int,
        1,
        ''
    FROM
        regexp_split_to_table($1, ',') WITH ORDINALITY AS rsta(instruction, idx)
    WHERE
        instruction <> ''
    UNION ALL
    SELECT
        CASE opcode
            WHEN 1
            THEN jsonb_set(instructions, path, to_jsonb(x + y))
            WHEN 2
            THEN jsonb_set(instructions, path, to_jsonb(x * y))
            WHEN 3
            THEN jsonb_set(instructions, path, to_jsonb(stdin[stdin_idx]::int))
            WHEN 7
            THEN CASE
                    WHEN x < y
                    THEN jsonb_set(instructions, path, to_jsonb(1))
                    ELSE jsonb_set(instructions, path, to_jsonb(0))
                END
            WHEN 8
            THEN CASE
                    WHEN x = y
                    THEN jsonb_set(instructions, path, to_jsonb(1))
                    ELSE jsonb_set(instructions, path, to_jsonb(0))
                END
            ELSE instructions
        END,
        CASE opcode
            WHEN 3
            THEN pointer + 2
            WHEN 4
            THEN pointer + 2
            WHEN 5
            THEN CASE
                    WHEN x <> 0
                    THEN y
                    ELSE pointer + 3
                 END
            WHEN 6
            THEN CASE
                    WHEN x = 0
                    THEN y
                    ELSE pointer + 3
                 END
            ELSE pointer + 4
        END,
        CASE opcode
            WHEN 3
            THEN stdin_idx + 1
            ELSE stdin_idx
        END,
        CASE
            WHEN opcode = '4'
            THEN x::text
            ELSE ''
        END
    FROM
        program
    CROSS JOIN
        stdin
    CROSS JOIN LATERAL
        (
            SELECT
                cast(jsonb_path_query(instructions, format('$.%I', pointer)::jsonpath) as int),
                jsonb_path_query(instructions, format('$.%I', pointer+1)::jsonpath)::text,
                jsonb_path_query(instructions, format('$.%I', pointer+2)::jsonpath)::text,
                ARRAY[jsonb_path_query(instructions, format('$.%I', pointer+3)::jsonpath)::text]::text[]
        ) AS sub(i, k, l, path)
    CROSS JOIN LATERAL
        (
            SELECT
                i%100,
                CASE (i/100)%10
                    WHEN 0
                    THEN (instructions->>k)::int
                    ELSE k::int
                END,
                CASE (i/1000)%10
                    WHEN 0  
                    THEN (instructions->>l)::int
                    ELSE l::int
                END
        ) AS sub2(opcode, x, y)
    WHERE
        opcode IN (1, 2, 3, 4, 5, 6, 7, 8)
)
SELECT
    value
FROM
    program
WHERE
    value IS NOT NULL
ORDER BY
    pointer DESC
LIMIT
    1
;


\set stdin '5'


EXECUTE problem05_2('3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9', :'stdin');
EXECUTE problem05_2('3,3,1105,-1,9,1101,0,0,12,4,12,99,1', :'stdin');
EXECUTE problem05_2('3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99', :'stdin');

\set input `cat 05.input`
EXECUTE problem05_2(:'input', :'stdin');

set track_io_timing to 'on';
\o 02-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem05_2(:'input', :'stdin');
\o
