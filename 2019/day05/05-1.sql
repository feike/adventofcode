DEALLOCATE problem05_1;

PREPARE problem05_1 AS
WITH RECURSIVE stdin(stdin) AS MATERIALIZED (
    SELECT
        *
    FROM
        regexp_split_to_array($2, ',')
), program(instructions, pointer, stdin_idx, value) AS (
    SELECT
        jsonb_object_agg(idx-1, instruction::int),
        0::int,
        1,
        ''
    FROM
        regexp_split_to_table($1, ',') WITH ORDINALITY AS rsta(instruction, idx)
    WHERE
        instruction <> ''
    UNION ALL
    SELECT
        CASE opcode
            WHEN 1
            THEN jsonb_set(instructions, path, to_jsonb(x + y))
            WHEN 2
            THEN jsonb_set(instructions, path, to_jsonb(x * y))
            WHEN 3
            THEN jsonb_set(instructions, path, to_jsonb(stdin[stdin_idx]::int))
            WHEN 4
            THEN instructions
        END,
        CASE opcode
            WHEN 1
            THEN pointer + 4
            WHEN 2
            THEN pointer + 4
            WHEN 3
            THEN pointer + 2
            WHEN 4
            THEN pointer + 2
        END,
        CASE opcode
            WHEN 3
            THEN stdin_idx + 1
            ELSE stdin_idx
        END,
        CASE
            WHEN opcode = '4'
            THEN x::text
            ELSE ''
        END
    FROM
        program
    CROSS JOIN
        stdin
    CROSS JOIN LATERAL
        (
            SELECT
                cast(jsonb_path_query(instructions, format('$.%I', pointer)::jsonpath) as int),
                jsonb_path_query(instructions, format('$.%I', pointer+1)::jsonpath)::text,
                jsonb_path_query(instructions, format('$.%I', pointer+2)::jsonpath)::text,
                ARRAY[jsonb_path_query(instructions, format('$.%I', pointer+3)::jsonpath)::text]::text[]
        ) AS sub(i, k, l, path)
    CROSS JOIN LATERAL
        (
            SELECT
                i%100,
                CASE (i/100)%10
                    WHEN 0
                    THEN (instructions->>k)::int
                    ELSE k::int
                END,
                CASE (i/1000)%10
                    WHEN 0  
                    THEN (instructions->>l)::int
                    ELSE l::int
                END
        ) AS sub2(opcode, x, y)
    WHERE
        opcode IN (1, 2, 3, 4)
)
SELECT
    value
FROM
    program
WHERE
    value IS NOT NULL
ORDER BY
    pointer DESC
LIMIT
    1
;

EXECUTE problem05_1('1002,4,3,4,33', '');

\set input `cat 05.input`
\set stdin '1'
EXECUTE problem05_1(:'input', :'stdin');

set track_io_timing to 'on';
\o 02-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem05_1(:'input', :'stdin');
\o
