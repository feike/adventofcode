DEALLOCATE problem12_1;

PREPARE problem12_1 AS
WITH RECURSIVE moons(moons) AS MATERIALIZED (
    SELECT
        jsonb_agg(
            jsonb_build_object(
                'x',  m[1]::int,
                'y',  m[2]::int,
                'z',  m[3]::int,
                'vx', 0,
                'vy', 0,
                'vz', 0
            )
            ORDER BY id
        )
    FROM
        regexp_split_to_table($1, '\n') WITH ORDINALITY AS sub(line, id)
    CROSS JOIN LATERAL
        regexp_replace(line, '^<x=([^\s,]+).*y=([^\s,]+).*z=([^\s,]+)>$', '\1 \2 \3') AS rr(rr)
    CROSS JOIN LATERAL
        regexp_split_to_array(rr, ' ') AS rsta(m)
), run_steps AS (
    SELECT
        0 AS loop,
        moons
    FROM
        moons
    UNION ALL
    SELECT
        loop + 1,
        iter.moons
    FROM
        run_steps a
    CROSS JOIN LATERAL (
        SELECT
            jsonb_agg(m_pos ORDER BY id3)
        FROM
            jsonb_array_elements(moons) WITH ORDINALITY AS jaet(m, id1)
        -- This applies the velocity delta's
        CROSS JOIN LATERAL (
            SELECT
                m||
                jsonb_build_object(
                    'vx',
                    (m->>'vx')::bigint + 
                    sum(
                        CASE
                            WHEN (m->>'x')::int < (m2->>'x')::int
                            THEN +1
                            WHEN (m->>'x')::int > (m2->>'x')::int
                            THEN -1
                            ELSE 0
                        END
                    ),

                    'vy',
                    (m->>'vy')::bigint + 
                    sum(
                        CASE
                            WHEN (m->>'y')::int < (m2->>'y')::int
                            THEN +1
                            WHEN (m->>'y')::int > (m2->>'y')::int
                            THEN -1
                            ELSE 0
                        END
                    ),

                    'vz',
                    (m->>'vz')::bigint + 
                    sum(
                        CASE
                            WHEN (m->>'z')::int < (m2->>'z')::int
                            THEN +1
                            WHEN (m->>'z')::int > (m2->>'z')::int
                            THEN -1
                            ELSE 0
                        END
                    )
                ),
                id1
            FROM
                jsonb_array_elements(moons) WITH ORDINALITY AS jaet2(m2, id2)
            WHERE
                id1 != id2
            GROUP BY
                id1
            ) as delta_vel(m_vel, id2)
        -- This applies the velocity delta's
        CROSS JOIN LATERAL (
            SELECT
                m_vel||
                jsonb_build_object(
                    'x',
                    (m_vel->>'x')::bigint + (m_vel->>'vx')::bigint,

                    'y',
                    (m_vel->>'y')::bigint + (m_vel->>'vy')::bigint,

                    'z',
                    (m_vel->>'z')::bigint + (m_vel->>'vz')::bigint
                ),
                id2
            ) as delta_pos(m_pos, id3)
        ) AS iter(moons)
    WHERE
        loop < $2
), last_position(id, kin, pot) AS (
    SELECT
        id,
        sum(
            CASE
                WHEN k LIKE 'v%'
                THEN abs(v::bigint)
            END
        ),
        sum(
            CASE
                WHEN k LIKE 'v%'
                THEN 0
                ELSE abs(v::bigint)
            END
        )
    FROM
        run_steps
    CROSS JOIN LATERAL
        jsonb_array_elements(moons) WITH ORDINALITY AS jae(moon, id)
    CROSS JOIN LATERAL
        jsonb_each(moon) AS je(k, v)
    WHERE
        loop = $2
    GROUP BY
        id
)
SELECT
    sum(kin * pot)
FROM
    last_position
;

\set steps 10
EXECUTE problem12_1(
$$<x=-1, y=0, z=2>
<x=2, y=-10, z=-7>
<x=4, y=-8, z=8>
<x=3, y=5, z=-1>$$, :'steps');

\set steps 100
EXECUTE problem12_1(
$$<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>$$, :'steps');

\set steps 1000
\set input `cat 12.input`
EXECUTE problem12_1(:'input', :'steps');

set track_io_timing to 'on';
\o 12-1.explain-analyze.txt
--EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem12_1(:'input', :'steps');
\o
