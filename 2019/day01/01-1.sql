DEALLOCATE problem01_1;

PREPARE problem01_1 AS
WITH input(module) AS (
    SELECT
        line::int
    FROM
        regexp_split_to_table($1, '\n') AS rstt(line)
)
SELECT
    sum((module/3)-2)
FROM
    input
;

EXECUTE problem01_1('100756');
EXECUTE problem01_1('1969');

\set input `cat 01.input`
EXECUTE problem01_1(:'input');

set track_io_timing to 'on';
\o 01_1.explain_analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_1(:'input');
\o
