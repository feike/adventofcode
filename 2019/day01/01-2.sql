DEALLOCATE problem01_2;

PREPARE problem01_2 AS
WITH RECURSIVE input(module) AS (
    SELECT
        line::int
    FROM
        regexp_split_to_table($1, '\n') AS rstt(line)
), fuel_calculation(fuel) AS (
    SELECT
        (module/3)-2
    FROM
        input
    UNION ALL
    SELECT
        (fuel/3)-2
    FROM
        fuel_calculation
    WHERE
        (fuel/3)-2 > 0
)
SELECT
    sum(fuel)
FROM
    fuel_calculation
;

EXECUTE problem01_2('100756');
EXECUTE problem01_2('1969');


\set input `cat 01.input`
EXECUTE problem01_2(:'input');

set track_io_timing to 'on';
\o 01_2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem01_2(:'input');
\o
