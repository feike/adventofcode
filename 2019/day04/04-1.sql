DEALLOCATE problem04_1;

PREPARE problem04_1 AS
WITH input(low, high) AS MATERIALIZED (
    SELECT
        v[1]::int,
        v[2]::int
    FROM
        regexp_split_to_array($1, '-') AS rsta(v)
)
SELECT
    count(*)
FROM
    input
CROSS JOIN
    generate_series(low, high) AS gs(num)
CROSS JOIN
    cast(num as text) AS c(val)
WHERE
    val ~ '^1*2*3*4*5*6*7*8*9*$'
    AND val ~ '(11|22|33|44|55|66|77|88|99)'
;

\set input `cat 04.input`
EXECUTE problem04_1(:'input');

set track_io_timing to 'on';
\o 04-1.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem04_1(:'input');
\o
