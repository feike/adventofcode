DEALLOCATE problem04_2;

PREPARE problem04_2 AS
WITH input(low, high) AS MATERIALIZED (
    SELECT
        v[1]::int,
        v[2]::int
    FROM
        regexp_split_to_array($1, '-') AS rsta(v)
)
SELECT
    count(*)
FROM
    input
CROSS JOIN
    generate_series(low, high) AS gs(num)
CROSS JOIN
    cast(num as text) AS c(val)
WHERE
    val ~ '^1*2*3*4*5*6*7*8*9*$'
    AND EXISTS (
        SELECT
        FROM
            regexp_matches(val, '((\d)\2+)', 'g') AS rm(m)
        WHERE
            length(m[1]) = 2
    )
;

\set input `cat 04.input`
EXECUTE problem04_2(:'input');

set track_io_timing to 'on';
\o 04-2.explain-analyze.txt
EXPLAIN (ANALYZE ON, BUFFERS ON) EXECUTE problem04_2(:'input');
\o
