#!/bin/bash

if [ -z "$2" ]; then
    echo "Usage: $0 YEAR DAY PART [dev]"
    exit 1
fi

YEAR="$1"
shift
cd "${YEAR}"
./run.sh "$@"
